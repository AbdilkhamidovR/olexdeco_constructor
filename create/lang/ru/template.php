<?
$MESS["FAQ"] = "Вопросы-ответы";
$MESS["LIKE"] = "Нравится";
$MESS["DISLIKE"] = "Не нравится";
$MESS["SUBMIT"] = "Отправить";
$MESS["RESET"] = "Сбросить";
$MESS["CANCEL"] = "Отменить";
$MESS["ASK_QUESTION"] = "Задать вопрос";
$MESS["NO_ITEM_FOUND"] = "Ничего не найдено";
$MESS["EMAIL_PLACEHOLDER"] = "Ваш email*";
$MESS["QUESTION_PLACEHOLDER"] = "Задайте свой вопрос";
$MESS["QUESTION_ADDED_SUCCESS_HEADER"] = "Ваш вопрос принят";
$MESS["QUESTION_ADDED_SUCCESS"] = "Ваш вопрос был передан специалистам и как только он будет отвечен, мы пришлем ответ вам на почту";
$MESS["ADD_QUESTION"] = "Задать вопрос";
$MESS["ASK"] = "Спросить";
$MESS["SHOW_MORE_ANSWERS"] = "Посмотреть еще ответы на этот вопрос";
$MESS["COLLAPSE_MORE_ANSWERS"] = "Скрыть дополнительные ответы";
$MESS["SHOW_MORE_QUESTIONS"] = "Показать еще вопросы";
$MESS["HIDE_MORE_QUESTIONS"] = "Скрыть дополнительные вопросы";
?>