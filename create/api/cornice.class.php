<?
namespace Ra\Constructor;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
error_reporting(E_ERROR);

// use Bitrix\Main;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

Loader::includeModule('iblock');
Loader::includeModule('highloadblock');



/* загрузка классов */
spl_autoload_register(function ($class) {
	if($class == "Ra\Constructor\Graphics"){
		include 'graphics.class.php';
	}
});

class Cornice {
 
	private $arParams = array();
	public $arResult = array();

	function __construct() {
		$this->setParams();
	}

	protected function setParams(){

		// конфиг для нового сайта
		$this->arParams = array(
			"APP_DIR" => "/constructor/create/",
			"IBLOCK_ID" => 25, // Инфоблок каталога товаров
			"IBLOCK_OFFERS_ID" => 26, // Инфоблок связанных торговых предложений
			"SECTIONS" => array(
				"METAL_SECTION_ID" => 135, // Id раздела "Комплектующие для металлических карнизов"
					"CAPS_SECTION_ID" => 136, // Id подраздела "Наконечники"
					"MOUNTS_SECTION_ID" => 178, // Id подраздела "Кронштейны"
					"TUBES_SECTION_ID" => 137, // Id подраздела "Трубы"
					"CONNECTORS_SECTION_ID" => 180, // Id подраздела "Соединители карнизов"
					"FIX_SECTION_ID" => 179, // Id подраздела "Фурнитура для крепления шторы к карнизу"
						"RINGS_SECTION_ID" => 181, // Id подраздела "Кольца"
						"RUNNERS_SECTION_ID" => 182, // Id подраздела "Бегунки"
						"HOOKS_SECTION_ID" => 183, // Id подраздела "Крючки"
			),
			"PROPERTIES" => array(
				"PARENT_PRODUCT" => "CML2_LINK",
				"PARENT_SECTION" => "CML2_SECTION",
				"PRODUCT_BASE_UNIT" => "CML2_BASE_UNIT",
				"OFFER_BASE_UNIT" => "EDINITSA_IZMERENIYA",
				"ROWS_QUANTITY" => "KOLICHESTVO_RYADOV",
				"COLOR" => "TSVET",
				"TUBE_LENGTH" => "DLINA_MM",
				"TUBE_TYPE" => "TIP_TRUBY",
				"PROFILE_TYPE" => "TIP_PROFILYA",
				"TUBE_FORM" => "FORMA_TRUBY",
				"TUBE_FORM_PROFILE_ID" => 30072,
				"CONNECTOR_TYPE" => "BUILDER_CONNECTOR_TYPE", // прямой или эркерный, тип список (L)
				"BUILDER_TUBE_PHOTO" => "BUILDER_TUBES_PHOTO",
				"BUILDER_CAP_PHOTO" => "BUILDER_CAPS_PHOTO",
				"BUILDER_CAP_X" => "BUILDER_CAPS_DX_C", // Глубина одевания на трубу
				"BUILDER_CAP_Y" => "BUILDER_CAPS_DOCK_POINT_Y",
				"BUILDER_MOUNT_PHOTO_LAYERS" => array("BUILDER_MOUNTS_LAYER_1", "BUILDER_MOUNTS_LAYER_2", "BUILDER_MOUNTS_LAYER_3", "BUILDER_MOUNTS_LAYER_4"),
				"BUILDER_MOUNT_ROWS_Y" => array("BUILDER_MOUNTS_DOCK_POINT_1", "BUILDER_MOUNTS_DOCK_POINT_2", "BUILDER_MOUNTS_DOCK_POINT_3"),
				"BUILDER_MOUNT_ROWS_D" => array("BUILDER_MOUNTS_DIAMETER_1", "BUILDER_MOUNTS_DIAMETER_2", "BUILDER_MOUNTS_DIAMETER_3"),
				"BUILDER_MOUNT_LEFT_SPAN" => "BUILDER_MOUNT_DX_M", // Отступ трубы слева от наконечника = RENDER[dX_M] по умолчанию
				"BUILDER_RING_PHOTO" => array("BUILDER_RINGS_LAYER_1", "BUILDER_RINGS_LAYER_2"),
				"BUILDER_RING_Y" => "BUILDER_RINGS_DOCK_POINT_Y",
				"WEIGHT" => "VES_KG", // для корзины
				"VOLUME" => "OBEM_M3", // для корзины
				"RUNNERS_PROFILE_BINDING" => "BUILDER_RUNNERS_PROFILE_BINDING", // привязка бегунка к профилю, тип Привязка к элементу инфоблока (E)
			),
			// сопоставление кода свойства типа Справочник и названия Highloadblock
			"HIGHLOAD_PROPERTIES" => array("TSVET" => "TSVET", "TIP_TRUBY" => "TIPTRUBY", "TIP_PROFILYA" => "TIPPROFILYA"),
			// данные для рендеринга картинки
			"RENDER" => array(
				"CANVAS_W" => 855, // ширина области вывода
				"CANVAS_H" => 564, // высота области вывода
				"W" => 1800, // ширина готовой картинки карниза
				"BG_SRC" => "/assets/images/background_855x564.png", // путь к картинке бэкграунда от корня приложения 
				"WATERMARK_SRC" => "/assets/images/watermark_855x564.png", // путь к картинке водяных знаков от корня приложения 
				"KP" => 0.13, // коэффициент перспективного уменьшения размеров для 2- и 3-рядов
				"dX_M" => 100, // сдвиг влево труб по карнизу относительно Х кронштейна по умолчанию (если не указан в св-ве BUILDER_MOUNT_DX_M)
				"dX_C" => 20, // глубина одевания наконечника на трубу
				"dX_C1" => 20, // глубина одевания наконечника второго ряда на трубу
				"dX_T1" => 40, // сдвиг трубы 2-го ряда относительно трубы 1-го ряда
				"dX_T12" => 20, // сдвиг трубы 3-го ряда относительно трубы 2-го ряда
				"dX_R1" => 70, // сдвиг правого кольца 2-го ряда относительно правого кольца 1-го ряда
				"dX_R2" => 140, // сдвиг правого кольца 3-го ряда относительно правого кольца 1-го ряда
				"dX_LR1" => -5, // сдвиг левого кольца 2-го ряда относительно левого кольца 1-го ряда
				"dX_LR2" => -10, // сдвиг левого кольца 3-го ряда относительно левого кольца 1-го ряда
				"SPAN" => 80, // отступы кртинки от краев
				"PATH" => "/cornice_builder/", // путь записи рендера от папки /upload
				"IMG_EXT" => "jpg", // Расширение файла рендера
			),
			// другое
			"OTHERS" => array(
				"CONNECTOR_TYPES" => array("direct", "oriel"), // тип соединения на 6-м шаге, из св-ва BUILDER_CONNECTOR_TYPE
			)

		);

		$this->arParams["SITE_URL"] = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://').$_SERVER["SERVER_NAME"];
		$this->arParams["SITE_DIR"] = $_SERVER["DOCUMENT_ROOT"];
		$this->arParams["IBLOCK_OFFERS_CODE"] = $this->getIblockXmlIdById($this->arParams["IBLOCK_OFFERS_ID"]);

		$this->getActivePrice();

		$this->arParams["PRICES"]["BASE"]["CURRENCY"] = \Bitrix\Currency\CurrencyManager::getBaseCurrency();

		global $USER;
		$this->arParams["ALLOW_HIRES"] = $USER->IsAdmin() || in_array(8, $USER->GetUserGroupArray()); // админ или контент редактор
		if($this->arParams["ALLOW_HIRES"]){
			$this->arParams["RENDER"]["CANVAS_W"] = 1800;
			$this->arParams["RENDER"]["CANVAS_H"] = 1187;
			$this->arParams["RENDER"]["BG_SRC"] = NULL;
			$this->arParams["RENDER"]["WATERMARK_SRC"] = NULL;
			$this->arParams["RENDER"]["PATH"] = "/cornice_builder/hi_res/";
			$this->arParams["RENDER"]["IMG_EXT"] = "png";
		}

		// $request =  \Bitrix\Main\Context::getCurrent()->getRequest();
		// $this->arParams["APP_DIR"] = $request->getRequestedPageDirectory();

	}

	public function getIblockXmlIdById($id){
		$res = \CIBlock::GetList(array(), array("ID" => $id));
		$arBlock = $res->GetNext(true, false);
		return $arBlock["XML_ID"];
	}

	private function getActivePrice(){
		$arUserPricesIds = \CAIHelper::getActuaPriceTypeForUser_ID();
		$this->arParams["PRICES"]["ACTIVE"] = array();
		if(is_array($arUserPricesIds) && $arUserPricesIds[0]){
			$priceId = $arUserPricesIds[0];
			$this->arParams["PRICES"]["ACTIVE"]["ID"] = $priceId;

			$arPrice = \Bitrix\Catalog\PriceTable::getRow(array(
				'filter' => array(
						'=CATALOG_GROUP_ID' => $priceId,
					),
				'select' => array(
						'CURRENCY'
					)
			));

			$this->arParams["PRICES"]["ACTIVE"]["CURRENCY"] = $arPrice["CURRENCY"];
			unset($arUserPricesIds, $arPrice);
		}else{
			$this->arParams["PRICES"]["ACTIVE"]["ID"] = false;
			$this->arParams["PRICES"]["ACTIVE"]["CURRENCY"] = false;
		}
	}

	private function getConvertedPrice($offerId){
		$arPrice = array();
		if($this->arParams["PRICES"]["ACTIVE"]["ID"]){
			$arPrice = \Bitrix\Catalog\PriceTable::getRow(array(
				'filter' => array(
						'=PRODUCT_ID' => $offerId,
						'=CATALOG_GROUP_ID' => $this->arParams["PRICES"]["ACTIVE"]["ID"],
					),
				'select' => array(
						'PRICE_SCALE'
					)
			));
			return $arPrice["PRICE_SCALE"];
		}else{
			return false;
		}
	}

	public function getParams(){
		return $this->arParams;
	}

	public function getResult(){
		return $this->arResult;
	}


	/**
	 * Обработка запроса
	 * 
	 */
	public function processRequest($arRequest){
		// $allowed = '[^a-zа-яё0-9!;= _\(\)\-\.\,\?\"\'\$]'; // допустимые символы в строке поиска, остальные вырезаются
		// $patt_replace = "/".$allowed."/iu"; 
		// $patt_email = "/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/iu";
		// $patt_question = "/.{5,}/iu";
		$this->arResult["REQUEST"] = $arRequest;
		return;
	}


	/**
	 * Установка сортировки по умолчанию
	 */
	protected function initSort(){
		$this->arResult["SORT"] = array('SORT' => 'ASC', 'ID' => 'ASC');
		return $this->arResult["SORT"];
	}

	/**
	 * Установка сортировки
	 */
	protected function setSort($arSort){
		$this->initSort();
		if(isset($arSort) && is_array($arSort)){
			$this->arResult["SORT"] = array_merge($this->arResult["SORT"], $arSort);
		}
		return;
	}


	/**
	 * Установка фильтра по умолчанию
	 */
	protected function initFilter(){
		$this->arResult["FILTER"] = array(
			"IBLOCK_ID" => $this->arParams["IBLOCK_OFFERS_ID"],
			"INCLUDE_SUBSECTIONS" => "Y",
			"ACTIVE" => "Y",
			"ACTIVE_DATE" => "Y",
			"CATALOG_AVAILABLE" => "Y", // учитывать остатки на складе
		);
		return;
	}

	/**
	 * Установка текущего фильтра
	 */
	protected function setFilter($arFilter){
		// Фильтр
		$this->initFilter();

		if(isset($arFilter) && is_array($arFilter)){

			
			// если не указан родительский продукт
			if(!$arFilter["PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"]]){
				$arProdFilter = null;
				// если указан раздел для выборки
				if($arFilter["SECTION_ID"]){
					$arProdFilter = array(
						"IBLOCK_ID" => $this->arParams["IBLOCK_ID"], 
						"SECTION_ID" => $arFilter["SECTION_ID"]
					);
					$arProdSelect = array("IBLOCK_ID", "ID");
					// $arFilter["PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_SECTION"]] = $arFilter["SECTION_ID"];
				}
				// если передан фильтр по продуктам
				if($arFilter["PRODUCT_FILTER"]){
					$arProdFilter = array("IBLOCK_ID" => $this->arParams["IBLOCK_ID"],);
					$arProdFilter = array_merge($arProdFilter, $arFilter["PRODUCT_FILTER"]);
					$arProdSelect = array("IBLOCK_ID", "ID");
					$arProdSelect = array_merge($arProdSelect, $arFilter["PRODUCT_SELECT"]);
				}
				// выборка при наличии фильтра
				if($arProdFilter){
					$resProducts = \CIBlockElement::GetList(
						array(), 
						$arProdFilter,
						$arProdSelect
					);
					$arProducts = array();
					while($arProduct = $resProducts->GetNext(true, false)){
						$arProducts[] = $arProduct["ID"];
					}
					$arFilter["PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"]] = $arProducts;
					unset($arFilter["SECTION_ID"], $arFilter["PRODUCT_FILTER"], $arFilter["PRODUCT_SELECT"]);
				}
			}

			$this->arResult["FILTER"] = array_merge($this->arResult["FILTER"], $arFilter);
			unset($this->arResult["FILTER"]["SECTION_ID"]);
		}
		return;
	}



	/**
	 * Установка полей выборки по умолчанию
	 */
	protected function initSelect(){
		$this->arResult["SELECT"] = array("IBLOCK_ID", "ID", "NAME", "XML_ID"); // -"CATALOG_GROUP_".$this->arParams["PRICES"]["ACTIVE"]["ID"] 
		return;
	}

	/**
	 * Установка полей выборки
	 */
	protected function setSelect($arSelect){
		$this->initSelect();
		if(isset($arSelect) && is_array($arSelect)){
			$this->arResult["SELECT"] =  array_merge($this->arResult["SELECT"], $arSelect);
		}
		return;
	}



	/**
	 * Установка группировки
	 */
	protected function setGroupBy($groupBy, $arSort){
		$this->arResult["GROUP_BY"] = array("PROPERTY_".$groupBy);
		$this->arResult["SORT"] = $arSort;

		// данные о св-ве
		if( array_key_exists($groupBy, $this->arParams["HIGHLOAD_PROPERTIES"]) ){
			$this->arResult["GROUP_BY_PROPERTY"]["PROPERTY_TYPE"] = "HL";
		}else{
			$this->arResult["GROUP_BY_PROPERTY"] = \CIBlockProperty::GetList(
				array("SORT"=>"ASC"), 
				array(
					"IBLOCK_ID" => $this->arParams["IBLOCK_OFFERS_ID"],
					"CODE" => $groupBy,
				)) -> getNext(true, false);
		}
		return;
	}


	/**
	 * Выборка элементов ТП
	 */
	protected function getItems($args = array(
			"FILTER" => array(),
			"SELECT" => array(),
			"SORT" => array(),
			"GROUP_BY" => false
		))
	{

		$this->arResult["ITEMS"] = array();

		// Поля выборки
		isset($args["SELECT"]) ? $this->setSelect($args["SELECT"]) : $this->initSelect();

		// Сортировка
		isset($args["SORT"]) ? $this->setSort($args["SORT"]) : $this->initSort();

		// Фильтр
		isset($args["FILTER"]) ? $this->setFilter($args["FILTER"]) : $this->initFilter();

		// Limit
		isset($args["LIMIT"]) ? $arNav = array("nTopCount" => $args["LIMIT"]) : $arNav = false;

		// если указана группировка
		if(isset($args["GROUP_BY"]) && $args["GROUP_BY"]){
			$this->setGroupBy($args["GROUP_BY"], $args["SORT"]);
		}else{
			$this->arResult["GROUP_BY"] = false;
		}

		// получение св-ва типа список
		if($this->arResult["GROUP_BY_PROPERTY"]["PROPERTY_TYPE"] == "L"){
			$listRes = \CIBlockPropertyEnum::GetList(
				array("SORT"=>"ASC"), 
				array("IBLOCK_ID"=>$this->arResult["FILTER"]["IBLOCK_ID"], "CODE"=>$this->arResult["GROUP_BY_PROPERTY"]["CODE"])
			);
			$arList = array();
			while($arListRes = $listRes->GetNext(true, false)){
			  $arList[$arListRes["ID"]] = $arListRes;
			}
		}

		// Запрос в БД
		$res = \CIBlockElement::GetList(
			$this->arResult["SORT"], 
			$this->arResult["FILTER"], 
			$this->arResult["GROUP_BY"], 
			$arNav, 
			$this->arResult["SELECT"]
		);

		// обработка результатов запроса
		$this->arResult["ITEMS_QUANTITY"] = $res->SelectedRowsCount();

		$indx = 0;
		if((int)$this->arResult["ITEMS_QUANTITY"]){
			while($arRes = $res->GetNext(true, false)){
				if(isset($args["GROUP_BY"])){
					switch ($this->arResult["GROUP_BY_PROPERTY"]["PROPERTY_TYPE"]) {
						case "L": // list
							if($args["RESULT_AS"] == "XML_IDS"){
								$this->arResult["ITEMS"][] = $arList[$arRes["PROPERTY_".$args["GROUP_BY"]."_ENUM_ID"]]["XML_ID"];
							}else{
								$this->arResult["ITEMS"][] = array(
									"id" => $arRes["PROPERTY_".$args["GROUP_BY"]."_ENUM_ID"],
									"xml_id" => $arList[$arRes["PROPERTY_".$args["GROUP_BY"]."_ENUM_ID"]]["XML_ID"],
									"name" => $arRes["PROPERTY_".$args["GROUP_BY"]."_VALUE"],
									"value" => $arRes["PROPERTY_".$args["GROUP_BY"]."_VALUE"],
								);
							}
							break;
						case "HL": // my custom highload type
							$this->arResult["ITEMS"][] = $arRes["PROPERTY_".$args["GROUP_BY"]."_VALUE"];
							break;
						default:
							$this->arResult["ITEMS"][] = $arRes;
							break;
					}
				}else{

					$this->arResult["ITEMS"][] = $arRes;

					// Заполнение данных для передачи на фронт в ангулар
					// получаем название и базовую единицу из родительского товара
					$arRes["PARENT_PRODUCT"] = $this->getProductByID($arRes["PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"]."_VALUE"]);
					
					// получаем картинки
					$arIm = $this->getItemImages($arRes);

					// получаем формат валюты с названием
					$arRes["CURRENCY"] = \CCurrencyLang::GetCurrencyFormat($this->arParams["PRICES"]["BASE"]["CURRENCY"]);
					// цена в базовой валюте (RUB)
					$price = round($this->getConvertedPrice($arRes["ID"]));

					$this->arResult["NG_ITEMS"][$indx] = array(
						"id" => (int)$arRes["ID"],
						"productId" => (int)$arRes["PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"]."_VALUE"],
						"productXmlId" => $arRes["XML_ID"],
						"catalogXmlId" => $this->arParams["IBLOCK_OFFERS_CODE"],
						"name" => $arRes["PARENT_PRODUCT"]["NAME"],
						"previewImgSrc" => $arIm["DETAIL_PICTURE_SRC"],
						"basketImgSrc" => $arIm["BASKET_PICTURE_SRC"],
						"baseUnit" => $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["OFFER_BASE_UNIT"]."_VALUE"],
						"weight" => $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["WEIGHT"]."_VALUE"],
						"volume" => $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["VOLUME"]."_VALUE"],
						"colorId" => $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"]."_VALUE"],
						"length" => (int)1,
						"currencyFormat" => $arRes["CURRENCY"]["FORMAT_STRING"],
						"priceFormatted" => CurrencyFormat($price, $this->arParams["PRICES"]["BASE"]["CURRENCY"]),
						"price" => $price
					);

					// уточнение результата для труб
					// а именно: получение типа трубы или профиля
					if($arRes["PARENT_PRODUCT"]["IBLOCK_SECTION_ID"] == $this->arParams["SECTIONS"]["TUBES_SECTION_ID"]){
						$propCode = $this->getTubeOrProfileType($arRes["PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"]."_ENUM_ID"]);
						$this->arResult["NG_ITEMS"][$indx]["tubeTypeId"] = $arRes["PROPERTY_".$propCode."_VALUE"];
						$this->arResult["NG_ITEMS"][$indx]["tubeLength"] = (int)$arRes["PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_LENGTH"]."_VALUE"];
					}

					// уточнение результата для колец
					// а именно: получение типа колец RING у труб или RUNNER у профилей
					if($arRes["PARENT_PRODUCT"]["IBLOCK_SECTION_ID"] == $this->arParams["SECTIONS"]["RINGS_SECTION_ID"]){
						$this->arResult["NG_ITEMS"][$indx]["ringType"] = "RING";
					}
					if($arRes["PARENT_PRODUCT"]["IBLOCK_SECTION_ID"] == $this->arParams["SECTIONS"]["RUNNERS_SECTION_ID"]){
						$this->arResult["NG_ITEMS"][$indx]["ringType"] = "RUNNER";
					}

					// уточнение результата для наконечников
					// а именно: получение типов труб (диаметров, размеров сечения) для всех рядов
					if($arRes["PARENT_PRODUCT"]["IBLOCK_SECTION_ID"] == $this->arParams["SECTIONS"]["MOUNTS_SECTION_ID"]){
						$this->arResult["NG_ITEMS"][$indx]["tubeTypeIds"] = array();
						$nRows = (int)$arRes["PROPERTY_".$this->arParams["PROPERTIES"]["ROWS_QUANTITY"]."_VALUE"];
						for ($i=0; $i < $nRows; $i++) { 
							// получить значения множественного св-ва
							$dbProp = \CIBlockElement::GetProperty($this->arParams["IBLOCK_OFFERS_ID"], $arRes["ID"], array("sort" => "asc"), array("CODE"=>$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][$i]));

							while ($arProp = $dbProp->Fetch()) {
								$ar_props[] = $arProp;
								$this->arResult["NG_ITEMS"][$indx]["tubeTypeIds"][$i][] = $arProp["VALUE_XML_ID"];
							}
						}
					}
					$indx++;
					// <
				}
			}
			return $this->arResult["ITEMS"];
		}else{
			return false;
		}

	}


	/**
	 * Получение элемента ТП по его id 
	 */
	protected function getItemById($id){
		if((int)$id){
			return $this->getItems( array("FILTER"=>array("ID" => $id), "SELECT"=>$this->arResult["SELECT"]) );;
		}
		return false;
	}


	/**
	 * Получение элементов ТП по id родительского продукта 
	 */
	protected function getItemByProductId($id){
		if((int)$id){
			$this->getItems( array(
				"FILTER" => array("PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"] => $id),
			));
			return $this->arResult["ITEMS"];
		}
		return false;
	}


	/**
	 * Получение элементов ТП по разделу родительского продукта 
	 */
	protected function getItemBySectionId($id){
		if((int)$id && in_array($id, $this->arParams["SECTIONS"])){
			$this->getItems( array(
				"FILTER"=>array("SECTION_ID" => $id),
			));
			return $this->arResult["ITEMS"];
		}
		return false;
	}


	/**
	 * Получение данных св-в типа Справочник
	 */ 
	protected function getHighloadProperty($propCode, $args = array("select" => array("*") , "filter" => array()) ){
		$arResult = false;

		if(isset($propCode) && isset($this->arParams["HIGHLOAD_PROPERTIES"][$propCode])){

			$highloadCode = $this->arParams["HIGHLOAD_PROPERTIES"][$propCode];

			$hlblock = HL\HighloadBlockTable::getRow(array(
				'filter' => array('NAME' => $highloadCode)
			));

			$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
			$entity_data_class = $entity->getDataClass(); 

			$res = $entity_data_class::getList(array(
				"select" => $args["select"],
				// "order" => array("ID" => "ASC"),
				"filter" => $args["filter"],
			));

			$arResult = array();
			while($arRes = $res->Fetch()){
				switch ($hlblock["NAME"]) {
					case $this->arParams["HIGHLOAD_PROPERTIES"][ $propCode ]:
					case $this->arParams["HIGHLOAD_PROPERTIES"][ $this->arParams["PROPERTIES"]["COLOR"] ]:
						$arRes["imgSrc"] = \CFile::GetPath($arRes["UF_FILE"]);
						break;
					
					default:
						break;
				}
				unset($arRes["ID"], $arRes["UF_FILE"]);
				$arResult[] = $arRes;
			}
		}

		return $arResult;
	}


	/**
	 * Получение данных св-ва Цвет типа Справочник
	 */ 
	protected function getColorProperties($arFilter){
		$arResult = $this->getHighloadProperty(
			$this->arParams["PROPERTIES"]["COLOR"],
			array(
				"filter" => $arFilter, 
				"select" => array("ID", "name"=>"UF_NAME", "id"=>"UF_XML_ID", "UF_FILE")
			));
		return $arResult;
	}


	/**
	 * Получение цветов (тип св-ва - список)
	 */ 
	protected function getColors($arFilter){
		$this -> getItems(array(
			"FILTER" => $arFilter,
			"SORT" => array("PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"] => "ASC"),
			"GROUP_BY" => $this->arParams["PROPERTIES"]["COLOR"],
		));
		return $this->arResult["ITEMS"];
	}


	/**
	 * Получение данных св-ва Тип трубы (размера сечения обычных труб) типа Справочник
	 */
	protected function getTubeTypeProperties($arFilter = array(), $propCode = NULL){
		if(!$propCode)
			$propCode = $this->arParams["PROPERTIES"]["TUBE_TYPE"];
		$arResult = $this->getHighloadProperty(
			$propCode,
			array(
				"filter" => $arFilter, 
				"select" => array("ID", "name"=>"UF_NAME", "id"=>"UF_XML_ID", "UF_FILE")
			));
		return $arResult;
	}


	/**
	 * Выборка труб в слайдер на основании формы трубы
	 * Берем первую найденную в дефолтной сортировке ('SORT' => 'DESC', 'ID' => 'ASC')		
	 */
	protected function getTubeByForm($_arFilter, $byKey = false, $propCode = NULL){

		if(!$propCode)
			$propCode = $this->arParams["PROPERTIES"]["TUBE_TYPE"];

		$arFilter = array(
			"SECTION_ID" => $this->arParams["SECTIONS"]["TUBES_SECTION_ID"],
		);

		if($_arFilter["tubeFormIds"][0]){
			$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"] ] = $_arFilter["tubeFormIds"][0];
		}
		
		$this->setSort(false);
		$this->setFilter($arFilter);
		$this->setSelect(array(
				"DETAIL_PICTURE", 
				// "PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"], 
				"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"],
				"PROPERTY_".$propCode,
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_TUBE_PHOTO"],
			));

		$res = \CIBlockElement::GetList(
			$this->arResult["SORT"], 
			$this->arResult["FILTER"], 
			false, 
			false, 
			$this->arResult["SELECT"]
		);

		$this->arResult["ITEMS"] = array();

		while($arRes = $res->GetNext(true, false)){
			
			if( $byKey && array_key_exists($arRes["PROPERTY_".$propCode."_VALUE"], $this->arResult["ITEMS"]) ){
				continue;
			}

			if($arRes["DETAIL_PICTURE"]){
				$arRes["DETAIL_PICTURE_SRC"] = \CFile::GetPath($arRes["DETAIL_PICTURE"]);
			}else{
				$arRes["DETAIL_PICTURE_SRC"] = "http://placehold.it/168x168";
			}
			
			$arRes["id"] = $arRes["ID"];

			if($byKey){
				$this->arResult["ITEMS"][$arRes[$byKey]] = $arRes;
			}else{
				$this->arResult["ITEMS"][] = $arRes;
			}
		}
		unset($res, $arRes);

		return $this->arResult["ITEMS"];
	}


	/**
	 * Возвращает код св-ва Тип трубы или Тип профиля
	 *	в зависимости от формы трубы		
	 */
	protected function getTubeOrProfileType($_tubeFormId){
		if($_tubeFormId == $this->arParams["PROPERTIES"]["TUBE_FORM_PROFILE_ID"]){
			$propCode = $this->arParams["PROPERTIES"]["PROFILE_TYPE"];
		}else{
			$propCode = $this->arParams["PROPERTIES"]["TUBE_TYPE"];
		}
		return $propCode;
	}


	/**
	 * Получение необходимых изображений
	 * @param  $arItem
	 * @param  $bulderPhotoPropCode - код св-ва, хранящего фото для конструктора
	 * @return $arResult("DETAIL_PICTURE_SRC"=>"...", "BASKET_PICTURE_SRC"=>"...", "BUILDER_PHOTO_SRC"=>"...", )
	 */
	protected function getItemImages($arItem){

		$arResult = array();

		// фото-превью для слайдера и корзины
		if($arItem["DETAIL_PICTURE"]){
			$arIm = \CFile::ResizeImageGet( 
				$arItem["DETAIL_PICTURE"], 
				array("width" => 168, "height" => 168), 
				BX_RESIZE_IMAGE_EXACT
			);
			$arResult["DETAIL_PICTURE_SRC"] = $arIm['src'];

			$arIm = \CFile::ResizeImageGet( 
				$arItem["DETAIL_PICTURE"], 
				array("width" => 70, "height" => 70), 
				BX_RESIZE_IMAGE_EXACT
			);
			$arResult["BASKET_PICTURE_SRC"] = $arIm['src'];

		}else{
			$arResult["DETAIL_PICTURE_SRC"] = "http://placehold.it/168x168";
			$arResult["BASKET_PICTURE_SRC"] = "http://placehold.it/70x70";
		}
		return $arResult;
	}

	/**
	 * Получение данных о продукте по его id
	 * @param  $id
	 * @return $arResult("NAME"=>"название продукта", "BASE_UNIT"=>"единица измерения")
	 */
	public function getProductByID($id){
		$arResult = \CIBlockElement::GetList(
			array(), 
			array("IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "ID" => $id), 
			false, 
			array("nTopCount" => 1), 
			array("IBLOCK_ID", "ID", "NAME", "PROPERTY_".$this->arParams["PROPERTIES"]["PRODUCT_BASE_UNIT"], "IBLOCK_SECTION_ID")
		)->GetNext(true, false);

		return $arResult;
	}

	/**
	 * Получение данных св-ва диаметра ряда кронштейна
	 */
	protected function getMountRowDiameter( $args = array("KEY" => "XML_ID", "VALUE" => "ID", "PROPCODE" => false) )
	{
		if(!$args["PROPCODE"]){
			$args["PROPCODE"] = $this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][0];
		}
		$listRes = \CIBlockPropertyEnum::GetList(
			array("SORT" => "ASC"), 
			array("IBLOCK_ID" => $this->arParams["IBLOCK_OFFERS_ID"], "CODE" => $args["PROPCODE"])
		);
		$arList = array();
		while($arListRes = $listRes->GetNext(true, false)){
		  $arList[ $arListRes[ $args["KEY"] ] ] = $arListRes[ $args["VALUE"] ];
		}
		return $arList;
	}


	/**
	 * Выборка данных наконечника для отрисовки карниза и для корзины
	 * @param  
	 * @return $this->arResult["NG_VARS"]
	 */
	protected function getCorniceItems($_args = array()){

		$nRow = $_args["N_ROW"] ? $_args["N_ROW"] : 0;

		// filter
		$arFilter["SECTION_ID"] = $_args["FILTER"]["SECTION_ID"];

		if($_args["FILTER"]["productId"]){
			$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"] ] = $_args["FILTER"]["productId"];
		}

		// уточнение фильтра для труб
		if($_args["FILTER"]["SECTION_ID"] == $this->arParams["SECTIONS"]["TUBES_SECTION_ID"] && $_args["FILTER"]["tubeFormIds"][0]){
			$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"] ] = $_args["FILTER"]["tubeFormIds"][0];
			if($_args["FILTER"]["tubeLength"]){
				$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_LENGTH"]."_VALUE" ] = $_args["FILTER"]["tubeLength"];
			}
		}
		// уточнение фильтра для кронштейнов
		if($arFilter["SECTION_ID"] == $this->arParams["SECTIONS"]["MOUNTS_SECTION_ID"]){
			$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["ROWS_QUANTITY"] ] = $_args["FILTER"]["nRowsId"];
			$arMountDmList = $this->getMountRowDiameter();
			$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][0] ] = $arMountDmList[$_args["FILTER"]["tubeTypeIds"][0]];

			if($_args["FILTER"]["tubeTypeIds"][1]){
				$arMountDmList =  $this->getMountRowDiameter( array("KEY" => "XML_ID", "VALUE" => "ID", "PROPCODE" => $this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][1]) );
				
				$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][1] ] = $arMountDmList[$_args["FILTER"]["tubeTypeIds"][1]];

			}
			if($_args["FILTER"]["tubeTypeIds"][2]){
				$arMountDmList =  $this->getMountRowDiameter( array("KEY" => "XML_ID", "VALUE" => "ID", "PROPCODE" => $this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][2]) );
				$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][2] ] = $arMountDmList[$_args["FILTER"]["tubeTypeIds"][2]];
			}
		}else{
			array_push($arFilter, array(
					"LOGIC" => "OR",
					"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"] => $_args["FILTER"]["tubeTypeIds"][$nRow],
					"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"] => $_args["FILTER"]["tubeTypeIds"][$nRow],
			));
		}

		// уточнение фильтра для бегунков профилей
		if($arFilter["SECTION_ID"] == $this->arParams["SECTIONS"]["RUNNERS_SECTION_ID"]){
			$arFilter = $_args["FILTER"];
		}

		// уточнение для наконечников. Используется для отбора заглушек на доп. ряды
		if($_args["FILTER"]["NAME"]){
			$arFilter["NAME"] = $_args["FILTER"]["NAME"];
		}

		if($_args["FILTER"]["colorId"]){
			$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"] ] = $_args["FILTER"]["colorId"];
		}

		// игнорировать наличие фото для конструктора
		if(!$_args["IGNORE_BUILDER_PHOTO_CHECK"]){
			$arFilter[ "!PROPERTY_".$_args["BUILDER_PHOTO_PROP_CODE"] ] = false;
		}

		// select
		$_arSelect = $_args["SELECT"];
		$defaultSelect = array(
				"DETAIL_PICTURE",
				"PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["OFFER_BASE_UNIT"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["WEIGHT"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["VOLUME"],
				// "PROPERTY_".$propCode,
				// "CATALOG_GROUP_".$this->arParams["PRICES"]["ACTIVE"]["ID"]
			);

		$args = array(
			"FILTER" => $arFilter,
			"SELECT" => array_merge($defaultSelect, $_args["SELECT"])
			);

		// limit
		if($_args["LIMIT"])
			$args["LIMIT"] = $_args["LIMIT"];

		// запрос
		$this -> getItems($args);
		if($this->arResult["ITEMS_QUANTITY"]){
			return $this->arResult["NG_ITEMS"];
		}else{
			return false;
		}
	}


	/**
	 * Выборка типов труб для ряда nRow по существующим типам крeпления труб кронштейна 
	 * Название св-ва: <>BDR_M_Диаметр ряд №1
	 * @return array of xml_ids for tubes filter
	 */
	public function getMountTubeTypes($_arFilter, $_arParams = array()){
		$nRow = $_arParams["nRow"] ? $_arParams["nRow"] : 0;

		$arFilter = array(
			"SECTION_ID" => $this->arParams["SECTIONS"]["MOUNTS_SECTION_ID"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["ROWS_QUANTITY"] => $_arFilter["nRowsId"],
			"!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][3] => false,
			"!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][0] => false
		);
		switch ($nRow) {
			// фильтр для второго ряда
			case 1:
				$arMountDmList = $this->getMountRowDiameter();
				$arFilter["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][0]] = $arMountDmList[$_arFilter["tubeTypeIds"][0]];
				$arFilter["!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][2]] = false;
				$arFilter["!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][0]] = false;
				$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"] ] = $_arFilter["colorId"];
				break;
		}

		$arItems = $this -> getItems(array(
			"FILTER" => $arFilter,
			"SORT" => array("PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][$nRow] => "ASC"),
			"GROUP_BY" => $this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][$nRow],
			"RESULT_AS" => "XML_IDS",
		));

		$this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][$nRow] ] = $this->arResult["ITEMS"];

		return $this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][$nRow] ];
	}


	/**
	 * ========= PUBLIC FUNCTIONS ===========
	 */

	/**
	 * Получение данных пользователя
	 */
	public function getUser(){
		$this->arResult["USER"] = array();
		global $USER;

		if($userId = $USER->GetID()){

			$this->arResult["USER"] = \Bitrix\Main\UserTable::getRow(array(
				'filter' => array(
						'=ID' => $userId,
						'=ACTIVE' => 'Y',
					),
				'select' => array(
						'EMAIL', 'LOGIN', 'NAME', 'SECOND_NAME', 'LAST_NAME', 
					)
			));

			$result = \Bitrix\Main\UserGroupTable::getList(array(
				'filter' => array('USER_ID'=>$USER->GetID(),'GROUP.ACTIVE'=>'Y'),
				'select' => array('GROUP_ID'), // выбираем идентификатор группы и символьный код группы
				'order' => array('GROUP.C_SORT'=>'ASC'), // сортируем в соответствии с сортировкой групп
			));
			$this->arResult["USER"]["IS_ADMIN"] = $USER->IsAdmin();
			
			while ($arGroup = $result->fetch()) {
				$arGroups[] = $arGroup["GROUP_ID"];
			}
			$this->arResult["USER"]["GROUPS"] = $arGroups;
			unset($arGroup, $arGroups);

			return $this->arResult["USER"];
		}else{
			return false;
		}
	}


	/**
	 * Получение доступного кол-ва рядов (тип св-ва - список)
	 */
	public function getRowQuantities(){
		$arFilter = array(
			"SECTION_ID" => $this->arParams["SECTIONS"]["MOUNTS_SECTION_ID"],
			"!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][3] => false
		);
		$arItems = $this -> getItems(array(
			"FILTER" => $arFilter,
			"SORT" => array("PROPERTY_".$this->arParams["PROPERTIES"]["ROWS_QUANTITY"] => "ASC"),
			"GROUP_BY" => $this->arParams["PROPERTIES"]["ROWS_QUANTITY"],
		));

		$this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["ROWS_QUANTITY"] ] = $this->arResult["ITEMS"];

		return $this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["ROWS_QUANTITY"] ];
	}


	/**
	 * Получение доступных форм трубы (тип св-ва - список)
	 */
	public function getTubeForms($_arFilter){
		$arTubeOrProfileTypeFilter = $this->getMountTubeTypes($_arFilter);
		$arFilter = array(
			"SECTION_ID" => $this->arParams["SECTIONS"]["TUBES_SECTION_ID"],
			array(
				"LOGIC" => "OR",
				"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"] => $arTubeOrProfileTypeFilter,
				"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"] => $arTubeOrProfileTypeFilter,
			),
			"!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_TUBE_PHOTO"] => false,
		);

		$arItems = $this -> getItems(array(
			"FILTER" => $arFilter,
			"SORT" => array("PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"] => "ASC"),
			"GROUP_BY" => $this->arParams["PROPERTIES"]["TUBE_FORM"],
		));

		$this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["TUBE_FORM"] ] = $this->arResult["ITEMS"];

		return $this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["TUBE_FORM"] ];
	}

	/**
	 * Получение типов трубы (диаметров) в выбранных элементах
	 */
	public function getTubeTypes($_arFilter = array(), $_arParams = array()){

		$nRow = $_arParams["nRow"] ? $_arParams["nRow"] : 0;

		$arTubeOrProfileTypeFilter = $this->getMountTubeTypes($_arFilter, array("nRow" => $nRow));

		$arFilter = array(
			"SECTION_ID" => $this->arParams["SECTIONS"]["TUBES_SECTION_ID"],
			array(
				"LOGIC" => "OR",
				"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"] => $arTubeOrProfileTypeFilter,
				"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"] => $arTubeOrProfileTypeFilter,
			),
			"!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_TUBE_PHOTO"] => false
		);
		if($_arFilter["tubeFormIds"][0]){
			$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"] ] = $_arFilter["tubeFormIds"][0];
		}

		$propCode = $this->getTubeOrProfileType($_arFilter["tubeFormIds"][0]);

		// --
		$arItems = $this -> getItems(array(
			"FILTER" => $arFilter,
			"SORT" => array("PROPERTY_".$propCode => "ASC"),
			"GROUP_BY" => $propCode,
		));

		$arProps = $this->getTubeTypeProperties( array("UF_XML_ID" => $arItems ), $propCode);

		//получить одну первую трубу выбранной формы в дефолтной сортировке ('SORT' => 'DESC', 'ID' => 'ASC')
		$arItems = $this->getTubeByForm($_arFilter, "PROPERTY_".$propCode."_VALUE", $propCode);

		foreach ($arProps as &$arProp) {
			$arProp["tubeId"] = $arItems[$arProp["id"]]["ID"];
			$arProp["tubeName"] = $arItems[$arProp["id"]]["NAME"];
			$arProp["tubeImgSrc"] = $arItems[$arProp["id"]]["DETAIL_PICTURE_SRC"];
			$arProp["previewImgSrc"] = $arItems[$arProp["id"]]["DETAIL_PICTURE_SRC"];
		}

		$this->arResult["PROPERTIES"][ $propCode ] = $arProps;
		return $this->arResult["PROPERTIES"][ $propCode ];
	}

	/**
	 * Получение цветов труб в выбранных элементах
	 */
	public function getTubeColors($_arFilter = array()){
		$arFilter = array(
			"SECTION_ID" => $this->arParams["SECTIONS"]["TUBES_SECTION_ID"],
			"!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_TUBE_PHOTO"] => false
		);
		if($_arFilter["tubeFormIds"][0]){
			$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"] ] = $_arFilter["tubeFormIds"][0];
		}

		$propCode = $this->getTubeOrProfileType($_arFilter["tubeFormIds"][0]);

		if($_arFilter["tubeTypeIds"][0]){
			$arFilter[ "PROPERTY_".$propCode ] = $_arFilter["tubeTypeIds"][0];
		}

		$this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["COLOR"] ] = $this->getColorProperties( array("UF_XML_ID" => $this->getColors($arFilter)));
		
		return $this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["COLOR"] ];
	}

	/**
	 * Получение цветов наконечников в выбранных элементах
	 */
	public function getMountColors($_arFilter = array()){
		$arFilter = array(
			"SECTION_ID" => $this->arParams["SECTIONS"]["MOUNTS_SECTION_ID"],
			"!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][3] => false
		);

		$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["ROWS_QUANTITY"] ] = $_arFilter["nRowsId"];
		$arMountDmList = $this->getMountRowDiameter();
		$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][0] ] = $arMountDmList[$_arFilter["tubeTypeIds"][0]];
		if($_arFilter["tubeTypeIds"][1]){
			$arMountDmList =  $this->getMountRowDiameter( array("KEY" => "XML_ID", "VALUE" => "ID", "PROPCODE" => $this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][1]) );
			$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][1] ] = $arMountDmList[$_arFilter["tubeTypeIds"][1]];
		}
		if($_arFilter["tubeTypeIds"][2]){
			$arMountDmList =  $this->getMountRowDiameter( array("KEY" => "XML_ID", "VALUE" => "ID", "PROPCODE" => $this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][2]) );
			$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][2] ] = $arMountDmList[$_arFilter["tubeTypeIds"][2]];
		}

		$this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["COLOR"] ] = $this->getColorProperties( array("UF_XML_ID" => $this->getColors($arFilter)));
		
		return $this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["COLOR"] ];
	}


	/**
	 * Получение цветов наконечников в выбранных элементах
	 */
	public function getCapsColors($_arFilter = array()){
		$arFilter = array(
			"SECTION_ID" => $this->arParams["SECTIONS"]["CAPS_SECTION_ID"],
			"!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_CAP_PHOTO"] => false,
			array(
				"LOGIC" => "OR",
				"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"] => $_arFilter["tubeTypeIds"][0],
				"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"] => $_arFilter["tubeTypeIds"][0],
			),
		);

		$this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["COLOR"] ] = $this->getColorProperties( array("UF_XML_ID" => $this->getColors($arFilter)));
		
		return $this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["COLOR"] ];
	}


	/**
	 * Получение цветов наконечников в выбранных элементах
	 */
	public function getRingsColors($_arFilter = array()){
		$arFilter = array(
			"SECTION_ID" => $this->arParams["SECTIONS"]["RINGS_SECTION_ID"],
			"!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_RING_PHOTO"][0] => false,
			array(
				"LOGIC" => "OR",
				"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"] => $_arFilter["tubeTypeIds"][0],
				"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"] => $_arFilter["tubeTypeIds"][0],
			),
		);

		$this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["COLOR"] ] = $this->getColorProperties( array("UF_XML_ID" => $this->getColors($arFilter)));
		
		return $this->arResult["PROPERTIES"][ $this->arParams["PROPERTIES"]["COLOR"] ];
	}



	/**
	 * Выборка труб
	 */
	public function getTubes($_arFilter, $_limit = false, $nRow=0){
		$_arFilter["SECTION_ID"] = $this->arParams["SECTIONS"]["TUBES_SECTION_ID"];

		if($_arFilter["productId"] || $_arFilter["tubeLength"]){
			$ignoreCheckBuilderPhoto = true;
		}

		$args = array(
			"FILTER" => $_arFilter,
			"SELECT" => array(
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_CAP_PHOTO"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_LENGTH"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_TUBE_PHOTO"],
				),
			"BUILDER_PHOTO_PROP_CODE" => $this->arParams["PROPERTIES"]["BUILDER_TUBE_PHOTO"],
			"IGNORE_BUILDER_PHOTO_CHECK" => $ignoreCheckBuilderPhoto,
			"N_ROW" => $nRow,
			);
		if($_limit)
			$args["LIMIT"] = $_limit;

		$arResult = $this->getCorniceItems($args);
		return $arResult;
	}

	/**
	 * Выборка трубы для отрисовки карниза и для корзины
	 */
	public function getTube($_arFilter){
		$arResult = $this->getTubes($_arFilter, 1, 0);
		return $arResult[0];
	}
	/**
	 * Выборка трубы для отрисовки карниза и для корзины для указанного в параметрах ряда
	 */
	public function getTubeExt($_arFilter, $_params){
		$nRow = $_params["nRow"] ? $_params["nRow"] : 0; 
		$arResult = $this->getTubes($_arFilter, 1, $nRow);
		return $arResult[0];
	}

	/**
	 * Выборка трубы второго ряда для отрисовки карниза и для корзины
	 */
	public function getTube2($_arFilter){
		$arResult = $this->getTubes($_arFilter, 1, 1);
		return $arResult[0];
	}

	/**
	 * Выборка трубы третьего ряда для отрисовки карниза и для корзины
	 */
	public function getTube3($_arFilter){
		$arResult = $this->getTubes($_arFilter, 1, 2);
		return $arResult[0];
	}

	/**
	 * Выборка кронштейнов
	 */
	public function getMounts($_arFilter, $_limit = false){
		$_arFilter["SECTION_ID"] = $this->arParams["SECTIONS"]["MOUNTS_SECTION_ID"];
		$args = array(
			"FILTER" => $_arFilter,
			"SELECT" => array(
				"PROPERTY_".$this->arParams["PROPERTIES"]["ROWS_QUANTITY"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][0],
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][1],
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_D"][2],
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][3],
				),
			"BUILDER_PHOTO_PROP_CODE" => $this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][3],
			);
		if($_limit)
			$args["LIMIT"] = $_limit;
		$arResult = $this->getCorniceItems($args);

		return $this->arResult["NG_ITEMS"];
	}

	/**
	* Выборка данных кронштейна для отрисовки карниза и для корзины
	*/
	public function getMount($_arFilter, $_arParams = array()){

		$nRow = $_arParams["nRow"] ? $_arParams["nRow"] : 0;
		if($nRow == 0){
			array_splice($_arFilter["tubeTypeIds"], 1);
		}
		$arResult = $this->getMounts($_arFilter, 1);
		return $arResult[0];
	}

	/**
	* Выборка данных кронштейна для отрисовки карниза и для корзины только по типу трубы первого ряда
	* для шага выбора трубы
	*/
	public function getMountByFirstRow($_arFilter){
		array_splice($_arFilter["tubeTypeIds"], 1);
		$arResult = $this->getMounts($_arFilter, 1);
		return $arResult[0];
	}


	/**
	 * Выборка наконечников
	 */
	public function getCaps($_arFilter, $_limit = false, $nRow = 0){
		$_arFilter["SECTION_ID"] = $this->arParams["SECTIONS"]["CAPS_SECTION_ID"];

		$args = array(
			"FILTER" => $_arFilter,
			"SELECT" => array("PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_CAP_PHOTO"]),
			"BUILDER_PHOTO_PROP_CODE" => $this->arParams["PROPERTIES"]["BUILDER_CAP_PHOTO"],
			"N_ROW" => $nRow,
			);
		if($_limit)
			$args["LIMIT"] = $_limit;

		$arResult = $this->getCorniceItems($args);
		return $this->arResult["NG_ITEMS"];
	}


	/**
	 * Выборка данных наконечника для отрисовки карниза и для корзины
	 */
	public function getCap($_arFilter){
		$arResult = $this->getCaps($_arFilter, 1);
		return $arResult[0];
	}

	/**
	 * Выборка данных наконечника 2-го ряда для отрисовки карниза и для корзины
	 * @return $this->arResult["NG_VARS"]
	 */
	public function getCap2($_arFilter){
		// если тип трубы второго ряда d25
		// выбрать наконечник "Филда"
		if($_arFilter['tubeTypeIds'][1] == 'b9fac578-7c3e-11e7-a9bb-000c290d7f42'){
			$_arFilter["NAME"] = "%Филда%";
		}else{
			$_arFilter["NAME"] = "%Заглушка%";
		}
		$arResult = $this->getCaps($_arFilter, 1, 1);
		return $arResult[0];
	}

	/**
	 * Выборка данных наконечника 3-го ряда для отрисовки карниза и для корзины
	 */
	public function getCap3($_arFilter){
		// если тип трубы третьего ряда d25
		// выбрать наконечник "Филда"
		if($_arFilter['tubeTypeIds'][1] == 'b9fac578-7c3e-11e7-a9bb-000c290d7f42'){
			$_arFilter["NAME"] = "%Филда%";
		}else{
			$_arFilter["NAME"] = "%Заглушка%";
		}
		$arResult = $this->getCaps($_arFilter, 1, 2);
		return $arResult[0];
	}


	/**
	 * Выборка колец
	 */
	public function getRings($_arFilter, $_args = null, $_limit = false, $nRow = 0){

		if($_arFilter["tubeFormIds"][0] == $this->arParams["PROPERTIES"]["TUBE_FORM_PROFILE_ID"]){
			// для выборки бегунков для профилей
			$_arFilter = array();
			$_arFilter["SECTION_ID"] = $this->arParams["SECTIONS"]["RUNNERS_SECTION_ID"];
			$_arFilter["PROPERTY_".$this->arParams["PROPERTIES"]["RUNNERS_PROFILE_BINDING"]] = $_args["profileId"];
			$args = array(
				"FILTER" => $_arFilter,
				"SELECT" => array(
					"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_RING_PHOTO"][0],
				),
				"BUILDER_PHOTO_PROP_CODE" => $this->arParams["PROPERTIES"]["BUILDER_RING_PHOTO"][0],
				"N_ROW" => $nRow,
				// "IGNORE_BUILDER_PHOTO_CHECK" => true
				);
		}else{
			// для выборки колец для труб
			$_arFilter["SECTION_ID"] = $this->arParams["SECTIONS"]["RINGS_SECTION_ID"];
			$args = array(
				"FILTER" => $_arFilter,
				"SELECT" => array("PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_RING_PHOTO"][0]),
				"BUILDER_PHOTO_PROP_CODE" => $this->arParams["PROPERTIES"]["BUILDER_RING_PHOTO"][0],
				"N_ROW" => $nRow,
				);
		}

		if($_limit)
			$args["LIMIT"] = $_limit;

		$arResult = $this->getCorniceItems($args);
		return $this->arResult["NG_ITEMS"];
	}


	/**
	 * Выборка данных кольца первого ряда для отрисовки карниза и для корзины
	 */
	public function getRing($_arFilter, $_args = null){
		$arResult = $this->getRings($_arFilter, $_args, 1, 0);
		return $arResult[0];
	}

	/**
	 * Выборка данных кольца первого ряда для отрисовки карниза и для корзины
	 */
	public function getRing2($_arFilter, $_args = null){
		if($_arFilter["name"])
			$_arFilter["NAME"] = $_arFilter["name"];
		$arResult = $this->getRings($_arFilter, $_args, 1, 1);
		return $arResult[0];
	}

	/**
	 * Выборка данных кольца первого ряда для отрисовки карниза и для корзины
	 */
	public function getRing3($_arFilter, $_args = null){
		$arResult = $this->getRings($_arFilter, $_args, 1, 2);
		return $arResult[0];
	}


	/**
	 * Получение длин (тип св-ва - строка) по трубе
	 * added 2018-09-10: по фильтру
	 */
	public function getTubeLengthsByTube($_arFilter){

		$this->setSelect(array(
			"PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"],
		));
		
		$_tubeId = $_arFilter["id"];

		if($_tubeId){
			
			$arItem = $this->getItemById($_tubeId);

			$arFilter = array(
				"PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"] => $arItem[0]["PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"]."_VALUE"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"] =>  $arItem[0]["PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"]."_VALUE"],
				array(
					"LOGIC" => "OR",
					"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"] => $arItem[0]["PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"]."_VALUE"],
					"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"] => $arItem[0]["PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"]."_VALUE"],
				),
			);
			
		}else{
			$arFilter = array(
				"SECTION_ID" =>  $this->arParams["SECTIONS"]["TUBES_SECTION_ID"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"] =>  $_arFilter["colorId"],
				"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"] => $_arFilter["tubeFormIds"][0],
				array(
					"LOGIC" => "OR",
					"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"] => $_arFilter["tubeTypeIds"][0],
					"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"] => $_arFilter["tubeTypeIds"][0],
				),
			);
		}

		$this -> getItems(array(
			"FILTER" => $arFilter,
			"SORT" => array("PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_LENGTH"] => "ASC"),
			"GROUP_BY" => $this->arParams["PROPERTIES"]["TUBE_LENGTH"],
		));

		return $this->arResult["ITEMS"];
	}

	/**
	 * Получение длин (тип св-ва - строка) по трубам
	 */
	public function getTubeLengthsByTubes($_arFilter){
		$arTubes = $_arFilter["tubes"];
		$ar = array();
		$arResult = array();
		$this->arResult["ITEMS"] = array();
		foreach ($arTubes as $arTube) {
			$ar[] = $this->getTubeLengthsByTube($arTube);
		}
		// fppr($ar, __FILE__.' $ar');
		// возвращаем только те длины, которые есть ВО ВСЕХ трубах
		switch (count($arTubes)) {
			case 3:
				$arResult = array_intersect($ar[0], $ar[1], $ar[2]);
				break;
			case 2:
				$arResult = array_intersect($ar[0], $ar[1]);
				break;
			case 1:
			default:
				$arResult = $ar[0];
				break;
		}
		if($arResult){
			$this->arResult["ITEMS"] = $arResult;
			return $this->arResult["ITEMS"]; 
		}else{
			return false;
		}
	}


	/**
	 * Получение соединителей
	 */
	public function getConnectors($_args){
		$arTubes = $_args["tubes"];

		$arSelect = array(
			"DETAIL_PICTURE",
			"PROPERTY_".$this->arParams["PROPERTIES"]["PARENT_PRODUCT"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["OFFER_BASE_UNIT"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["WEIGHT"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["VOLUME"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"],
			// "CATALOG_GROUP_".$this->arParams["PRICES"]["ACTIVE"]["ID"]
		);
		$this->setSelect($arSelect);

		$arConnectors = array();
		foreach ($this->arParams["OTHERS"]["CONNECTOR_TYPES"] as $connectorType) {
			$arExists[$connectorType] = true;
			foreach ($arTubes as $nrow => $arTube) {
				$arTubeItems = $this->getItemById($arTube["id"]);
				$arTubeItem = $arTubeItems[0];
				$arFilter = array(
					"SECTION_ID" => $this->arParams["SECTIONS"]["CONNECTORS_SECTION_ID"],
					array(
						"LOGIC" => "OR",
						"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"] => $arTubeItem["PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"]."_VALUE"],
						"PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"] => $arTubeItem["PROPERTY_".$this->arParams["PROPERTIES"]["PROFILE_TYPE"]."_VALUE"],
					),
					"PRODUCT_FILTER" => array("PROPERTY_".$this->arParams["PROPERTIES"]["CONNECTOR_TYPE"]."_VALUE" => $connectorType),
					"PRODUCT_SELECT" => array("PROPERTY_".$this->arParams["PROPERTIES"]["CONNECTOR_TYPE"])
				);

				if($connectorType == 'oriel'){
					$arFilter["PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"]] = $arTubeItem["PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"]."_VALUE"];
				}

				
				// запрос
				$arRes = $this -> getItems(array(
					"FILTER" => $arFilter,
					"SELECT" => $arSelect,
				));
				if($arRes){
					$arConnectors[$connectorType][$nrow] = $this->arResult["NG_ITEMS"][0];
				}else{
					$arConnectors[$connectorType][$nrow] = null;
					$arExists[$connectorType] = false;
				}
			}
		}

		return array("connectors" => $arConnectors, "exists" => $arExists);
	}

	/**
	 * добавление в корзину битрикса
	 */
	public function addToBxBasket($_args){
		$arBasket = $_args["basket"];
		// fppr($_args, __FILE__.' $_args');
		
		$basket = \Bitrix\Sale\Basket::loadItemsForFUser(
			\Bitrix\Sale\Fuser::getId(),
			\Bitrix\Main\Context::getCurrent()->getSite()
		);

		foreach ($arBasket["items"] as $key => $basketItem) {
			$basketProps = array(
				array(
					'NAME' => 'Вес (кг)',
					'CODE' => $this->arParams["PROPERTIES"]["WEIGHT"],
					'VALUE' => $basketItem["weight"],
				),
				array(
					'NAME' => 'Объем (м3)',
					'CODE' => $this->arParams["PROPERTIES"]["VOLUME"],
					'VALUE' => $basketItem["volume"],
				),
				array(
					'NAME' => 'Товар из конструктора',
					'CODE' => 'IS_FROM_BUILDER',
					'VALUE' => 'Y',
				),
			);

			$quantity = $basketItem["length"];

			if ($item = $basket->getExistsItem('catalog', $basketItem["id"], $basketProps)){
				//Обновление товара в корзине
				$item->setField('QUANTITY', $item->getQuantity() + $quantity);
			}else{
				// Добавление товара
				$item = $basket->createItem('catalog', $basketItem["id"]);
				$item->setFields([
					'QUANTITY' => $quantity,
					'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
					'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
					'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
					'PRODUCT_XML_ID' => $basketItem["productXmlId"],
					'CATALOG_XML_ID' => $basketItem["catalogXmlId"],
				]);
				// Сохранение изменений
				$item->getPropertyCollection()->setProperty($basketProps);
			}
		}
		$basket->save();

		return true;
	}



	/*	====================	*/
	/*	Графические функции	*/
	/**
	 * Отрисовка картинки карниза
	 * @param array("cornice" => <массив из angular объекта Cornice>, "filter" => <массив из angular объекта Filter>)
	 * @return картинка сборки карниза
	 */
	public function getCorniceImg($args){
		$GD = new Graphics($this->arParams);
		if($imgSrc = $GD->getCorniceImg($args)){
			return $imgSrc;
		}else{
			return false;
		}
	}

	/**
	 * Удаление картинки карниза из кэша для обновления
	 * @param array("imgSrc" => imgSrc)
	 * @return true or false
	 */
	public function updateImg($args){
		$GD = new Graphics($this->arParams);
		return $GD->updateImg($args);
	}

}
?>
