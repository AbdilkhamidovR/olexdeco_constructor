<?
namespace Ra\Constructor;

$start = microtime(true);

require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";

require_once "cornice.class.php";

$arErrors = array();
$arMess = array();

$arRequest = json_decode(file_get_contents('php://input'), TRUE);

$arResponse = array(
	"error" => $arErrors,
	"msg" =>  $arMess
);

$CCornice = new Cornice();

$arResponse['arItems'] = call_user_func(array($CCornice, $arRequest["act"]), $arRequest["filter"], $arRequest["params"]);

$time = microtime(true) - $start;
$arResponse["times"] = (int)($time * 1000);

echo json_encode($arResponse);
?>