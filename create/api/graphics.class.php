<?
namespace Ra\Constructor;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
error_reporting(E_ERROR);

use Bitrix\Main\Loader;

Loader::includeModule('iblock');


class Graphics {

	private $arParams = array();
	private $arResult = array();

	function __construct($arParams) {
		$this->arParams = $arParams;
	}

	/**
	 * Поиск фотки трубы для конструктора среди ТП с таким-же цветом, формой и типом (диаметром и др.) трубы
	 *	берем первую найденную		
	 */
	protected function getTubeBuilderPhoto($tubeId){
		//--
		$arSelect = array(
			"IBLOCK_ID", "ID", "NAME",
			"PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"],
			"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_TUBE_PHOTO"]
		);
		$arSort = array('SORT' => 'ASC', 'ID' => 'ASC');
		//--
		$arItem = \CIBlockElement::GetList(
				$arSort, 
				array("IBLOCK_ID" => $this->arParams["IBLOCK_OFFERS_ID"], "ID" => $tubeId), 
				false, 
				array("nTopCount"=>1), 
				$arSelect
			) -> GetNext(true, false);
		//--
		$arFilter = array(
			"IBLOCK_ID" => $this->arParams["IBLOCK_OFFERS_ID"],
			"INCLUDE_SUBSECTIONS" => "Y",
			"ACTIVE" => "Y",
			"ACTIVE_DATE" => "Y",
			"CATALOG_AVAILABLE" => "Y", // учитывать остатки на складе
		);
		$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"] ] = $arItem["PROPERTY_".$this->arParams["PROPERTIES"]["COLOR"]."_VALUE"];
		$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"] ] = $arItem["PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_FORM"]."_ENUM_ID"];
		$arFilter[ "PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"] ] = $arItem["PROPERTY_".$this->arParams["PROPERTIES"]["TUBE_TYPE"]."_VALUE"];
		$arFilter[ "!PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_TUBE_PHOTO"] ] = false;

		//--
		$arRes = \CIBlockElement::GetList(
			$arSort, 
			$arFilter, 
			false, 
			array("nTopCount"=>1), 
			$arSelect
		)->GetNext(true, false);

		return array_merge(
					array("NAME" => $arItem["NAME"], "TUBE_ID" => $tubeId),
					\CFile::GetFileArray($arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_TUBE_PHOTO"]."_VALUE"])
				);
	}


	/**
	 * Отрисовка картинки карниза
	 * @param array("cornice" => <массив из angular объекта Cornice>, "filter" => <массив из angular объекта Filter>)
	 * @return картинка сборки карниза
	 */
	public function getCorniceImg($args){
		$this->arResult["CORNICE_IMAGE"] = array();

		$fname = $this->getCorniceImgName($args['cornice']);
		$fpath = $this->getCorniceImgPath();
		$this->arResult["CORNICE_IMAGE"]["SRC"] = $fpath.$fname;

		/* возвращаем путь к картинке и входим из функции, если нужная существует */
		if($this->isCorniceImgExists($args['cornice'])){
			return $this->arResult["CORNICE_IMAGE"]["SRC"];
		}

		/* рендер новой картинки */
		// Заполнение графических слоев
		// Получение слоев труб
		foreach ($args['cornice']['tubes'] as $key => $tube) {
			$tubeId = $tube["id"];
			if($tubeId){
				$this->arResult["CORNICE_IMAGE"]["TUBE"][] = array_merge(
					$this->getTubeBuilderPhoto($tubeId),
					array("SCALE" => 1 - $this->arParams["RENDER"]["KP"] * $key)
				);
			}
		}

		// Получение слоев наконечников (главного и заглушек на доп. ряды)
		$arO = array("CREATED"=>"DESC", "ID"=>"ASC");
		$arN = array("nTopCount"=>1);
		$arF = array("IBLOCK_ID" => $this->arParams["IBLOCK_OFFERS_ID"]);

		foreach ($args['cornice']['caps'] as $key => $cap) {
			$arF["ID"] = $cap["id"];
			$arS = array("IBLOCK_ID", "ID", "NAME", 
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_CAP_PHOTO"],
				"PROPERTY_BUILDER_CAPS_dX_C",
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_CAP_Y"],
				);
			$arRes = \CIBlockElement::GetList($arO, $arF, false, $arN, $arS) -> GetNext(true, false);
			$this->arResult["CORNICE_IMAGE"]["CAP"][] = array_merge(
				array("NAME" => $arRes["NAME"], "CAP_ID" => $arRes["ID"]),
				\CFile::GetFileArray($arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_CAP_PHOTO"]."_VALUE"]),
				array(
					"TUBE_DOCK_X" => $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_CAP_X"]."_VALUE"],
					"TUBE_DOCK_Y" => $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_CAP_Y"]."_VALUE"],
					"SCALE" => 1 - $this->arParams["RENDER"]["KP"] * $key
				)
			);
		}

		// Получение слоев кронштейна
		$arF = array(
			"IBLOCK_ID" => $this->arParams["IBLOCK_OFFERS_ID"], 
			"ID"=>$args['cornice']['mounts'][0]["id"]
		);
		$arS = array("IBLOCK_ID", "ID", "NAME",
			"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][0],
			"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][1],
			"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][2],
			"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][3],
			"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_Y"][0],
			"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_Y"][1],
			"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_Y"][2],
			"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_LEFT_SPAN"],
			);
		$arRes = \CIBlockElement::GetList($arO, $arF, false, $arN, $arS) -> getNext(true, false);
		
		$this->arResult["CORNICE_IMAGE"]["MOUNT"] = array(
			array_merge(
				array("NAME" => $arRes["NAME"], "MOUNT_ID" => $arRes["ID"]),
				\CFile::GetFileArray($arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][3]."_VALUE"]), 
				array("TUBE_DOCK_Y" => false)
			),
			array_merge(
				array("NAME" => $arRes["NAME"], "MOUNT_ID" => $arRes["ID"]),
				\CFile::GetFileArray($arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][2]."_VALUE"]), 
				array(
					"TUBE_DOCK_X" => ($arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_LEFT_SPAN"]."_VALUE"] ? $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_LEFT_SPAN"]."_VALUE"] : $this->arParams["RENDER"]["dX_M"]), 
					"TUBE_DOCK_Y" => $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_Y"][0]."_VALUE"]
				)
			),
			array_merge(
				array("NAME" => $arRes["NAME"], "MOUNT_ID" => $arRes["ID"]),
				\CFile::GetFileArray($arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][1]."_VALUE"]), 
				array("TUBE_DOCK_Y" => $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_Y"][1]."_VALUE"])
			),
			array_merge(
				array("NAME" => $arRes["NAME"], "MOUNT_ID" => $arRes["ID"]),
				\CFile::GetFileArray($arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_PHOTO_LAYERS"][0]."_VALUE"]), 
				array("TUBE_DOCK_Y" => $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_MOUNT_ROWS_Y"][2]."_VALUE"])
			),
		);

	
		// Получение слоев колец для труб или бегунков для профилей
		$i = 0;
		foreach ($args['cornice']['rings'] as $key => $ring) {
			$arF["ID"] = $ring["id"];
			$arS = array("IBLOCK_ID", "ID", "NAME", 
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_RING_PHOTO"][0],
				"PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_RING_Y"],
				);
			$arRes = \CIBlockElement::GetList($arO, $arF, false, $arN, $arS) -> GetNext(true, false);
			$this->arResult["CORNICE_IMAGE"]["RING"][$i][0] = array_merge(
				array("NAME" => $arRes["NAME"], "RING_ID" => $arRes["ID"]),
				\CFile::GetFileArray($arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_RING_PHOTO"][0]."_VALUE"]),
				array(
					"TUBE_DOCK_Y" => $arRes["PROPERTY_".$this->arParams["PROPERTIES"]["BUILDER_RING_Y"]."_VALUE"],
					"SCALE" => 1 - $this->arParams["RENDER"]["KP"] * $i,
					"TYPE" => $ring["ringType"],
				)
			);
			$i++;
		}
		if($imgSrc = $this->renderCornice()){
			return $imgSrc;
		}else{
			return false;
		}
	}

	/**
	 * Возвращает сгенерированное имя файла картинки на основании переданного фильтра
	 * @return имя файла картинки
	 */
	protected function getCorniceImgName($args){
		$str = "c";
		foreach ($args['caps'] as $key => $value) {
			$str .= "_".$value["id"];
		}
		$str .= "_m";
		foreach ($args['mounts'] as $key => $value) {
			$str .= "_".$value["id"];
		}
		$str .= "_r";
		foreach ($args['rings'] as $key => $value) {
			$str .= "_".$value["id"];
		}
		$str .= "_t";
		foreach ($args['tubes'] as $key => $value) {
			$str .= "_".$value["id"];
		}
		$fname = $str.".".$this->arParams["RENDER"]["IMG_EXT"];
		return $fname;
	}

	/**
	 * Возвращает сгенерированное имя файла картинки на основании переданного фильтра
	 * @return имя файла картинки
	 */
	protected function getCorniceImgPath(){
		$fpath = "/".\Bitrix\Main\Config\Option::get("main", "upload_dir").$this->arParams["RENDER"]["PATH"];
		if(!is_dir($this->arParams["APP_DIR"].$fpath)){
			mkdir($this->arParams["SITE_DIR"].$fpath, 0755);
		}
		return $fpath;
	}

	/**
	 * Проверяет существование картинки для указанного в args фильтра
	 * @return картинка сборки карниза  или false при её отсутствии
	 */
	public function isCorniceImgExists($args){
		$fname = $this->getCorniceImgName($args);
		$fpath = $this->getCorniceImgPath();
		$imgSrc = $fpath.$fname;

		// возвращаем путь к картинке и входим из функции, если нужная существует
		if (file_exists($this->arParams["SITE_DIR"].$imgSrc)) {
			return $imgSrc;
		}else{
			return false;
		}
	}

	/**
	 * Отрисовка карниза
	 *  @param $this->arResult["CORNICE_IMAGE"]
	 *  @return image source without domain or false
	 */
	protected function renderCornice(){
		// Background
		// Расчет размеров
		$this->arResult["CORNICE_IMAGE"]["WIDTH"] = $this->arParams["RENDER"]["W"];
		
		// Расчет высоты выше оси первой трубы и ниже оси первой трубы
		// у кронштейна
		$this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["h1"] = $this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["TUBE_DOCK_Y"] - .5*$this->arResult["CORNICE_IMAGE"]["TUBE"][0]["HEIGHT"];
		$this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["h2"] = $this->arResult["CORNICE_IMAGE"]["MOUNT"][0]["HEIGHT"] - $this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["TUBE_DOCK_Y"] + .5*$this->arResult["CORNICE_IMAGE"]["TUBE"][0]["HEIGHT"];
		// у главного наконечника
		$this->arResult["CORNICE_IMAGE"]["CAP"][0]["h1"] = $this->arResult["CORNICE_IMAGE"]["CAP"][0]["TUBE_DOCK_Y"];
		$this->arResult["CORNICE_IMAGE"]["CAP"][0]["h2"] = $this->arResult["CORNICE_IMAGE"]["CAP"][0]["HEIGHT"] - $this->arResult["CORNICE_IMAGE"]["CAP"][0]["TUBE_DOCK_Y"];

		// у кольца
		if($this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TYPE"] == "RING"){
			$this->arResult["CORNICE_IMAGE"]["RING"][0][0]["h2"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["HEIGHT"] - $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TUBE_DOCK_Y"] - .5*$this->arResult["CORNICE_IMAGE"]["TUBE"][0]["HEIGHT"];
		}
		if($this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TYPE"] == "RUNNER"){
			$this->arResult["CORNICE_IMAGE"]["RING"][0][0]["h2"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["HEIGHT"] - $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TUBE_DOCK_Y"] + .5*$this->arResult["CORNICE_IMAGE"]["TUBE"][0]["HEIGHT"];
		}

		// расчет высоты
		$H1 = max($this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["h1"], $this->arResult["CORNICE_IMAGE"]["CAP"][0]["h1"]);
		$H2 = max($this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["h2"], $this->arResult["CORNICE_IMAGE"]["CAP"][0]["h2"], $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["h2"]);

		$this->arResult["CORNICE_IMAGE"]["H1"] = $H1;
		$this->arResult["CORNICE_IMAGE"]["H2"] = $H2;
		$this->arResult["CORNICE_IMAGE"]["HEIGHT"] = $H1 + $H2 + $this->arParams["RENDER"]["SPAN"];

		$this->arResult["CORNICE_IMAGE"]["BACKGROUND"] = array(
			"NAME" => "Background",
			"DST_WIDTH" => $this->arParams["RENDER"]["CANVAS_W"],
			"DST_HEIGHT" => $this->arParams["RENDER"]["CANVAS_H"],
			"SRC" => $this->arParams["APP_DIR"].$this->arParams["RENDER"]["BG_SRC"],
		);
		list($this->arResult["CORNICE_IMAGE"]["BACKGROUND"]["WIDTH"], $this->arResult["CORNICE_IMAGE"]["BACKGROUND"]["HEIGHT"]) = getimagesize($this->arParams["SITE_DIR"].$this->arResult["CORNICE_IMAGE"]["BACKGROUND"]["SRC"]);

		//Watermark
		$this->arResult["CORNICE_IMAGE"]["WATERMARK"] = array(
			"NAME" => "Watermark",
			"DST_WIDTH" => $this->arParams["RENDER"]["CANVAS_W"],
			"DST_HEIGHT" => $this->arParams["RENDER"]["CANVAS_H"],
			"SRC" => $this->arParams["APP_DIR"].$this->arParams["RENDER"]["WATERMARK_SRC"],
		);
		list($this->arResult["CORNICE_IMAGE"]["WATERMARK"]["WIDTH"], $this->arResult["CORNICE_IMAGE"]["WATERMARK"]["HEIGHT"]) = getimagesize($this->arParams["SITE_DIR"].$this->arResult["CORNICE_IMAGE"]["WATERMARK"]["SRC"]);


		// расстановка координат
		// с учетом перспективных искажений
		$this->arResult["CORNICE_IMAGE"]["BACKGROUND"]["POS"] = array("X" => 0, "Y" => 0);

		if($this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["h1"] >= $this->arResult["CORNICE_IMAGE"]["CAP"][0]["h1"]){
			$mountY = round(.5* $this->arParams["RENDER"]["SPAN"], 2);
		}else{
			$mountY = $this->arResult["CORNICE_IMAGE"]["CAP"][0]["h1"] - $this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["h1"] + round(.5* $this->arParams["RENDER"]["SPAN"], 2);
		}
		//mounts
		$this->arResult["CORNICE_IMAGE"]["MOUNT"][0]["POS"] = array(
			"X" => $this->arResult["CORNICE_IMAGE"]["CAP"][0]["WIDTH"] + $this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["TUBE_DOCK_X"],
			"Y" => $mountY
		);
		$this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["POS"] = array(
			"X" => $this->arResult["CORNICE_IMAGE"]["CAP"][0]["WIDTH"] + $this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["TUBE_DOCK_X"],
			"Y" => $mountY
		);
		if($this->arResult["CORNICE_IMAGE"]["MOUNT"][2]){
			$this->arResult["CORNICE_IMAGE"]["MOUNT"][2]["POS"] = array(
				"X" => $this->arResult["CORNICE_IMAGE"]["CAP"][0]["WIDTH"] + $this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["TUBE_DOCK_X"],
				"Y" => $mountY
			);
		}
		if($this->arResult["CORNICE_IMAGE"]["MOUNT"][3]){
			$this->arResult["CORNICE_IMAGE"]["MOUNT"][3]["POS"] = array(
				"X" => $this->arResult["CORNICE_IMAGE"]["CAP"][0]["WIDTH"] + $this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["TUBE_DOCK_X"],
				"Y" => $mountY
			);
		}
		//tubes
		$this->arResult["CORNICE_IMAGE"]["TUBE"][0]["POS"] = array(
			"X" => $this->arResult["CORNICE_IMAGE"]["CAP"][0]["WIDTH"] - ($this->arResult["CORNICE_IMAGE"]["CAP"][0]["TUBE_DOCK_X"] ? $this->arResult["CORNICE_IMAGE"]["CAP"][0]["TUBE_DOCK_X"] : $this->arParams["RENDER"]["dX_C"]), 
			"Y" => $this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["TUBE_DOCK_Y"] + $this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["POS"]["Y"] - $this->arResult["CORNICE_IMAGE"]["TUBE"][0]["HEIGHT"]
		);
		if($this->arResult["CORNICE_IMAGE"]["TUBE"][1]){
			$this->arResult["CORNICE_IMAGE"]["TUBE"][1]["POS"] = array(
				"X" => $this->arResult["CORNICE_IMAGE"]["TUBE"][0]["POS"]["X"] - ($this->arParams["RENDER"]["dX_T1"] - ($this->arResult["CORNICE_IMAGE"]["CAP"][0]["TUBE_DOCK_X"] ? $this->arResult["CORNICE_IMAGE"]["CAP"][0]["TUBE_DOCK_X"] : $this->arParams["RENDER"]["dX_C"])), 
				"Y" => $this->arResult["CORNICE_IMAGE"]["MOUNT"][2]["TUBE_DOCK_Y"] + $this->arResult["CORNICE_IMAGE"]["MOUNT"][2]["POS"]["Y"] - $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["HEIGHT"]
			);
		}
		if($this->arResult["CORNICE_IMAGE"]["TUBE"][2]){
			$this->arResult["CORNICE_IMAGE"]["TUBE"][2]["POS"] = array(
				"X" => $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["POS"]["X"] - $this->arParams["RENDER"]["dX_T12"], 
				"Y" => $this->arResult["CORNICE_IMAGE"]["MOUNT"][3]["TUBE_DOCK_Y"] + $this->arResult["CORNICE_IMAGE"]["MOUNT"][3]["POS"]["Y"] - $this->arResult["CORNICE_IMAGE"]["TUBE"][2]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["TUBE"][2]["HEIGHT"]
			);
		}
		// caps
		$this->arResult["CORNICE_IMAGE"]["CAP"][0]["POS"] = array(
			"X" => 0, 
			"Y" => $this->arResult["CORNICE_IMAGE"]["TUBE"][0]["POS"]["Y"] + .5 * $this->arResult["CORNICE_IMAGE"]["TUBE"][0]["HEIGHT"] - $this->arResult["CORNICE_IMAGE"]["CAP"][0]["TUBE_DOCK_Y"]
		);
		if($this->arResult["CORNICE_IMAGE"]["CAP"][1]){
			$this->arResult["CORNICE_IMAGE"]["CAP"][1]["POS"] = array(
				// "X" =>  $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["POS"]["X"] - $this->arResult["CORNICE_IMAGE"]["CAP"][1]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["CAP"][1]["WIDTH"] + $this->arParams["RENDER"]["dX_C"], 
				"X" =>  $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["POS"]["X"] - $this->arResult["CORNICE_IMAGE"]["CAP"][1]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["CAP"][1]["WIDTH"] + $this->arParams["RENDER"]["dX_C1"], 
				"Y" => $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["POS"]["Y"] - .5 * $this->arResult["CORNICE_IMAGE"]["CAP"][1]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["CAP"][1]["HEIGHT"] + .5 * $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["HEIGHT"]
			);
		}
		if($this->arResult["CORNICE_IMAGE"]["CAP"][2]){
			$this->arResult["CORNICE_IMAGE"]["CAP"][2]["POS"] = array(
				"X" =>  $this->arResult["CORNICE_IMAGE"]["TUBE"][2]["POS"]["X"] - $this->arResult["CORNICE_IMAGE"]["CAP"][2]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["CAP"][2]["WIDTH"] + $this->arParams["RENDER"]["dX_C1"], 
				"Y" => $this->arResult["CORNICE_IMAGE"]["TUBE"][2]["POS"]["Y"] - .5 * $this->arResult["CORNICE_IMAGE"]["CAP"][2]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["CAP"][2]["HEIGHT"] + .5 * $this->arResult["CORNICE_IMAGE"]["TUBE"][2]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["TUBE"][2]["HEIGHT"]
			);
		}

		// кольца для труб или бегунки для профилей
		// row 1
		if($this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TYPE"] == "RING"){
			$this->arResult["CORNICE_IMAGE"]["RING"][0][0]["POS"] = array(
				"X" => $this->arResult["CORNICE_IMAGE"]["MOUNT"][0]["POS"]["X"] + $this->arResult["CORNICE_IMAGE"]["MOUNT"][0]["WIDTH"] + .8*$this->arResult["CORNICE_IMAGE"]["RING"][0][0]["WIDTH"], 
				"Y" => $this->arResult["CORNICE_IMAGE"]["TUBE"][0]["POS"]["Y"] - $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TUBE_DOCK_Y"]
			);
			$this->arResult["CORNICE_IMAGE"]["RING"][0][1] = $this->arResult["CORNICE_IMAGE"]["RING"][0][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][0][2] = $this->arResult["CORNICE_IMAGE"]["RING"][0][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][0][1]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["POS"]["X"] - .8*$this->arResult["CORNICE_IMAGE"]["RING"][0][0]["WIDTH"];
			$this->arResult["CORNICE_IMAGE"]["RING"][0][2]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["CAP"][0]["POS"]["X"] +  $this->arResult["CORNICE_IMAGE"]["CAP"][0]["WIDTH"] + .3*$this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["TUBE_DOCK_X"];
		}
		if($this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TYPE"] == "RUNNER"){
			$this->arResult["CORNICE_IMAGE"]["RING"][0][0]["POS"] = array(
				"X" => $this->arResult["CORNICE_IMAGE"]["MOUNT"][0]["POS"]["X"] + $this->arResult["CORNICE_IMAGE"]["MOUNT"][0]["WIDTH"] + 2 * $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["WIDTH"], 
				"Y" => $this->arResult["CORNICE_IMAGE"]["TUBE"][0]["POS"]["Y"] + $this->arResult["CORNICE_IMAGE"]["TUBE"][0]["HEIGHT"] - $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TUBE_DOCK_Y"]
			);
			$this->arResult["CORNICE_IMAGE"]["RING"][0][1] = $this->arResult["CORNICE_IMAGE"]["RING"][0][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][0][2] = $this->arResult["CORNICE_IMAGE"]["RING"][0][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][0][1]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["POS"]["X"] + $this->arResult["CORNICE_IMAGE"]["MOUNT"][0]["WIDTH"];
			$this->arResult["CORNICE_IMAGE"]["RING"][0][2]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["CAP"][0]["POS"]["X"] + $this->arResult["CORNICE_IMAGE"]["CAP"][0]["WIDTH"] + .3*$this->arResult["CORNICE_IMAGE"]["MOUNT"][1]["TUBE_DOCK_X"];
		}

		// row 2
		if($this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TYPE"] == "RING"){
			$this->arResult["CORNICE_IMAGE"]["RING"][1][0]["POS"] = array(
				"X" => $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["POS"]["X"] - $this->arParams["RENDER"]["dX_R1"], 
				"Y" => $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["POS"]["Y"] - $this->arResult["CORNICE_IMAGE"]["RING"][1][0]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["RING"][1][0]["TUBE_DOCK_Y"]
			);
			$this->arResult["CORNICE_IMAGE"]["RING"][1][1] = $this->arResult["CORNICE_IMAGE"]["RING"][1][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][1][2] = $this->arResult["CORNICE_IMAGE"]["RING"][1][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][1][1]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][1]["POS"]["X"] - (1-$this->arParams["RENDER"]["KP"])*$this->arParams["RENDER"]["dX_R1"];
			$this->arResult["CORNICE_IMAGE"]["RING"][1][2]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][2]["POS"]["X"] + $this->arParams["RENDER"]["dX_LR1"];
		}
		if($this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TYPE"] == "RUNNER"){
			$this->arResult["CORNICE_IMAGE"]["RING"][1][0]["POS"] = array(
				"X" => $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["POS"]["X"] + $this->arResult["CORNICE_IMAGE"]["RING"][0][1]["WIDTH"] - $this->arParams["RENDER"]["dX_R1"], 
				"Y" => $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["POS"]["Y"] + $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["TUBE"][1]["HEIGHT"] - $this->arResult["CORNICE_IMAGE"]["RING"][1][0]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["RING"][1][0]["TUBE_DOCK_Y"]
			);
			$this->arResult["CORNICE_IMAGE"]["RING"][1][1] = $this->arResult["CORNICE_IMAGE"]["RING"][1][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][1][2] = $this->arResult["CORNICE_IMAGE"]["RING"][1][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][1][1]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][1]["POS"]["X"] + $this->arResult["CORNICE_IMAGE"]["RING"][0][1]["WIDTH"] - $this->arParams["RENDER"]["dX_R1"];
			$this->arResult["CORNICE_IMAGE"]["RING"][1][2]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][2]["POS"]["X"] + $this->arParams["RENDER"]["dX_LR1"];
		}

		// row 3
		if($this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TYPE"] == "RING"){
			$this->arResult["CORNICE_IMAGE"]["RING"][2][0]["POS"] = array(
				"X" => $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["POS"]["X"] - $this->arParams["RENDER"]["dX_R2"], 
				"Y" => $this->arResult["CORNICE_IMAGE"]["TUBE"][2]["POS"]["Y"] - $this->arResult["CORNICE_IMAGE"]["RING"][2][0]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["RING"][2][0]["TUBE_DOCK_Y"]
			);
			$this->arResult["CORNICE_IMAGE"]["RING"][2][1] = $this->arResult["CORNICE_IMAGE"]["RING"][2][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][2][2] = $this->arResult["CORNICE_IMAGE"]["RING"][2][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][2][1]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][1]["POS"]["X"] - (1-2*$this->arParams["RENDER"]["KP"])*$this->arParams["RENDER"]["dX_R2"];
			$this->arResult["CORNICE_IMAGE"]["RING"][2][2]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][2]["POS"]["X"] + $this->arParams["RENDER"]["dX_LR2"];
		}
		if($this->arResult["CORNICE_IMAGE"]["RING"][0][0]["TYPE"] == "RUNNER"){
			$this->arResult["CORNICE_IMAGE"]["RING"][2][0]["POS"] = array(
				"X" => $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["POS"]["X"] - $this->arParams["RENDER"]["dX_R2"], 
				"Y" => $this->arResult["CORNICE_IMAGE"]["TUBE"][2]["POS"]["Y"] + $this->arResult["CORNICE_IMAGE"]["TUBE"][2]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["TUBE"][2]["HEIGHT"] - $this->arResult["CORNICE_IMAGE"]["RING"][2][0]["SCALE"] * $this->arResult["CORNICE_IMAGE"]["RING"][2][0]["TUBE_DOCK_Y"]
			);
			$this->arResult["CORNICE_IMAGE"]["RING"][2][1] = $this->arResult["CORNICE_IMAGE"]["RING"][2][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][2][2] = $this->arResult["CORNICE_IMAGE"]["RING"][2][0];
			$this->arResult["CORNICE_IMAGE"]["RING"][2][1]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][1]["POS"]["X"] - $this->arParams["RENDER"]["dX_R2"];
			$this->arResult["CORNICE_IMAGE"]["RING"][2][2]["POS"]["X"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][2]["POS"]["X"];
		}

		// Подбор ширины рендера
		// ориентипуемся по третьему кольцу 1-го ряда
		$this->arResult["CORNICE_IMAGE"]["WIDTH"] = $this->arResult["CORNICE_IMAGE"]["RING"][0][0]["POS"]["X"] + $this->arResult["CORNICE_IMAGE"]["RING"][0][2]["WIDTH"] + $this->arParams["RENDER"]["SPAN"];

		$aspect_canvas = round($this->arParams["RENDER"]["CANVAS_W"] / $this->arParams["RENDER"]["CANVAS_H"], 2);
		$aspect_image = round($this->arResult["CORNICE_IMAGE"]["WIDTH"] / $this->arResult["CORNICE_IMAGE"]["HEIGHT"], 2);

		$this->arResult["CORNICE_IMAGE"]["HEIGHT_0"] = $this->arResult["CORNICE_IMAGE"]["HEIGHT"];
		if($aspect_image > $aspect_canvas){
			$this->arResult["CORNICE_IMAGE"]["HEIGHT"] = $this->arResult["CORNICE_IMAGE"]["WIDTH"] * round(1/$aspect_canvas, 2);
		}else{
			$this->arResult["CORNICE_IMAGE"]["WIDTH"] = $this->arResult["CORNICE_IMAGE"]["HEIGHT"] * $aspect_canvas;
		}

		$this->arResult["CORNICE_IMAGE"]["WATERMARK"]["POS"] = array("X" => 0, "Y" => 0);

		/* Cборка слоев рендера */
		// порядок: от стены
		$this->arResult["CORNICE_IMAGE"]["LAYERS"] = array(
			// $this->arResult["CORNICE_IMAGE"]["BACKGROUND"],
			$this->arResult["CORNICE_IMAGE"]["MOUNT"][3],
			$this->arResult["CORNICE_IMAGE"]["TUBE"][2],
			$this->arResult["CORNICE_IMAGE"]["RING"][2][0],
			$this->arResult["CORNICE_IMAGE"]["RING"][2][1],
			$this->arResult["CORNICE_IMAGE"]["RING"][2][2],
			$this->arResult["CORNICE_IMAGE"]["CAP"][2],
			$this->arResult["CORNICE_IMAGE"]["MOUNT"][2],
			$this->arResult["CORNICE_IMAGE"]["TUBE"][1],
			$this->arResult["CORNICE_IMAGE"]["RING"][1][0],
			$this->arResult["CORNICE_IMAGE"]["RING"][1][1],
			$this->arResult["CORNICE_IMAGE"]["RING"][1][2],
			$this->arResult["CORNICE_IMAGE"]["CAP"][1],
			$this->arResult["CORNICE_IMAGE"]["MOUNT"][1],
			$this->arResult["CORNICE_IMAGE"]["TUBE"][0],
			$this->arResult["CORNICE_IMAGE"]["CAP"][0],
			$this->arResult["CORNICE_IMAGE"]["RING"][0][0],
			$this->arResult["CORNICE_IMAGE"]["RING"][0][1],
			$this->arResult["CORNICE_IMAGE"]["RING"][0][2],
			$this->arResult["CORNICE_IMAGE"]["MOUNT"][0],
			// $this->arResult["CORNICE_IMAGE"]["WATERMARK"]
		);

		// Pендер картинки
		// создание прозрачного полотна
		$im = imagecreatetruecolor($this->arResult["CORNICE_IMAGE"]["WIDTH"], $this->arResult["CORNICE_IMAGE"]["HEIGHT"]);
		imagesavealpha($im, true);
		imagealphablending($im, true);
		$bg = imagecolorallocatealpha($im, 255, 255, 255, 127);
		imagefill($im, 0, 0, $bg);
		
		// рендер слоев
		foreach ($this->arResult["CORNICE_IMAGE"]["LAYERS"] as $key => $arLayer) {
			if($arLayer["SRC"]){
				$im = $this->renderLayer($im, $arLayer);
			}
		}

		$im = $this->resizeToCanvas($im);
		$im = $this->renderLayer($im, $this->arResult["CORNICE_IMAGE"]["WATERMARK"]);

		switch ($this->arParams["RENDER"]["IMG_EXT"]) {
			case 'jpg':
				if(imagejpeg($im, $this->arParams["SITE_DIR"].$this->arResult["CORNICE_IMAGE"]["SRC"])){
					return $this->arResult["CORNICE_IMAGE"]["SRC"];
				}else{
					return false;
				}
				break;

			case 'png':
				if(imagepng($im, $this->arParams["SITE_DIR"].$this->arResult["CORNICE_IMAGE"]["SRC"])){
					return $this->arResult["CORNICE_IMAGE"]["SRC"];
				}else{
					return false;
				}
				break;
			
			default:
				return false;
				break;
		}
	}

	/**
	 * Отрисовка слоя карниза
	 *  @param $im - ресурс картинки
	 *  @param $layer - array("SRC" = <Url к картинке слоя без домена>, "WIDTH" => <ширина картинки в слое>, "HEIGHT" => <высота картинки в слое>)
	 */
	protected function renderLayer($im, $layer){
		if($layer["SRC"]){
			$gdLayer = imagecreatefrompng($this->arParams["SITE_DIR"].$layer["SRC"]);
			$dst_x = $layer["POS"]["X"];
			$dst_y = $layer["POS"]["Y"];
			$src_w = $layer["WIDTH"];
			$src_h = $layer["HEIGHT"];
			$scale = $layer["SCALE"] ? $layer["SCALE"] : 1;
			$dst_w = $scale * ($layer["DST_WIDTH"] ? $layer["DST_WIDTH"] : $layer["WIDTH"]);
			$dst_h = $scale * ($layer["DST_HEIGHT"] ? $layer["DST_HEIGHT"] : $layer["HEIGHT"]);
			imagecopyresampled($im, $gdLayer, $dst_x, $dst_y, 0, 0, $dst_w, $dst_h, $src_w, $src_h);
			imagedestroy($gdLayer);
		}
		return $im;
	}

	/**
	 * Отрисовка слоя карниза
	 */
	protected function resizeToCanvas($im){
		$canvas = imagecreatetruecolor($this->arParams["RENDER"]["CANVAS_W"], $this->arParams["RENDER"]["CANVAS_H"]);
		imagesavealpha($canvas, true);
		imagealphablending($canvas, true);
		$bg = imagecolorallocatealpha($canvas, 255, 255, 255, 127);
		imagefill($canvas, 0, 0, $bg);

		$canvas = $this->renderLayer($canvas, $this->arResult["CORNICE_IMAGE"]["BACKGROUND"]);

		$dst_x = 20;
		$dst_y = 0;
		$src_w = $this->arResult["CORNICE_IMAGE"]["WIDTH"];
		$src_h = $this->arResult["CORNICE_IMAGE"]["HEIGHT"];
		$scale = round($this->arParams["RENDER"]["CANVAS_W"] / $this->arResult["CORNICE_IMAGE"]["WIDTH"], 2);
		$dst_w = $this->arParams["RENDER"]["CANVAS_W"];
		$dst_h = round($scale * $src_h, 2);
		$dst_dy = round($scale * ($this->arResult["CORNICE_IMAGE"]["HEIGHT"] - $this->arResult["CORNICE_IMAGE"]["HEIGHT_0"]), 2);
		
		$dst_y = round(.5 * ($this->arParams["RENDER"]["CANVAS_H"] - $dst_h) ,2) + round(.5 * $dst_dy, 2);

		imagecopyresampled($canvas, $im, $dst_x, $dst_y, 0, 0, $dst_w, $dst_h, $src_w, $src_h);
		return $canvas;
	}
	
	/**
	 * Удаление картинки карниза из кэша для обновления
	 * Доступно только админу
	 * @param array("imgSrc" => imgSrc)
	 * @return true or false
	 */
	public function updateImg($args){
		list($url_hiRes, $ver) = explode("?", $args["imgSrc"]);
		$url_lowRes = preg_replace('/hi_res\/(.*)\.png/', "\$1.jpg", $url_hiRes);
		$file_low =$this->arParams["SITE_DIR"].$url_lowRes;
		$file_hiRes = $this->arParams["SITE_DIR"].$url_hiRes;
		if(file_exists($file_low)){
			unlink($file_low);
		}
		if(file_exists($file_hiRes)){
			unlink($file_hiRes);
		}
		return true;
	}

}
?>
