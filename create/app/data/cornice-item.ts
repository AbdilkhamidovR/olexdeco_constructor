export class CorniceItem {
  constructor(
	public id: number,
	public productId: number,
	public name: string = "",
	public previewImgSrc: string = "",
	public basketImgSrc: string = "",
	public length: number = 1, // кол-во в корзине
	public price: number = 0,
	public currency: string = "руб.",
	public priceFormatted: string = "",
	public baseUnit: string = "",
	public tubeTypeIds: any[] = [],
	public tubeLength: number = 0,
	public weight: string = "",
	public volume: string = "",
	public colorId: string = "",
	public tubeTypeId: string = "",
	public ringType: string = "", // RING у труб или RUNNER у профилей
  ) { }
}