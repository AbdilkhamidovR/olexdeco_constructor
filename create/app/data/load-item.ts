import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { CorniceItem }	from './cornice-item';
export class LoadItem {
  constructor(
	public corniceKey: string,
	public corniceIndx: number,
	public name: string ,
	public func: string,
	public subject: Subject<CorniceItem>,
	public subscription: Subscription,
  ) { }
}