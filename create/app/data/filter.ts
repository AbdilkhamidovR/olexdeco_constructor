/**
 * фильтр для выборки элементов карниза
 */
export class Filter {
	constructor(
		public nRowsId: number = 0,
		public tubeFormIds: any[] = [],
		public tubeTypeIds: any[] = [],
		public tubeLength: number = 0,
		public colorId: any = 0,
		public productId: number  = 0,
	) { }
}