"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Color = (function () {
    function Color(id, name, imgSrc) {
        this.id = id;
        this.name = name;
        this.imgSrc = imgSrc;
    }
    return Color;
}());
exports.Color = Color;
//# sourceMappingURL=color.js.map