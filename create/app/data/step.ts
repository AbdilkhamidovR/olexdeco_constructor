export class Step {
  constructor(
    public id: number,
    public title: string,
    public isActive: boolean = false,
    public isDisabled: boolean = true,
    public texts: Object = {},
  ) { }

  public setActive(){
  	this.isActive = true;
  	this.isDisabled = false;
  	return;
  }
  public setDisactive(){
  	this.isActive = false;
  	return;
  }
  public setEnable(){
  	return this.isDisabled = false;
  }
  public setDisable(){
  	return this.isDisabled = true;
  }
}