"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CorniceSide = (function () {
    function CorniceSide(type, tubeLength, tubes, // трубы в рядах
        connectors, // трубы в рядах
        imgSrc) {
        if (type === void 0) { type = ""; }
        if (tubeLength === void 0) { tubeLength = 0; }
        if (tubes === void 0) { tubes = []; }
        if (connectors === void 0) { connectors = []; }
        if (imgSrc === void 0) { imgSrc = ""; }
        this.type = type;
        this.tubeLength = tubeLength;
        this.tubes = tubes;
        this.connectors = connectors;
        this.imgSrc = imgSrc;
    }
    return CorniceSide;
}());
exports.CorniceSide = CorniceSide;
//# sourceMappingURL=cornice-side.js.map