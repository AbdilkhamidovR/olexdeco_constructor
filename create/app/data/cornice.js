"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var cornice_side_1 = require("./cornice-side");
var Cornice = (function () {
    function Cornice(id, activeStep, nRows, tubes, mounts, caps, rings, imgSrc, schemeImgSrc, 
        // для шага 6. Выбор характеристик
        sides) {
        if (nRows === void 0) { nRows = 0; }
        if (tubes === void 0) { tubes = []; }
        if (mounts === void 0) { mounts = []; }
        if (caps === void 0) { caps = []; }
        if (rings === void 0) { rings = []; }
        if (imgSrc === void 0) { imgSrc = ""; }
        if (schemeImgSrc === void 0) { schemeImgSrc = ""; }
        if (sides === void 0) { sides = {
            left: new cornice_side_1.CorniceSide("", 0, [null], [null], ""),
            right: new cornice_side_1.CorniceSide("", 0, [null], [null], "")
        }; }
        this.id = id;
        this.activeStep = activeStep;
        this.nRows = nRows;
        this.tubes = tubes;
        this.mounts = mounts;
        this.caps = caps;
        this.rings = rings;
        this.imgSrc = imgSrc;
        this.schemeImgSrc = schemeImgSrc;
        this.sides = sides;
    }
    Cornice.prototype.resetLeftSide = function () {
        this.sides.left = new cornice_side_1.CorniceSide("", 0, [null], [null], "");
    };
    Cornice.prototype.resetRightSide = function () {
        this.sides.right = new cornice_side_1.CorniceSide("", 0, [null], [null], "");
    };
    return Cornice;
}());
exports.Cornice = Cornice;
//# sourceMappingURL=cornice.js.map