"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LoadItem = (function () {
    function LoadItem(corniceKey, corniceIndx, name, func, subject, subscription) {
        this.corniceKey = corniceKey;
        this.corniceIndx = corniceIndx;
        this.name = name;
        this.func = func;
        this.subject = subject;
        this.subscription = subscription;
    }
    return LoadItem;
}());
exports.LoadItem = LoadItem;
//# sourceMappingURL=load-item.js.map