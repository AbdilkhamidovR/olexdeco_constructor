import { CorniceItem }    from './cornice-item';
export class CorniceSide {
	constructor(
		public type: string = "",
		public tubeLength: number = 0,
		public tubes: CorniceItem[] = [], // трубы в рядах
		public connectors: CorniceItem[] = [], // трубы в рядах
		public imgSrc: string = "",
	) { }
}