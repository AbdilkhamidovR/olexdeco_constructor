import { CorniceItem }    from '../data/cornice-item';
export class Basket {
  constructor(
    public id: number,
    public sum: any = 0,
    public currencyFormat: string = "",
    public sumFormatted: string = "",
    public items: object = {},
    public itemKeys: any[] = [],
    public itemLengths: object = {mounts:0, rings:0}
  ) { }

}