import { CorniceItem }    from './cornice-item';
import { CorniceSide }    from './cornice-side';
export class Cornice {
	constructor(
		public id: number,
		public activeStep: number,
		public nRows: number = 0,
		public tubes: CorniceItem[] = [],
		public mounts: CorniceItem[] = [],
		public caps: CorniceItem[] = [],
		public rings: CorniceItem[] = [],
		public imgSrc: string = "",
		public schemeImgSrc: string = "",
		// для шага 6. Выбор характеристик
		public sides: any = {
			left: new CorniceSide("", 0, [null], [null], ""),
			right: new CorniceSide("", 0, [null], [null], "")
		}
	) { }

	public resetLeftSide(){
		this.sides.left = new CorniceSide("", 0, [null], [null], "");
	}
	public resetRightSide(){
		this.sides.right = new CorniceSide("", 0, [null], [null], "");
	}

}