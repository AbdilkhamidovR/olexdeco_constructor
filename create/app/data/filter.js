"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * фильтр для выборки элементов карниза
 */
var Filter = (function () {
    function Filter(nRowsId, tubeFormIds, tubeTypeIds, tubeLength, colorId, productId) {
        if (nRowsId === void 0) { nRowsId = 0; }
        if (tubeFormIds === void 0) { tubeFormIds = []; }
        if (tubeTypeIds === void 0) { tubeTypeIds = []; }
        if (tubeLength === void 0) { tubeLength = 0; }
        if (colorId === void 0) { colorId = 0; }
        if (productId === void 0) { productId = 0; }
        this.nRowsId = nRowsId;
        this.tubeFormIds = tubeFormIds;
        this.tubeTypeIds = tubeTypeIds;
        this.tubeLength = tubeLength;
        this.colorId = colorId;
        this.productId = productId;
    }
    return Filter;
}());
exports.Filter = Filter;
//# sourceMappingURL=filter.js.map