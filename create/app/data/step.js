"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Step = (function () {
    function Step(id, title, isActive, isDisabled, texts) {
        if (isActive === void 0) { isActive = false; }
        if (isDisabled === void 0) { isDisabled = true; }
        if (texts === void 0) { texts = {}; }
        this.id = id;
        this.title = title;
        this.isActive = isActive;
        this.isDisabled = isDisabled;
        this.texts = texts;
    }
    Step.prototype.setActive = function () {
        this.isActive = true;
        this.isDisabled = false;
        return;
    };
    Step.prototype.setDisactive = function () {
        this.isActive = false;
        return;
    };
    Step.prototype.setEnable = function () {
        return this.isDisabled = false;
    };
    Step.prototype.setDisable = function () {
        return this.isDisabled = true;
    };
    return Step;
}());
exports.Step = Step;
//# sourceMappingURL=step.js.map