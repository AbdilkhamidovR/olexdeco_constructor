"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CorniceItem = (function () {
    function CorniceItem(id, productId, name, previewImgSrc, basketImgSrc, length, // кол-во в корзине
        price, currency, priceFormatted, baseUnit, tubeTypeIds, tubeLength, weight, volume, colorId, tubeTypeId, ringType) {
        if (name === void 0) { name = ""; }
        if (previewImgSrc === void 0) { previewImgSrc = ""; }
        if (basketImgSrc === void 0) { basketImgSrc = ""; }
        if (length === void 0) { length = 1; }
        if (price === void 0) { price = 0; }
        if (currency === void 0) { currency = "руб."; }
        if (priceFormatted === void 0) { priceFormatted = ""; }
        if (baseUnit === void 0) { baseUnit = ""; }
        if (tubeTypeIds === void 0) { tubeTypeIds = []; }
        if (tubeLength === void 0) { tubeLength = 0; }
        if (weight === void 0) { weight = ""; }
        if (volume === void 0) { volume = ""; }
        if (colorId === void 0) { colorId = ""; }
        if (tubeTypeId === void 0) { tubeTypeId = ""; }
        if (ringType === void 0) { ringType = ""; }
        this.id = id;
        this.productId = productId;
        this.name = name;
        this.previewImgSrc = previewImgSrc;
        this.basketImgSrc = basketImgSrc;
        this.length = length;
        this.price = price;
        this.currency = currency;
        this.priceFormatted = priceFormatted;
        this.baseUnit = baseUnit;
        this.tubeTypeIds = tubeTypeIds;
        this.tubeLength = tubeLength;
        this.weight = weight;
        this.volume = volume;
        this.colorId = colorId;
        this.tubeTypeId = tubeTypeId;
        this.ringType = ringType;
    }
    return CorniceItem;
}());
exports.CorniceItem = CorniceItem;
//# sourceMappingURL=cornice-item.js.map