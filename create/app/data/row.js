"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Row = (function () {
    function Row(id, name) {
        if (name === void 0) { name = ""; }
        this.id = id;
        this.name = name;
    }
    return Row;
}());
exports.Row = Row;
//# sourceMappingURL=row.js.map