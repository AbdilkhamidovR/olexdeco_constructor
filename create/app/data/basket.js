"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Basket = (function () {
    function Basket(id, sum, currencyFormat, sumFormatted, items, itemKeys, itemLengths) {
        if (sum === void 0) { sum = 0; }
        if (currencyFormat === void 0) { currencyFormat = ""; }
        if (sumFormatted === void 0) { sumFormatted = ""; }
        if (items === void 0) { items = {}; }
        if (itemKeys === void 0) { itemKeys = []; }
        if (itemLengths === void 0) { itemLengths = { mounts: 0, rings: 0 }; }
        this.id = id;
        this.sum = sum;
        this.currencyFormat = currencyFormat;
        this.sumFormatted = sumFormatted;
        this.items = items;
        this.itemKeys = itemKeys;
        this.itemLengths = itemLengths;
    }
    return Basket;
}());
exports.Basket = Basket;
//# sourceMappingURL=basket.js.map