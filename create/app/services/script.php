<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$arErrors = array();
$arMess = array();

$arMess["REQUEST"] = $_REQUEST;

$arResponse = array(
	"error" => $arErrors,
	"msg" =>  $arMess
);
// fppr($arResponse, __FILE__.' arResponse');
echo json_encode($arResponse);
?>