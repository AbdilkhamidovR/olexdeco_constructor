"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var basket_1 = require("../data/basket");
var global_1 = require("../global");
var CorniceService = (function () {
    function CorniceService(http) {
        this.http = http;
        this.scriptPath = "/constructor/create/api/script.php";
    }
    /**
     * Получение св-в пользователя
     */
    CorniceService.prototype.getUser = function () {
        return this.http
            .post(this.scriptPath, { 'act': 'getUser' }, {});
    };
    /**
     * Получение кол-ва рядов
     */
    CorniceService.prototype.getRowQuantities = function () {
        return this.http
            .post(this.scriptPath, { 'act': 'getRowQuantities' }, {});
    };
    /**
     * Цвета
     */
    CorniceService.prototype.getColors = function (filter, act) {
        console.log("getColors act: %s filter", act, filter);
        return this.http
            .post(this.scriptPath, { 'filter': filter, 'act': act }, {});
    };
    /**
     * Форма трубы
     */
    CorniceService.prototype.getTubeForms = function (filter) {
        console.log("getTubeForms filter", filter);
        return this.http
            .post(this.scriptPath, { 'filter': filter, 'act': 'getTubeForms' }, {});
    };
    /**
     * Длины трубы
     */
    CorniceService.prototype.getTubeLength = function (filter) {
        console.log("getTubeLength filter", filter);
        return this.http
            .post(this.scriptPath, { 'filter': filter, 'act': 'getTubeLengthsByTube' }, {});
    };
    /**
     * Тип трубы (диаметр и т.д.)
     */
    CorniceService.prototype.getTubeTypes = function (filter, params) {
        if (params === void 0) { params = null; }
        console.log("getTubeTypes filter", filter);
        console.log("getTubeTypes params", params);
        return this.http
            .post(this.scriptPath, { 'filter': filter, 'act': 'getTubeTypes', 'params': params }, {});
    };
    /**
     * Варианты длины труб по id трубы
     */
    CorniceService.prototype.getTubeLengthsByTube = function (id) {
        var filter = { id: id };
        console.log("getTubeLengthsByTube filter", filter);
        return this.http
            .post(this.scriptPath, { 'filter': filter, 'act': 'getTubeLengthsByTube' }, {});
    };
    /**
     * Варианты длины труб по трубам
     */
    CorniceService.prototype.getTubeLengthsByTubes = function (tubes) {
        var filter = { tubes: tubes };
        console.log("getTubeLengthsByTube filter", filter);
        return this.http
            .post(this.scriptPath, { 'filter': filter, 'act': 'getTubeLengthsByTubes' }, {});
    };
    /**
     * Получение элемента карниза
     */
    CorniceService.prototype.getCorniceItem = function (funcName, filter, params) {
        if (params === void 0) { params = null; }
        console.log("getCorniceItem (%s) filter: ", funcName, filter);
        console.log("getCorniceItem (%s) params: ", funcName, params);
        return this.http
            .post(this.scriptPath, { 'act': funcName, 'filter': filter, 'params': params }, {});
    };
    /**
     * Получение элементов карниза
     */
    CorniceService.prototype.getCorniceItems = function (funcName, filter, params) {
        if (params === void 0) { params = null; }
        console.log("getCorniceItems (%s) filter: ", funcName, filter);
        console.log("getCorniceItem (%s) params: ", funcName, params);
        return this.http
            .post(this.scriptPath, { 'filter': filter, 'act': funcName, 'params': params }, {});
    };
    /**
     * Генерация картинки карниза
     */
    CorniceService.prototype.getCorniceImg = function (cornice) {
        var args = {
            "cornice": cornice
        };
        console.log("getCorniceImg args", args);
        return this.http
            .post(this.scriptPath, { 'filter': args, 'act': 'getCorniceImg' }, {});
    };
    /**
     * Получение соединителей
     */
    CorniceService.prototype.getConnectors = function (tubes) {
        var args = { tubes: tubes };
        console.log("getConnectors args", args);
        return this.http
            .post(this.scriptPath, { 'filter': args, 'act': 'getConnectors' }, {});
    };
    /**
     * Подсчет кол-ва кронштейнов
     */
    CorniceService.prototype.getMountLength = function (tubeLength) {
        // console.log("getMountLength tubeLength: ", tubeLength);
        var length = 0;
        if (tubeLength > 0) {
            length = Math.ceil(tubeLength / global_1.Global.MOUNT_MAX_DISTANCE) + 1;
        }
        return length;
    };
    /**
     * Подсчет кол-ва кронштейнов
     */
    CorniceService.prototype.getRingsInRowLength = function (tubeLength, ringsType) {
        if (ringsType === void 0) { ringsType = "RING"; }
        var packQuantity;
        switch (ringsType) {
            case "RING":
                packQuantity = global_1.Global.RINGS_IN_PACK;
                break;
            case "RUNNER":
                packQuantity = global_1.Global.RUNNERS_IN_PACK;
                break;
        }
        // alert(packQuantity);
        var length = tubeLength * 0.001 * global_1.Global.RINGS_BY_METER / packQuantity;
        return length;
    };
    /**
     * формирование корзины
     */
    CorniceService.prototype.makeBasket = function (cornice) {
        // console.log("cornice.service getBasket cornice", cornice);
        var basket = new basket_1.Basket(cornice.id);
        // трубы центра
        this.addBasketItems(basket, cornice.tubes, "tubes", "t_");
        // трубы левой стороны
        this.addBasketItems(basket, cornice.sides.left.tubes, "tubes", "t_");
        // трубы правой стороны
        this.addBasketItems(basket, cornice.sides.right.tubes, "tubes", "t_");
        // кронштейн
        var itemName = "";
        if (cornice.mounts[0]) {
            itemName = "m_" + cornice.mounts[0].id;
            basket.items[itemName] = this.copyObj(cornice.mounts[0]);
            basket.items[itemName].length = this.getBasketMountLength(cornice);
        }
        basket.itemLengths["mounts"] = basket.items[itemName].length;
        // наконечники
        this.addBasketItems(basket, cornice.caps, "caps", "c_");
        // кольца
        this.addBasketItems(basket, cornice.rings, "rings", "r_");
        // округление кол-в для получения кол-ва упаковок бегунков и колец
        basket.itemLengths["rings"] = 0;
        for (var _i = 0, _a = basket.itemKeys; _i < _a.length; _i++) {
            var key = _a[_i];
            var item = basket.items[key];
            if (item) {
                if (item["ringType"] == "RUNNER" || item["ringType"] == "RING") {
                    item.length = Math.ceil(item.length);
                    basket.itemLengths["rings"] += item.length;
                }
            }
        }
        // basket.itemLengths["rings"] = Math.ceil(basket.itemLengths["rings"]);
        // соединители левой стороны
        this.addBasketItems(basket, cornice.sides.left.connectors, "connectors", "j_");
        // соединители правой стороны
        this.addBasketItems(basket, cornice.sides.right.connectors, "connectors", "j_");
        //
        basket.itemKeys = Object.keys(basket.items);
        this.calculateBasket(basket);
        console.log("cornice.service makeBasket basket", basket);
        return basket;
    };
    /**
     * добавление элементов карнизв в корзину
     */
    CorniceService.prototype.addBasketItems = function (basket, arItems, type, prefix) {
        if (!basket.itemLengths[type]) {
            basket.itemLengths[type] = 0;
        }
        if (type == "tubes" && !basket.itemLengths["tubes_mm"]) {
            basket.itemLengths["tubes_mm"] = 0;
        }
        var itemName = "";
        for (var _i = 0, arItems_1 = arItems; _i < arItems_1.length; _i++) {
            var item = arItems_1[_i];
            if (item) {
                itemName = prefix + item.id;
                if (this.contains(basket.itemKeys, itemName)) {
                    basket.items[itemName].length += item.length;
                }
                else {
                    basket.items[itemName] = this.copyObj(item);
                }
                basket.itemLengths[type] = basket.items[itemName].length;
                if (type == "tubes") {
                    basket.itemLengths["tubes_mm"] += basket.items[itemName].tubeLength;
                }
                basket.itemKeys = Object.keys(basket.items);
            }
        }
    };
    /**
     * расчет кол-ва кронштейнов в корзине
     */
    CorniceService.prototype.getBasketMountLength = function (cornice) {
        var n = this.getMountLength(cornice.tubes[0].tubeLength) + this.getMountLength(cornice.sides.left.tubeLength) + this.getMountLength(cornice.sides.right.tubeLength);
        if (cornice.sides.left.type == 'direct') {
            n--;
        }
        if (cornice.sides.right.type == 'direct') {
            n--;
        }
        return n;
    };
    /**
     * расчет кол-ва колец или бегунков в корзине
     */
    CorniceService.prototype.getBasketRingsInRowLength = function (cornice) {
        var n = this.getRingsInRowLength(cornice.tubes[0].tubeLength, cornice.rings[0].ringType) + this.getRingsInRowLength(cornice.sides.left.tubeLength, cornice.rings[0].ringType) + this.getRingsInRowLength(cornice.sides.right.tubeLength, cornice.rings[0].ringType);
        return n;
    };
    /**
     * расчет суммы корзины
     */
    CorniceService.prototype.calculateBasket = function (basket) {
        basket.sum = 0;
        for (var _i = 0, _a = basket.itemKeys; _i < _a.length; _i++) {
            var key = _a[_i];
            basket.sum += basket.items[key].price * basket.items[key].length;
        }
        basket.currencyFormat = basket.items[basket.itemKeys[0]].currencyFormat;
        basket.sumFormatted = basket.currencyFormat.replace("#", basket.sum);
    };
    /**
     * удаление элемента корзины
     */
    CorniceService.prototype.deleteBasketItem = function (basket, itemName) {
        delete basket.items[itemName];
        this.calculateBasket(basket);
        return basket;
    };
    /**
     * Добавление товаров в корзину битрикса
     */
    CorniceService.prototype.addToBxBasket = function (basket) {
        var args = { basket: basket };
        console.log("service addToBxBasket args", args);
        return this.http
            .post(this.scriptPath, { 'filter': args, 'act': 'addToBxBasket' }, {});
    };
    /**
     * Печать картинки карниза
     */
    CorniceService.prototype.printImg = function (imgSrc) {
        var args = { imgSrc: imgSrc };
        var new_window = window.open(imgSrc, 'olexdeco_constructor.jpg', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=no, resizable=no, width=1280, height=800');
        this.http.get(imgSrc)
            .subscribe(function (response) {
            new_window.print();
        });
    };
    /**
     * Удаление картинки карниза из кэша, для обновления
     */
    CorniceService.prototype.updateImg = function (imgSrc) {
        var args = { imgSrc: imgSrc };
        console.log("updateImg args", args);
        return this.http
            .post(this.scriptPath, { 'filter': args, 'act': 'updateImg' }, {});
    };
    /*== Вспомогательные функции ==*/
    /**
     * копирование простых объектов и массивов объектов
     */
    CorniceService.prototype.copyObj = function (object) {
        return JSON.parse(JSON.stringify(object));
    };
    /**
     * проверяет наличие элемента в одномерном массиве
     * true - есть
     * false - нет
     */
    CorniceService.prototype.contains = function (arr, elem) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === elem) {
                return true;
            }
        }
        return false;
    };
    /**
     * проверяет точное равенство двух массивов
     */
    CorniceService.prototype.isArraysEquals = function (arr1, arr2) {
        if (JSON.stringify(arr1) === JSON.stringify(arr2)) {
            return true;
        }
        else {
            return false;
        }
    };
    return CorniceService;
}());
CorniceService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], CorniceService);
exports.CorniceService = CorniceService;
//# sourceMappingURL=cornice.service.js.map