import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
//my
import { Filter }	from '../data/filter';
import { CorniceItem }	from '../data/cornice-item';
import { Cornice }	from '../data/cornice';
import { Basket }	from '../data/basket';
import { Global } from '../global'; 

@Injectable()
export class CorniceService {

	private scriptPath: string = "/constructor/create/api/script.php";


	constructor(
		private http: Http,
	) {}


	/**
	 * Получение св-в пользователя
	 */
	getUser(){
		return this.http
			.post(this.scriptPath, {'act': 'getUser' }, {});
	}

	/**
	 * Получение кол-ва рядов
	 */
	getRowQuantities(){
		return this.http
			.post(this.scriptPath, {'act': 'getRowQuantities' }, {})
	}

	/**
	 * Цвета
	 */
	getColors(filter: Filter, act: string){
		console.log("getColors act: %s filter", act, filter);
		return this.http
			.post(this.scriptPath, {'filter': filter, 'act': act }, {});
	}

	/**
	 * Форма трубы
	 */
	getTubeForms(filter: Filter){
		console.log("getTubeForms filter", filter);
		return this.http
			.post(this.scriptPath, {'filter': filter, 'act': 'getTubeForms' }, {});
	}

	/**
	 * Длины трубы
	 */
	getTubeLength(filter: Filter){
		console.log("getTubeLength filter", filter);
		return this.http
			.post(this.scriptPath, {'filter': filter, 'act': 'getTubeLengthsByTube' }, {});
	}


	/**
	 * Тип трубы (диаметр и т.д.)
	 */
	getTubeTypes(filter: Filter, params: object = null){
		console.log("getTubeTypes filter", filter);
		console.log("getTubeTypes params", params);
		return this.http
			.post(this.scriptPath, {'filter': filter, 'act': 'getTubeTypes', 'params': params }, {});
	}

	/**
	 * Варианты длины труб по id трубы
	 */
	getTubeLengthsByTube(id:number){
		let filter = {id: id};
		console.log("getTubeLengthsByTube filter", filter);
		return this.http
			.post(this.scriptPath, {'filter': filter, 'act': 'getTubeLengthsByTube' }, {});
	}

	/**
	 * Варианты длины труб по трубам
	 */
	getTubeLengthsByTubes(tubes: CorniceItem[]){
		let filter = {tubes: tubes};
		console.log("getTubeLengthsByTube filter", filter);
		return this.http
			.post(this.scriptPath, {'filter': filter, 'act': 'getTubeLengthsByTubes' }, {});
	}


	/**
	 * Получение элемента карниза
	 */
	getCorniceItem(funcName: string, filter: Filter, params: object = null){
		console.log("getCorniceItem (%s) filter: ", funcName, filter);
		console.log("getCorniceItem (%s) params: ", funcName, params);
		return this.http
			.post(this.scriptPath, {'act': funcName, 'filter': filter, 'params': params}, {})
	}


	/**
	 * Получение элементов карниза
	 */
	getCorniceItems(funcName: string, filter: Filter, params: object = null){
		console.log("getCorniceItems (%s) filter: ", funcName, filter);
		console.log("getCorniceItem (%s) params: ", funcName, params);
		return this.http
			.post(this.scriptPath, {'filter': filter, 'act': funcName, 'params': params}, {})
	}


	/**
	 * Генерация картинки карниза
	 */
	getCorniceImg(cornice: Cornice){
		let args = {
			"cornice": cornice
		}
		console.log("getCorniceImg args", args);
		return this.http
			.post(this.scriptPath, {'filter': args, 'act': 'getCorniceImg' }, {})

	}


	/**
	 * Получение соединителей
	 */
	getConnectors(tubes: CorniceItem[]){
		let args = {tubes: tubes};
		console.log("getConnectors args", args);
		return this.http
			.post(this.scriptPath, {'filter': args, 'act': 'getConnectors' }, {})

	}

	/**
	 * Подсчет кол-ва кронштейнов
	 */
	getMountLength(tubeLength: number){
		// console.log("getMountLength tubeLength: ", tubeLength);
		let length: number = 0;
		if(tubeLength > 0){
			length = Math.ceil(tubeLength/Global.MOUNT_MAX_DISTANCE) + 1;
		}
		return length;
	}

	/**
	 * Подсчет кол-ва кронштейнов
	 */
	getRingsInRowLength(tubeLength: number, ringsType: string = "RING"){
		let packQuantity;
		switch (ringsType) {
			case "RING":
				packQuantity =  Global.RINGS_IN_PACK;
				break;
			
			case "RUNNER":
				packQuantity =  Global.RUNNERS_IN_PACK;
				break;
		}
		// alert(packQuantity);
		let length = tubeLength*0.001*Global.RINGS_BY_METER/packQuantity;
		return length;
	}


	/**
	 * формирование корзины
	 */
	makeBasket(cornice: Cornice){
		// console.log("cornice.service getBasket cornice", cornice);
		let basket:Basket = new Basket(cornice.id);

		// трубы центра
		this.addBasketItems(basket, cornice.tubes, "tubes", "t_");

		// трубы левой стороны
		this.addBasketItems(basket, cornice.sides.left.tubes, "tubes", "t_");

		// трубы правой стороны
		this.addBasketItems(basket, cornice.sides.right.tubes, "tubes", "t_");

		// кронштейн
		let itemName = "";
		if(cornice.mounts[0]){
			itemName = "m_"+cornice.mounts[0].id;
			basket.items[itemName] = this.copyObj(cornice.mounts[0]);
			basket.items[itemName].length = this.getBasketMountLength(cornice);
		}
		basket.itemLengths["mounts"] = basket.items[itemName].length;

		// наконечники
		this.addBasketItems(basket, cornice.caps, "caps", "c_");

		// кольца
		this.addBasketItems(basket, cornice.rings, "rings", "r_");

		// округление кол-в для получения кол-ва упаковок бегунков и колец
		basket.itemLengths["rings"] = 0;
		for (let key of basket.itemKeys) {
			let item = basket.items[key];
			if(item){
				if(item["ringType"] == "RUNNER" || item["ringType"] == "RING"){
					item.length = Math.ceil(item.length);
					basket.itemLengths["rings"] += item.length;
				}
			}
		}
		// basket.itemLengths["rings"] = Math.ceil(basket.itemLengths["rings"]);

		// соединители левой стороны
		this.addBasketItems(basket, cornice.sides.left.connectors, "connectors", "j_");

		// соединители правой стороны
		this.addBasketItems(basket, cornice.sides.right.connectors, "connectors", "j_");

		//
		basket.itemKeys = Object.keys(basket.items);
		this.calculateBasket(basket);
		console.log("cornice.service makeBasket basket", basket);

		return basket;
	}

	/**
	 * добавление элементов карнизв в корзину
	 */
	addBasketItems(basket:Basket, arItems: CorniceItem[], type:string, prefix:string){
		if(!basket.itemLengths[type]){
			basket.itemLengths[type] = 0;
		}
		if(type == "tubes" && !basket.itemLengths["tubes_mm"]){
			basket.itemLengths["tubes_mm"] = 0;
		}

		let itemName = "";

		for (let item of arItems) {
			if(item){
				itemName = prefix + item.id;
				if(this.contains(basket.itemKeys, itemName)){
					basket.items[itemName].length += item.length;
				}else{
					basket.items[itemName] = this.copyObj(item);
				}
				basket.itemLengths[type] = basket.items[itemName].length;
				if(type == "tubes"){
					basket.itemLengths["tubes_mm"] += basket.items[itemName].tubeLength;
				}
				basket.itemKeys = Object.keys(basket.items);
			}
		}
	}

	/**
	 * расчет кол-ва кронштейнов в корзине
	 */
	getBasketMountLength(cornice: Cornice){
		let n:number = this.getMountLength(cornice.tubes[0].tubeLength) + this.getMountLength(cornice.sides.left.tubeLength) + this.getMountLength(cornice.sides.right.tubeLength);
		if(cornice.sides.left.type == 'direct'){
			n--;
		}
		if(cornice.sides.right.type == 'direct'){
			n--;
		}
		return n;
	}

	/**
	 * расчет кол-ва колец или бегунков в корзине
	 */
	getBasketRingsInRowLength(cornice: Cornice){
		let n:number = this.getRingsInRowLength(cornice.tubes[0].tubeLength, cornice.rings[0].ringType) + this.getRingsInRowLength(cornice.sides.left.tubeLength, cornice.rings[0].ringType) + this.getRingsInRowLength(cornice.sides.right.tubeLength, cornice.rings[0].ringType);
		return n;
	}

	/**
	 * расчет суммы корзины
	 */
	calculateBasket(basket:Basket){
		basket.sum = 0;
		for (let key of basket.itemKeys) {
			basket.sum += basket.items[key].price * basket.items[key].length;
		}
		basket.currencyFormat = basket.items[basket.itemKeys[0]].currencyFormat;
		basket.sumFormatted = basket.currencyFormat.replace("#", basket.sum);
	}

	/**
	 * удаление элемента корзины
	 */
	deleteBasketItem(basket:Basket, itemName:string){
		delete basket.items[itemName];
		this.calculateBasket(basket);
		return basket;
	}

	/**
	 * Добавление товаров в корзину битрикса
	 */
	addToBxBasket(basket:Basket){
		let args = {basket: basket};
		console.log("service addToBxBasket args", args);
		return this.http
			.post(this.scriptPath, {'filter': args, 'act': 'addToBxBasket' }, {})
	}

	/**
	 * Печать картинки карниза
	 */
	printImg(imgSrc:string){
		let args = {imgSrc: imgSrc};
		let new_window = window.open(imgSrc, 'olexdeco_constructor.jpg', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=no, resizable=no, width=1280, height=800');
		this.http.get(imgSrc)
			.subscribe(
				(response:Response) => {
					new_window.print();
				}
			);
	}

	/**
	 * Удаление картинки карниза из кэша, для обновления
	 */
	updateImg(imgSrc:string){
		let args = {imgSrc: imgSrc};
		console.log("updateImg args", args);
		return this.http
			.post(this.scriptPath, {'filter': args, 'act': 'updateImg' }, {})
	}



	/*== Вспомогательные функции ==*/

	/**
	 * копирование простых объектов и массивов объектов
	 */
	copyObj(object: object){
		return JSON.parse(JSON.stringify(object));
	}

	/**
	 * проверяет наличие элемента в одномерном массиве
	 * true - есть
	 * false - нет
	 */
	contains(arr:string[], elem:string) {
		for (var i = 0; i < arr.length; i++) {
			if (arr[i] === elem) {
				return true;
			}
		}
		return false;
	}

	/**
	 * проверяет точное равенство двух массивов
	 */
	isArraysEquals(arr1:string[], arr2:string[]) {
		if(JSON.stringify(arr1) === JSON.stringify(arr2)){
			return true;
		}else{
			return false;
		}
	}
}
