import { Component, HostListener }	from '@angular/core';
import { trigger, state, animate, transition, style} from '@angular/core';
import { Http, Response } from '@angular/http';
//my
import { Global } from './global'; // мои глобальные константы
import { Step }	from './data/step';
import { Cornice }	from './data/cornice';
import { Filter }	from './data/filter';
import { CorniceService }  from './services/cornice.service';




@Component({
	selector: 'cornice-app',
	animations: [
			trigger('visibilityTrigger', [
				state('true' , style({ opacity: 1, transform: 'scale(1.0)' })),
				state('false', style({ opacity: 0, transform: 'scale(0.0)' })),
				transition('1 => 0', animate('200ms ease-out')),
				transition('0 => 1', animate('200ms cubic-bezier(0.3, 0.3, .8, 1.5)'))
			]),
			trigger('mobileSlideTrigger', [
				state('true' , style({ right: '0' })),
				state('false', style({ right: '-100%' })),
				transition('1 => 0', animate('200ms ease-in-out')),
				transition('0 => 1', animate('200ms ease-in-out'))
			]),
		],
	templateUrl: './templates/home.html' + ((Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
	moduleId: module.id
})
export class AppComponent {

	private steps: any[];
	private cornice: Cornice;
	private filter: Filter;
	private activeStep: number = 0;
	private isLoaded = false;
	private isVisible = false;
	private user: object;
	private isMobileMenuActive: boolean = false;
	private isMobileBasketActive: boolean = false;
	private innerWidth: number;

	//
	constructor(
		private corniceService: CorniceService,
		) {}

	init(){
		this.steps = [ 
			new Step(0,'Выбор ряда', true, false), 
			new Step(1,'Выбор трубы (профиля)', false, true, {swiperCaption:"Выберите форму трубы"}), 
			new Step(2,'Выбор кронштейна', false, true, {swiperCaption:"Выберите кронштейн"}), 
			new Step(3,'Выбор наконечника', false, true, {swiperCaption:"Выберите наконечник"}), 
			new Step(4,'Выбор колец и крючков', false, true, {swiperCaption:"Выберите кольца"}), 
			new Step(5,'Выбор характеристик карниза', false, true), 
			new Step(6,'Оформление заказа', false, true), 
		];

		let date = new Date();
		this.cornice = new Cornice(date.getTime(), 0);

		this.filter = new Filter();
		this.isLoaded = true;
		this.isVisible = true;
	}


	ngOnInit(): void {
		this.innerWidth = window.innerWidth;
		this.getUser();
	}

	/* Get window width */
	@HostListener('window:resize', ['$event'])
	onResize(event:any) {
	 	this.innerWidth = window.innerWidth;
	}


	/* Конец загрузки */
	ngAfterViewInit() {
	}

	getUser(){
		this.corniceService.getUser()
			.subscribe(
				(response:Response) => {
					if(response.ok && response.json().arItems){
						this.user = response.json().arItems;
					}else{
						this.user = undefined;
					}
					this.init();
				}
			);
	}

	/* Ckick on gamburger */
	mobNavlinkClicked(){
		this.isMobileMenuActive = !this.isMobileMenuActive;
	}

	/* Ckick on basket icon */
	mobBasketlinkClicked(){
		this.isMobileBasketActive = !this.isMobileBasketActive;
	}

	/* Навигация */
	gotoStep(i:number){
		for (let step of this.steps) {
			if(step.id == i){
				step.setActive();
			}else{
				step.setDisactive();
			}
		}
		this.cornice.activeStep = i;
		this.isMobileMenuActive = false;
	}

	/* Обработчик события изменения текущего шага в дочерних компонентах */
	onStepChanged(step_id: number){
		this.gotoStep(step_id);
	}

	onCorniceChanged(_cornice:Cornice){
		this.cornice = _cornice;
	}

}
