"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
//my
var global_1 = require("./global"); // мои глобальные константы
var step_1 = require("./data/step");
var cornice_1 = require("./data/cornice");
var filter_1 = require("./data/filter");
var cornice_service_1 = require("./services/cornice.service");
var AppComponent = (function () {
    //
    function AppComponent(corniceService) {
        this.corniceService = corniceService;
        this.activeStep = 0;
        this.isLoaded = false;
        this.isVisible = false;
        this.isMobileMenuActive = false;
        this.isMobileBasketActive = false;
    }
    AppComponent.prototype.init = function () {
        this.steps = [
            new step_1.Step(0, 'Выбор ряда', true, false),
            new step_1.Step(1, 'Выбор трубы (профиля)', false, true, { swiperCaption: "Выберите форму трубы" }),
            new step_1.Step(2, 'Выбор кронштейна', false, true, { swiperCaption: "Выберите кронштейн" }),
            new step_1.Step(3, 'Выбор наконечника', false, true, { swiperCaption: "Выберите наконечник" }),
            new step_1.Step(4, 'Выбор колец и крючков', false, true, { swiperCaption: "Выберите кольца" }),
            new step_1.Step(5, 'Выбор характеристик карниза', false, true),
            new step_1.Step(6, 'Оформление заказа', false, true),
        ];
        var date = new Date();
        this.cornice = new cornice_1.Cornice(date.getTime(), 0);
        this.filter = new filter_1.Filter();
        this.isLoaded = true;
        this.isVisible = true;
    };
    AppComponent.prototype.ngOnInit = function () {
        this.innerWidth = window.innerWidth;
        this.getUser();
    };
    /* Get window width */
    AppComponent.prototype.onResize = function (event) {
        this.innerWidth = window.innerWidth;
    };
    /* Конец загрузки */
    AppComponent.prototype.ngAfterViewInit = function () {
    };
    AppComponent.prototype.getUser = function () {
        var _this = this;
        this.corniceService.getUser()
            .subscribe(function (response) {
            if (response.ok && response.json().arItems) {
                _this.user = response.json().arItems;
            }
            else {
                _this.user = undefined;
            }
            _this.init();
        });
    };
    /* Ckick on gamburger */
    AppComponent.prototype.mobNavlinkClicked = function () {
        this.isMobileMenuActive = !this.isMobileMenuActive;
    };
    /* Ckick on basket icon */
    AppComponent.prototype.mobBasketlinkClicked = function () {
        this.isMobileBasketActive = !this.isMobileBasketActive;
    };
    /* Навигация */
    AppComponent.prototype.gotoStep = function (i) {
        for (var _i = 0, _a = this.steps; _i < _a.length; _i++) {
            var step = _a[_i];
            if (step.id == i) {
                step.setActive();
            }
            else {
                step.setDisactive();
            }
        }
        this.cornice.activeStep = i;
        this.isMobileMenuActive = false;
    };
    /* Обработчик события изменения текущего шага в дочерних компонентах */
    AppComponent.prototype.onStepChanged = function (step_id) {
        this.gotoStep(step_id);
    };
    AppComponent.prototype.onCorniceChanged = function (_cornice) {
        this.cornice = _cornice;
    };
    return AppComponent;
}());
__decorate([
    core_1.HostListener('window:resize', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AppComponent.prototype, "onResize", null);
AppComponent = __decorate([
    core_1.Component({
        selector: 'cornice-app',
        animations: [
            core_2.trigger('visibilityTrigger', [
                core_2.state('true', core_2.style({ opacity: 1, transform: 'scale(1.0)' })),
                core_2.state('false', core_2.style({ opacity: 0, transform: 'scale(0.0)' })),
                core_2.transition('1 => 0', core_2.animate('200ms ease-out')),
                core_2.transition('0 => 1', core_2.animate('200ms cubic-bezier(0.3, 0.3, .8, 1.5)'))
            ]),
            core_2.trigger('mobileSlideTrigger', [
                core_2.state('true', core_2.style({ right: '0' })),
                core_2.state('false', core_2.style({ right: '-100%' })),
                core_2.transition('1 => 0', core_2.animate('200ms ease-in-out')),
                core_2.transition('0 => 1', core_2.animate('200ms ease-in-out'))
            ]),
        ],
        templateUrl: './templates/home.html' + ((global_1.Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
        moduleId: module.id
    }),
    __metadata("design:paramtypes", [cornice_service_1.CorniceService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map