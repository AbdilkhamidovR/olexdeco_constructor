import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import {KSSwiperModule} from 'angular2-swiper';
import { enableProdMode } from '@angular/core';

import { AppComponent }         from './app.component';

//my
import { PriceformatPipe} from './pipes/priceformat.pipe';
import { RowsComponent }  from './components/rows.component';
import { PartsTubesComponent }  from './components/parts-tubes.component';
import { PartsMountComponent }  from './components/parts-mount.component';
import { PartsCapComponent }  from './components/parts-cap.component';
import { PartsRingComponent }  from './components/parts-ring.component';
import { PropertiesComponent }  from './components/properties.component';
import { CheckoutComponent }  from './components/checkout.component';
import { FinalpopupComponent }  from './components/finalpopup.component';
import { HelperService }  from './services/helper.service';
import { CorniceService }  from './services/cornice.service';
import { ItemsSliderComponent }  from './components/items-slider.component';
import { BasketSliderComponent }  from './components/basket-slider.component';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    KSSwiperModule
  ],
  declarations: [
    AppComponent,
    PriceformatPipe,
    RowsComponent,
    PartsTubesComponent,
    PartsMountComponent,
    PartsCapComponent,
    PartsRingComponent,
    PropertiesComponent,
    CheckoutComponent,
    FinalpopupComponent,
    ItemsSliderComponent,
    BasketSliderComponent
  ],
  providers: [ HelperService, CorniceService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { 
  constructor() {
  }
}
