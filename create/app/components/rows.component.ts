import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, animate, transition, style} from '@angular/core';

import { Http, Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';

import { Global } from '../global'; // мои глобальные константы
import { Step }	from '../data/step';
import { Row }	from '../data/row';
import { Cornice }	from '../data/cornice';
import { CorniceService }  from '../services/cornice.service';
import { Filter }	from '../data/filter';

@Component({
	selector: 'cornice-rows',
	animations: [
			trigger('visibilityTrigger', [
				state('true' , style({ opacity: 1, transform: 'scale(1.0)' })),
				state('false', style({ opacity: 0, transform: 'scale(0.0)' })),
				transition('1 => 0', animate('200ms ease-out')),
				transition('0 => 1', animate('200ms cubic-bezier(0.3, 0.3, .8, 1.5)'))
			])
		],
	templateUrl: 'app/templates/rows.template.html' + ((Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
})
export class RowsComponent  implements OnInit{

	private isVisible : boolean = false;

	// данные из app.component
	@Input() cornice: Cornice;
	@Input() filter: Filter;
	@Input() steps: any;

	private row: Row;
	private rows: Row[];
	private preloader: boolean = false;
	private phpTimes: object[] = [];
	private phpAllTime: number = 0;

	// объявление события изменения фильтра выборки элементов карниза
	@Output() onFilterChanged = new EventEmitter<Filter>();
	@Output() onCorniceChanged = new EventEmitter<Cornice>();

	private rowQuantitiesSubject: Subject<boolean> = new Subject();
	private rowQuantitiesSubscription: Subscription;



	constructor(
		private corniceService: CorniceService
	) { }

	ngOnInit(): void {
		this.startPreloading();
		this.getRowQuantities();
	}

	startPreloading(){
		this.preloader = true;
		this.isVisible = false;
	}

	stopPreloading(){
		this.preloader = false;
		this.isVisible = true;
		console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
		this.phpTimes = [];
		this.phpAllTime = 0;
	}

	/* Навигация: выбор кол-ва рядов */
	rowsSelected(nRowsId:number = 0){
		this.steps[0].setDisactive();
		this.steps[1].setActive();
		this.steps[2].setEnable();
		this.steps[3].setEnable();
		this.steps[4].setEnable();
		this.steps[5].setEnable();
		this.steps[6].setEnable();

		this.cornice.activeStep = 1;

		this.filter.nRowsId = nRowsId;
		this.cornice.nRows = Global.NROWS[this.filter.nRowsId];
		this.cornice.caps.length = Global.NROWS[nRowsId];
		this.cornice.tubes.length = Global.NROWS[nRowsId];
		this.cornice.rings.length = Global.NROWS[nRowsId];
		this.filter.tubeFormIds.length = Global.NROWS[nRowsId];
		this.filter.tubeTypeIds.length = Global.NROWS[nRowsId];

		console.log("%c rowsSelected: this.filter", "color:red", this.filter);

		this.onFilterChanged.emit(this.filter);
		this.onCorniceChanged.emit(this.cornice);
	}


	getRowQuantities(){
		this.corniceService.getRowQuantities()
			.subscribe(
				(response:Response) => {
					if(response.ok){
						// console.log("getRowQuantities: this.response", response);
						if(response.json().arItems){
							this.rows = response.json().arItems;
							this.timing('getRowQuantities', response.json().times);
							this.stopPreloading();
						}
					}
				}
			);
	}

	timing(title:string, times:number){
		if(Global.IS_DEBUG){
			this.phpTimes.push({[title]: times});
			this.phpAllTime += times;
		}
	}

	ngOnDestroy() {
	}


}
