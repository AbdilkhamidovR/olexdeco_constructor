"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var cornice_1 = require("../data/cornice");
var FinalpopupComponent = (function () {
    function FinalpopupComponent(elementRef) {
        this.elementRef = elementRef;
    }
    ;
    FinalpopupComponent.prototype.ngOnInit = function () {
        // console.log(this.steps);
        console.log(this.cornice);
    };
    return FinalpopupComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", cornice_1.Cornice)
], FinalpopupComponent.prototype, "cornice", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], FinalpopupComponent.prototype, "steps", void 0);
FinalpopupComponent = __decorate([
    core_1.Component({
        selector: 'cornice-finalpopup',
        templateUrl: 'app/templates/finalpopup.template.html',
    }),
    __metadata("design:paramtypes", [core_2.ElementRef])
], FinalpopupComponent);
exports.FinalpopupComponent = FinalpopupComponent;
//# sourceMappingURL=finalpopup.component.js.map