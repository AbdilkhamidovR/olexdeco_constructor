"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular2_swiper_1 = require("angular2-swiper");
//my
var global_1 = require("../global");
var basket_1 = require("../data/basket");
var BasketSliderComponent = (function () {
    function BasketSliderComponent() {
        this.basketSwiperOptions = {
            init: true,
            direction: "vertical",
            slidesPerView: 'auto',
            scrollbarHide: true,
            nextButton: ".swiper-button-down",
            prevButton: ".swiper-button-up",
            scrollbar: ".swiper-scrollbar"
        };
    }
    BasketSliderComponent.prototype.ngOnInit = function () {
    };
    BasketSliderComponent.prototype.slidesLoaded = function () {
        // console.log('basket-slider: basket loaded!');
        this.basketSwiperContainer.swiper.slideTo(0);
    };
    // moveNext() {
    // 	this.basketSwiperOptions.swiper.slideNext();
    // }
    // movePrev() {
    // 	this.basketSwiperOptions.swiper.slidePrev();
    // }
    // slideClicked(id: any){
    // 	console.log("items-slider: slideClicked id", id);
    // 	this.swiperItemSelected.emit(id);
    // }
    BasketSliderComponent.prototype.ngAfterViewInit = function () {
        // console.log("basket-slider: basketSwiperContainer", this.basketSwiperContainer);
    };
    BasketSliderComponent.prototype.ngOnDestroy = function () {
        this.basketSwiperContainer.swiper.destroy();
    };
    return BasketSliderComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", basket_1.Basket)
], BasketSliderComponent.prototype, "basket", void 0);
__decorate([
    core_1.ViewChild(angular2_swiper_1.KSSwiperContainer),
    __metadata("design:type", angular2_swiper_1.KSSwiperContainer)
], BasketSliderComponent.prototype, "basketSwiperContainer", void 0);
BasketSliderComponent = __decorate([
    core_1.Component({
        selector: 'basket-slider',
        templateUrl: 'app/templates/basket-slider.template.html' + ((global_1.Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
    }),
    __metadata("design:paramtypes", [])
], BasketSliderComponent);
exports.BasketSliderComponent = BasketSliderComponent;
//# sourceMappingURL=basket-slider.component.js.map