import { Component, EventEmitter, OnInit, Input, Output, ElementRef, ViewChild } from '@angular/core';
import { trigger, state, animate, transition, style} from '@angular/core';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
//my
import { Global } from '../global'; 
import { Step }	from '../data/step';
import { Cornice }	from '../data/cornice';
import { CorniceItem }	from '../data/cornice-item';
import { LoadItem }	from '../data/load-item';
import { CorniceService }  from '../services/cornice.service';
import { Filter }	from '../data/filter';
import { Color }	from '../data/color';
import { Basket }	from '../data/basket';
import { ItemsSliderComponent }  from '../components/items-slider.component';
import { BasketSliderComponent }  from '../components/basket-slider.component';

@Component({
	selector: 'cornice-parts-mount',
	animations: [
			trigger('mobileBasketTrigger', [
				state('true' , style({ right: '0' })),
				state('false', style({ right: '-100%' })),
				transition('1 => 0', animate('200ms ease-in-out')),
				transition('0 => 1', animate('200ms ease-in-out'))
			]),
		],
	templateUrl: 'app/templates/parts.template.html' + ((Global.IS_DEBUG) ? '?v=' + Math.random() : '')
})
export class PartsMountComponent  implements OnInit{

	// данные из app.component
	@Input() private cornice: Cornice;
	@Input() private filter: Filter;
	@Input() private steps: any;
	@Input() private user: object;
	@Input() private isMobileBasketActive: boolean;

	@Output() private onStepChanged = new EventEmitter<number>();
	@Output() private onCorniceChanged = new EventEmitter<Cornice>();

	@ViewChild('itemsSlider')
	private itemsSlider: ItemsSliderComponent;

	@ViewChild('basketSlider')
	private basketSlider: BasketSliderComponent;
	//
	
	private preloader: boolean;
	private isInit: boolean = true;

	private colors: Color[];
	private mountFilter: Filter;
	private swiperItems: CorniceItem[];
	private corniceItemId: number;
	private corniceItemColorId: string;
	private basket: Basket = new Basket(0);
	private basketItemKeys: string[];
	private phpTimes: object[] = [];
	private phpAllTime: number = 0;
	//
	private mountsSubject: Subject<string> = new Subject();
	private mountsSubscription: Subscription;

	private colorsSubject: Subject<string> = new Subject();
	private colorsSubscription: Subscription;

	private corniceSubject: Subject<Cornice> = new Subject();
	private corniceSubscription: Subscription;

	private corniceDrawSubject: Subject<boolean> = new Subject();
	private corniceDrawSubscription: Subscription;
	//

	constructor(
		private elementRef: ElementRef,
		private corniceService: CorniceService,
	) {
	};

		
	/*=========*/
	ngOnInit(): void {
		this.startPreloading();

		// загрузка шага
		this.initStep();
		this.initSubscriptions();
	}

	// инициализация данных шага 1. Выбор трубы (профиля)
	initStep(){
		console.clear();
		
		this.mountFilter = new Filter(this.filter.nRowsId, this.filter.tubeFormIds, this.filter.tubeTypeIds, this.filter.tubeLength, this.filter.colorId, 0);
		this.corniceItemId = this.cornice.mounts[0].id;
		this.corniceItemColorId = this.cornice.mounts[0].colorId;

		this.basket = this.corniceService.makeBasket(this.cornice);

		this.getMounts(this.mountFilter);

		this.getColors(this.mountFilter);
	}

	initSubscriptions(){
		// Отслеживание загрузки цветов
		this.colorsSubscription = this.colorsSubject
			.subscribe( 
				(x) => { 
					console.log('%c Colors loaded! olorsSubject onNext: ' + x, 'color:green'); 
					this.getMounts(this.mountFilter);
				},
				(e) => { console.log('colorsSubject onError: ' + e.message); },
				() => { console.log('%c colorsSubject complete', 'color:green'); }
			);


		// Отслеживание загрузки наконечников
		this.mountsSubscription = this.mountsSubject
			.subscribe( 
				(s) => { 
					console.log('%c mountsSubject onNext: ', 'color:green',  s); 
					this.corniceSubject.next(this.cornice);
				},
				(e) => { console.log('mountsSubject onError: ' + e.message); },
				() => { console.log('%c mountsSubject complete', 'color:green'); }
			);


		/* ===== */
		// Отслеживание изменения параметров карниза
		this.corniceSubscription = this.corniceSubject
			.subscribe( 
				(cornice:Cornice) => { 
					console.log('%c Cornice changed! corniceSubject onNext:', 'color:orange', this.cornice); 
					this.isInit = false;
					
					this.onCorniceChanged.emit(this.cornice);
					this.corniceItemId = this.cornice.mounts[0].id;
					this.corniceItemColorId = this.cornice.mounts[0].colorId;
					
					this.basket = this.corniceService.makeBasket(this.cornice);
					this.basketSlider.slidesLoaded();
					
					this.drawCornice(this.cornice);
				},
				(e) => { console.log('corniceSubject onError: ' + e.message); },
				() => { console.log('%c corniceSubject complete', 'color:green'); }
			);

		
		// Отслеживание рендеринга карниза
		this.corniceDrawSubscription = this.corniceDrawSubject
			.subscribe( 
				(x) => { 
					console.log('%c Cornice drawed! corniceDrawSubject onNext: ' + x, 'color:green');
					this.stopPreloading();
				},
				(e) => { console.log('corniceDrawSubject onError: ' + e.message); },
				() => { console.log('%c corniceDrawSubject complete', 'color:green'); }
			);
		}

	getMounts(filter:Filter){
		this.corniceService.getCorniceItems("getMounts", filter)
			.subscribe(
				(response:Response) => {
					// console.log("%c getCorniceItems (%s): ", "color:red", "response", response);
					if(response.ok && response.json().arItems){
						this.swiperItems = response.json().arItems;
						this.timing('getMounts', response.json().times);
						console.log("getCorniceItems (%s): ", "swiperItems", this.swiperItems);

						// смена цвета наконечников
						if(!this.isInit){
							let productId = this.cornice.mounts[0].productId;
							let arFounded = this.swiperItems.filter(function(item) {
								return item.productId == productId;
							});
							if(arFounded.length){
								this.cornice.mounts[0] = arFounded[0];
							}else{
								this.cornice.mounts[0] = this.swiperItems[0];
							}
							// this.filter.colorId = this.mountFilter.colorId;
						}
						this.mountsSubject.next("Mounts");
						this.itemsSlider.slidesLoaded();
						
					}else{
						console.log("%c getCorniceItems (%s): %s not found! Use first.", "color:red", "getMounts", "mounts");
						this.mountFilter.colorId = this.cornice.mounts[0].colorId;
						console.log("this.mountFilter", this.mountFilter);
						this.getMounts(this.mountFilter);
					}

				}
			);
			
	}

	getColors(filter:Filter){
		this.corniceService.getColors(filter, "getMountColors")
			.subscribe(
				(response:Response) => {

					// console.log("%c getColors (%s): ", "color:red", "response", response);

					if(response.ok && response.json().arItems){
						this.colors = response.json().arItems;
						this.timing('getColors', response.json().times);
						console.log("getColors (%s): ", "this.colors", this.colors);
					}else{
						console.log("%c getColors: No colors found", "color:red");
					}
				}
			);
	}


	startPreloading(){
		this.preloader = true;
	}

	stopPreloading(){
		this.preloader = false;
		console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
		this.phpTimes = [];
		this.phpAllTime = 0;
	}

	

	/*=== ОБРАБОТКА СОБЫТИЙ ===*/
	/**
	 * Смена шага
	 */
	gotoStep(step_id:number){
		this.onStepChanged.emit(step_id);
	}

	/**
	 * Отрисовка карниза
	 */
	drawCornice(cornice:Cornice){
		this.corniceService.getCorniceImg(cornice)
			.subscribe(
				(response:Response) => {
					if(response.ok){
						let imgSrc = response.json().arItems;
						let ar = imgSrc.split(".");
						this.cornice.imgSrc = imgSrc + "?v=" + new Date().getTime();
						this.cornice['imgExt'] = ar[1];
						this.timing('drawCornice', response.json().times);
						console.log("getCorniceImg: this.cornice.imgSrc", cornice.imgSrc);
						this.corniceDrawSubject.next(true);
					}
				}
			);
	}

	/**
	 * Добавление скриптов после загрузки
	 */
	ngAfterViewInit() {
	}

	/*=== ОБРАБОТКА СОБЫТИЙ ===*/

	/**
	 * click on color box handler
	 */
	colorSelected(color_id: number){
		this.startPreloading();
		this.mountFilter.colorId = color_id;
		// this.filter.colorId = color_id;
		this.colorsSubject.next("colors");
	}

	/**
	 * click on mount
	 */
	swiperItemSelected(swiperItemId: number){
		this.startPreloading();

		console.log("parts-mount: slideClicked swiperItemId", swiperItemId);
		let arFounded = this.swiperItems.filter(function(item) {
			return item.id == swiperItemId;
		});

		this.cornice.mounts[0] = arFounded[0];
		this.corniceSubject.next(this.cornice);
	}

	buy(cornice:Cornice){
		this.gotoStep(6);
	}

	updateImg(imgSrc:string){
		this.startPreloading();
		this.corniceService.updateImg(imgSrc).subscribe(
				(response:Response) => {
					if(response.ok && response.json().arItems){
						this.drawCornice(this.cornice);
					}
				}
			);
	}

	timing(title:string, times:number){
		if(Global.IS_DEBUG){
			this.phpTimes.push({[title]: times});
			this.phpAllTime += times;
		}
	}


	ngOnDestroy() {
		this.mountsSubscription.unsubscribe();
		this.corniceSubscription.unsubscribe();
		this.corniceDrawSubscription.unsubscribe();
	}

}