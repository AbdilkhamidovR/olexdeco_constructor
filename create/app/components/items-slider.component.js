"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular2_swiper_1 = require("angular2-swiper");
//my
var global_1 = require("../global");
var ItemsSliderComponent = (function () {
    function ItemsSliderComponent() {
        // объявление события изменения фильтра выборки элементов карниза
        this.swiperItemSelected = new core_1.EventEmitter();
        this.itemsSwipeOptions = {
            init: false,
            slidesPerView: 'auto',
            scrollbarHide: false,
            nextButton: ".ks-swiper-container .swiper-button-next",
            prevButton: ".ks-swiper-container .swiper-button-prev",
            scrollbar: ".ks-swiper-container .swiper-scrollbar"
        };
    }
    ItemsSliderComponent.prototype.ngOnInit = function () {
    };
    ItemsSliderComponent.prototype.slidesLoaded = function () {
        // console.log('items-slider: sliderItemsLoaded from parts-mount loaded!');
        this.itemsSwiperContainer.swiper.slideTo(0);
    };
    // moveNext() {
    // 	this.itemsSwiperContainer.swiper.slideNext();
    // }
    // movePrev() {
    // 	this.itemsSwiperContainer.swiper.slidePrev();
    // }
    ItemsSliderComponent.prototype.slideClicked = function (id) {
        // console.log("items-slider: slideClicked id", id);
        this.swiperItemSelected.emit(id);
    };
    ItemsSliderComponent.prototype.ngAfterViewInit = function () {
        // console.log("items-slider: itemsSwiperContainer", this.itemsSwiperContainer);
        // this.itemsSwiperContainer.swiper.update();
    };
    ItemsSliderComponent.prototype.ngOnDestroy = function () {
        this.itemsSwiperContainer.swiper.destroy();
    };
    return ItemsSliderComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], ItemsSliderComponent.prototype, "swiperItems", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], ItemsSliderComponent.prototype, "corniceItemId", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ItemsSliderComponent.prototype, "swiperItemSelected", void 0);
__decorate([
    core_1.ViewChild(angular2_swiper_1.KSSwiperContainer),
    __metadata("design:type", angular2_swiper_1.KSSwiperContainer)
], ItemsSliderComponent.prototype, "itemsSwiperContainer", void 0);
ItemsSliderComponent = __decorate([
    core_1.Component({
        selector: 'items-slider',
        templateUrl: 'app/templates/items-slider.template.html' + ((global_1.Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
    }),
    __metadata("design:paramtypes", [])
], ItemsSliderComponent);
exports.ItemsSliderComponent = ItemsSliderComponent;
//# sourceMappingURL=items-slider.component.js.map