import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';

import { Global } from '../global'; 
import { Step }	from '../data/step';
import { Cornice }	from '../data/cornice';
import { Basket }	from '../data/basket';
import { CorniceService }  from '../services/cornice.service';


@Component({
  selector: 'cornice-checkout',
  templateUrl: 'app/templates/checkout.template.html' + ((Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
})
export class CheckoutComponent  implements OnInit{

	// данные из app.component
	@Input() cornice: Cornice;
	@Input() steps: any;
	@Output() private onStepChanged = new EventEmitter<number>();
	//
	
	private basket: Basket;
	private preloader: boolean = false;
	private showTimer: boolean = false;
	private ticks: number = 0;

	private addToBxBasketSubject: Subject<boolean> = new Subject();
	private addToBxBasketSubscription: Subscription;

	constructor(
		private corniceService: CorniceService,
	) {};

	
	ngOnInit(): void {
		// console.log(this.steps);
		console.clear();
		// console.log(this.cornice);
		this.initStep();
	}

	initStep(){
		// апдэйт корзины
		this.basket = this.corniceService.makeBasket(this.cornice);

		// Отслеживание загрузки цветов
		this.addToBxBasketSubscription = this.addToBxBasketSubject
			.subscribe( 
				(x) => { 
					console.log('%c BxBasket updated! addToBxBasketSubject onNext: ' + x, 'color:green'); 
				},
				(e) => { console.log('addToBxBasketSubject onError: ' + e.message); },
				() => { console.log('%c addToBxBasketSubject complete', 'color:green'); }
			);
			
	}


	startPreloading(){
		this.preloader = true;
	}

	stopPreloading(){
		this.preloader = false;
	}

	addToBxBasket(basket:Basket){
		this.startPreloading();
		this.corniceService.addToBxBasket(basket)
			.subscribe(
				(response:Response) => {
					// console.log("%c addToBxBasket (%s): ", "color:black", "response", response);
					if(response.ok && response.json().arItems){
						console.log("%c addToBxBasket (%s): ", "color:green", "added");
						window.location.href = Global.BX_BASKET_URL;
					}else{
						console.log("%c addToBxBasket: error adding!", "color:red");
					}
				}
			);
	}

	/*=== ОБРАБОТКА ПОЛЬЗОВАТЕЛЬСКИХ СОБЫТИЙ ===*/
	gotoStep(step_id:number){
		this.onStepChanged.emit(step_id);
	}

	checkout(basket:Basket){
		this.addToBxBasket(basket);
	}

	ngOnDestroy() {
		this.addToBxBasketSubscription.unsubscribe();
	}


}
