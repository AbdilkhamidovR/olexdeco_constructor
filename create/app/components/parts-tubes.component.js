"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/zip");
var global_1 = require("../global");
var cornice_1 = require("../data/cornice");
var load_item_1 = require("../data/load-item");
var cornice_service_1 = require("../services/cornice.service");
var filter_1 = require("../data/filter");
var basket_1 = require("../data/basket");
var items_slider_component_1 = require("../components/items-slider.component");
var basket_slider_component_1 = require("../components/basket-slider.component");
var PartsTubesComponent = (function () {
    function PartsTubesComponent(corniceService) {
        this.corniceService = corniceService;
        this.onStepChanged = new core_1.EventEmitter();
        this.onCorniceChanged = new core_1.EventEmitter();
        this.basket = new basket_1.Basket(0);
        this.tubeFormsSubject = new Subject_1.Subject();
        this.colorsSubject = new Subject_1.Subject();
        this.tubeTypesSubject = new Subject_1.Subject();
        this.tubeLengthSubject = new Subject_1.Subject();
        this.tubeTypes2Subject = new Subject_1.Subject();
        this.tubeSubject = new Subject_1.Subject();
        this.capSubject = new Subject_1.Subject();
        this.mountSubject = new Subject_1.Subject();
        this.ringSubject = new Subject_1.Subject();
        this.ring2Subject = new Subject_1.Subject();
        this.ring3Subject = new Subject_1.Subject();
        this.tube2Subject = new Subject_1.Subject();
        this.tube3Subject = new Subject_1.Subject();
        this.cap2Subject = new Subject_1.Subject();
        this.cap3Subject = new Subject_1.Subject();
        this.corniceSubject = new Subject_1.Subject();
        this.corniceDrawSubject = new Subject_1.Subject();
        this.phpTimes = [];
        this.phpAllTime = 0;
        this.loadItems = {
            tubes: [
                new load_item_1.LoadItem("tubes", 0, "tube", "getTube", this.tubeSubject, this.tubeSubscription),
                new load_item_1.LoadItem("tubes", 1, "tube2", "getTube2", this.tube2Subject, this.tube2Subscription),
                new load_item_1.LoadItem("tubes", 2, "tube3", "getTube3", this.tube3Subject, this.tube3Subscription),
            ],
            caps: [
                new load_item_1.LoadItem("caps", 0, "cap", "getCap", this.capSubject, this.capSubscription),
                new load_item_1.LoadItem("caps", 1, "cap2", "getCap2", this.cap2Subject, this.cap2Subscription),
                new load_item_1.LoadItem("caps", 2, "cap3", "getCap3", this.cap3Subject, this.cap3Subscription),
            ],
            mounts: [
                new load_item_1.LoadItem("mounts", 0, "mount", "getMount", this.mountSubject, this.mountSubscription),
            ],
            rings: [
                new load_item_1.LoadItem("rings", 0, "ring", "getRing", this.ringSubject, this.ringSubscription),
                new load_item_1.LoadItem("rings", 1, "ring2", "getRing2", this.ring2Subject, this.ring2Subscription),
                new load_item_1.LoadItem("rings", 2, "ring3", "getRing3", this.ring3Subject, this.ring3Subscription),
            ]
        };
    }
    ;
    /*=========*/
    PartsTubesComponent.prototype.ngOnInit = function () {
        this.startPreloading();
        this.initStep();
    };
    // инициализация шага 1
    PartsTubesComponent.prototype.initStep = function () {
        this.getTubeForms(this.filter);
        this.initSubsciptions();
        console.log('user', this.user);
    };
    PartsTubesComponent.prototype.initSubsciptions = function () {
        var _this = this;
        // Отслеживание загрузки форм труб
        this.tubeFormsSubscription = this.tubeFormsSubject
            .subscribe(function (x) {
            console.log('%c Tube forms loaded! tubeFormsSubject onNext: ' + x, 'color:green');
            _this.getTubeTypes(_this.filter);
        }, function (e) { console.log('tubeFormsSubject onError: ' + e.message); }, function () { console.log('%c tubeFormsSubject complete', 'color:green'); });
        // Отслеживание загрузки типов труб (диаметров и др.)
        this.tubeTypesSubscription = this.tubeTypesSubject
            .subscribe(function (x) {
            console.log('%c Tube types loaded! tubeTypesSubject onNext: ' + x, 'color:green');
            _this.getColors(_this.filter);
        }, function (e) { console.log('tubeTypesSubject onError: ' + e.message); }, function () { console.log('%c tubeTypesSubject complete', 'color:green'); });
        // Отслеживание загрузки типов труб (диаметров и др.) 2-го ряда
        this.tubeTypes2Subscription = this.tubeTypes2Subject
            .subscribe(function (x) {
            console.log('%c Tube types for 2 row loaded! tubeTypes2Subject onNext: ' + x, 'color:green');
            // this.getColors(this.filter);
        }, function (e) { console.log('tubeTypesSubject onError: ' + e.message); }, function () { console.log('%c tubeTypesSubject complete', 'color:green'); });
        // Отслеживание загрузки цветов
        this.colorsSubscription = this.colorsSubject
            .subscribe(function (x) {
            console.log('%c Colors loaded! olorsSubject onNext: ' + x, 'color:green');
            _this.getTubeLength(_this.filter);
            // this.getCorniceItem(this.loadItems.tubes[0], this.filter);
        }, function (e) { console.log('colorsSubject onError: ' + e.message); }, function () { console.log('%c colorsSubject complete', 'color:green'); });
        // Отслеживание загрузки длины трубы
        this.tubeLengthSubscription = this.tubeLengthSubject
            .subscribe(function (x) {
            console.log('%c tubeLength loaded! tubeLength onNext: ' + x, 'color:green');
            _this.getCorniceItem(_this.loadItems.tubes[0], _this.filter);
        }, function (e) { console.log('colorsSubject onError: ' + e.message); }, function () { console.log('%c colorsSubject complete', 'color:green'); });
        // Отслеживание загрузки трубы
        this.tubeSubscription = this.tubeSubject
            .subscribe(function (item) {
            console.log('%c tubeSubject onNext: ', 'color:green', item);
            _this.cornice.tubes[0] = item;
            _this.filter.tubeLength = item.tubeLength;
            _this.getCorniceItem(_this.loadItems.caps[0], _this.filter);
            _this.getCorniceItem(_this.loadItems.mounts[0], _this.filter);
        }, function (e) { console.log('tubeSubject onError: ' + e.message); }, function () { console.log('%c tubeSubject complete', 'color:green'); });
        // Отслеживание загрузки наконечника
        this.capSubscription = this.capSubject
            .subscribe(function (item) {
            console.log('%c capSubject onNext: ', 'color:green', item);
            _this.cornice.caps[0] = item;
        }, function (e) { console.log('capSubject onError: ' + e.message); }, function () { console.log('%c capSubject complete', 'color:green'); });
        // Отслеживание загрузки кронштейна
        this.mountSubscription = this.mountSubject
            .subscribe(function (item) {
            console.log('%c mountSubject onNext: ', 'color:green', item);
            _this.cornice.mounts[0] = item;
            for (var i in item.tubeTypeIds) {
                if (_this.corniceService.contains(item.tubeTypeIds[i], _this.cornice.tubes[0].tubeTypeId)) {
                    _this.filter.tubeTypeIds[i] = _this.cornice.tubes[0].tubeTypeId;
                }
                else {
                    _this.filter.tubeTypeIds[i] = item.tubeTypeIds[i][0];
                }
            }
            // this.filter.tubeTypeIds = item.tubeTypeIds;
            var params = null;
            if (_this.filter.tubeFormIds[0] == global_1.Global.PROFILE) {
                params = { profileId: _this.cornice.tubes[0].productId };
            }
            switch (_this.cornice.nRows) {
                case 1:
                    // карниз 1-рядный
                    _this.getCorniceItem(_this.loadItems.rings[0], _this.filter, params);
                    break;
                case 2:
                    // карниз 2-рядный
                    _this.getTubeTypes(_this.filter, { "nRow": 1 });
                case 3:
                    // карниз 3-рядный
                    _this.getCorniceItem(_this.loadItems.rings[0], _this.filter, params);
                    _this.getCorniceItem(_this.loadItems.tubes[1], _this.filter);
                    _this.getCorniceItem(_this.loadItems.caps[1], _this.filter);
                    break;
            }
        }, function (e) { console.log('mountSubject onError: ' + e.message); }, function () { console.log('%c mountSubject complete', 'color:green'); });
        // Отслеживание загрузки наконечника
        this.ringSubscription = this.ringSubject
            .subscribe(function (item) {
            console.log('%c ringSubject onNext: ', 'color:green', item);
            if (item) {
                _this.cornice.rings[0] = item;
            }
            else {
                delete _this.cornice.rings[0];
            }
            if (_this.cornice.nRows == 2 || _this.cornice.nRows == 3) {
                _this.getRing2(_this.filter);
            }
        }, function (e) { console.log('ringSubject onError: ' + e.message); }, function () { console.log('%c ringSubject complete', 'color:green'); });
        // Отслеживание загрузки трубы второго ряда
        this.tube2Subscription = this.tube2Subject
            .subscribe(function (item) {
            console.log('%c tube2Subject onNext: ', 'color:green', item);
            _this.cornice.tubes[1] = item;
            if (_this.cornice.nRows == 3) {
                _this.getTube3(_this.filter);
            }
        }, function (e) { console.log('tube2Subject onError: ' + e.message); }, function () { console.log('%c tube2Subject complete', 'color:green'); });
        // Отслеживание загрузки трубы третьего ряда
        this.tube3Subscription = this.tube3Subject
            .subscribe(function (item) {
            console.log('%c tube3Subject onNext: ', 'color:green', item);
            _this.cornice.tubes[2] = item;
        }, function (e) { console.log('tube3Subject onError: ' + e.message); }, function () { console.log('%c tube3Subject complete', 'color:green'); });
        // Отслеживание загрузки наконечника 2-го ряда
        this.cap2Subscription = this.cap2Subject
            .subscribe(function (item) {
            console.log('%c cap2Subject onNext: ', 'color:green', item);
            _this.cornice.caps[1] = item;
            if (_this.cornice.nRows == 3) {
                _this.getCap3(_this.filter);
            }
        }, function (e) { console.log('cap2Subject onError: ' + e.message); }, function () { console.log('%c cap2Subject complete', 'color:green'); });
        // Отслеживание загрузки наконечника 3-го ряда
        this.cap3Subscription = this.cap3Subject
            .subscribe(function (item) {
            console.log('%c cap3Subject onNext: ', 'color:green', item);
            _this.cornice.caps[2] = item;
        }, function (e) { console.log('Cap3Subject onError: ' + e.message); }, function () { console.log('%c cap3Subject complete', 'color:green'); });
        // Отслеживание загрузки кольца 2-го ряда
        this.ring2Subscription = this.ring2Subject
            .subscribe(function (item) {
            console.log('%c ring2Subject onNext: ', 'color:green', item);
            if (item) {
                _this.cornice.rings[1] = item;
            }
            else {
                delete _this.cornice.rings[1];
            }
            if (_this.cornice.nRows == 3) {
                _this.getRing3(_this.filter);
            }
        }, function (e) { console.log('ring2Subject onError: ' + e.message); }, function () { console.log('%c ring2Subject complete', 'color:green'); });
        // Отслеживание загрузки кольца 3-го ряда
        this.ring3Subscription = this.ring3Subject
            .subscribe(function (item) {
            console.log('%c ring3Subject onNext: ', 'color:green', item);
            if (item) {
                _this.cornice.rings[2] = item;
            }
            else {
                delete _this.cornice.rings[2];
            }
        }, function (e) { console.log('ring3Subject onError: ' + e.message); }, function () { console.log('%c ring3Subject complete', 'color:green'); });
        //======//
        // Параллельная загрузка доп. рядов
        switch (this.cornice.nRows) {
            case 1:
                // карниз 1-рядный
                Observable_1.Observable.zip(this.capSubject, this.ringSubject, this.mountSubject)
                    .subscribe(function (items) {
                    console.log('%c nRows: 1. Observable.zip onNext: ', 'color:green', items);
                    _this.corniceSubject.next(_this.cornice);
                });
                break;
            case 2:
                // карниз 2-рядный
                Observable_1.Observable.zip(this.capSubject, this.ringSubject, this.tube2Subject, this.cap2Subject, this.ring2Subject)
                    .subscribe(function (items) {
                    console.log('%c nRows: 2. Observable.zip onNext: ', 'color:green', items);
                    _this.corniceSubject.next(_this.cornice);
                });
                break;
            case 3:
                // карниз 3-рядный
                // грузим параллельно
                Observable_1.Observable.zip(this.capSubject, this.ringSubject, this.tube2Subject, this.cap2Subject, this.tube3Subject, this.cap3Subject, this.ring2Subject, this.ring3Subject)
                    .subscribe(function (items) {
                    console.log('%c nRows: 3. Observable.zip onNext: ', 'color:green', items);
                    _this.corniceSubject.next(_this.cornice);
                });
                break;
        }
        /* ===== */
        // Отслеживание изменения параметров карниза
        this.corniceSubscription = this.corniceSubject
            .subscribe(function (cornice) {
            console.log('%c Cornice changed! corniceSubject onNext:', 'color:orange', _this.cornice);
            // установка кол-ва для корзины
            _this.cornice.mounts[0].length = _this.corniceService.getMountLength(_this.cornice.tubes[0].tubeLength);
            for (var i in _this.cornice.rings) {
                _this.cornice.rings[i].length = _this.corniceService.getBasketRingsInRowLength(_this.cornice);
            }
            _this.onCorniceChanged.emit(_this.cornice);
            _this.basket = _this.corniceService.makeBasket(_this.cornice);
            _this.basketSlider.slidesLoaded();
            _this.drawCornice(_this.cornice);
            _this.corniceItemId = _this.filter.tubeTypeIds[0];
            _this.corniceItemId2 = _this.filter.tubeTypeIds[1];
            _this.corniceItemColorId = _this.cornice.tubes[0].colorId;
            if (!_this.cornice.rings[0]) {
                _this.steps[4].setDisable();
            }
            else {
                _this.steps[4].setEnable();
            }
        }, function (e) { console.log('corniceSubject onError: ' + e.message); }, function () { console.log('%c corniceSubject complete', 'color:green'); });
        // Отслеживание рендеринга карниза
        this.corniceDrawSubscription = this.corniceDrawSubject
            .subscribe(function (x) {
            console.log('%c Cornice drawed! corniceDrawSubject onNext: ' + x, 'color:green');
            _this.stopPreloading();
        }, function (e) { console.log('corniceDrawSubject onError: ' + e.message); }, function () { console.log('%c corniceDrawSubject complete', 'color:green'); });
    };
    PartsTubesComponent.prototype.getTubeForms = function (filter) {
        var _this = this;
        this.corniceService.getTubeForms(filter).subscribe(function (response) {
            if (response.ok) {
                _this.tubeForms = response.json().arItems;
                _this.timing('getTubeForms', response.json().times);
                console.log("initTubes: this.tubeForms", _this.tubeForms, response.json().times);
                if (!_this.filter.tubeFormIds[0]) {
                    _this.filter.tubeFormIds[0] = _this.tubeForms[0].id;
                }
                _this.tubeFormsSubject.next("tubeForms");
            }
        });
    };
    PartsTubesComponent.prototype.getTubeTypes = function (filter, params) {
        var _this = this;
        if (params === void 0) { params = null; }
        var nRow = 0;
        if (params && params.hasOwnProperty('nRow')) {
            nRow = params["nRow"];
        }
        this.corniceService.getTubeTypes(filter, params).subscribe(function (response) {
            if (response.ok) {
                switch (nRow) {
                    case 0:
                        // получаем типы труб (диаметры и др.) для 1-го ряда
                        _this.tubeTypes = response.json().arItems;
                        _this.swiperItems = _this.tubeTypes;
                        _this.timing('getTubeTypes', response.json().times);
                        console.log("initTubeTypes: this.TubeTypes", _this.tubeTypes);
                        if (_this.tubeTypes.length) {
                            if (!_this.filter.tubeTypeIds[0]) {
                                // инициализация
                                _this.filter.tubeTypeIds[0] = _this.tubeTypes[0].id;
                            }
                            else {
                                var isTubeTypeFound = false;
                                for (var _i = 0, _a = _this.tubeTypes; _i < _a.length; _i++) {
                                    var tubeType = _a[_i];
                                    if (tubeType.id == _this.filter.tubeTypeIds[0]) {
                                        isTubeTypeFound = true;
                                        break;
                                    }
                                }
                                if (!isTubeTypeFound) {
                                    console.log("%c getTubeTypes: No tubeType selected", "color:red");
                                    _this.filter.tubeTypeIds[0] = _this.tubeTypes[0].id;
                                }
                            }
                            _this.tubeTypesSubject.next("tubeTypes");
                            _this.itemsSlider.slidesLoaded();
                        }
                        else {
                            console.log("%c getTubeTypes: No tubeType found", "color:red");
                            _this.reloadStep();
                        }
                        break;
                    case 1:
                        // получаем типы труб (диаметры и др.) для 2-го ряда
                        _this.tubeTypes2 = response.json().arItems;
                        _this.swiperItems2 = _this.tubeTypes2;
                        var isTubeType2Found = false;
                        for (var _b = 0, _c = _this.tubeTypes2; _b < _c.length; _b++) {
                            var tubeType2 = _c[_b];
                            if (tubeType2.id == _this.filter.tubeTypeIds[1]) {
                                isTubeType2Found = true;
                                break;
                            }
                        }
                        // console.log("getTubeTypes: TubeTypes2 this.filter", this.filter);
                        console.log("getTubeTypes: this.TubeTypes2", _this.tubeTypes2);
                        if (!isTubeType2Found) {
                            console.log("%c getTubeTypes: No tubeType2 founded", "color:red");
                            _this.filter.tubeTypeIds[1] = _this.tubeTypes2[0].id;
                        }
                        _this.tubeTypes2Subject.next("tubeTypes2");
                        if (_this.swiperItems2.length > 1) {
                            _this.itemsSlider.slidesLoaded();
                        }
                        break;
                    default:
                        // code...
                        break;
                }
            }
        });
    };
    PartsTubesComponent.prototype.getColors = function (filter) {
        var _this = this;
        this.corniceService.getColors(filter, "getTubeColors")
            .subscribe(function (response) {
            if (response.ok) {
                _this.colors = response.json().arItems;
                _this.timing('getColors', response.json().times);
                if (_this.colors.length) {
                    if (!_this.filter.colorId) {
                        _this.filter.colorId = _this.colors[0].id;
                    }
                    else {
                        var isColorFound = false;
                        for (var _i = 0, _a = _this.colors; _i < _a.length; _i++) {
                            var color = _a[_i];
                            if (color.id == _this.filter.colorId) {
                                isColorFound = true;
                                break;
                            }
                        }
                        if (!isColorFound) {
                            console.log("%c getColors: No colors selected", "color:red");
                            _this.filter.colorId = _this.colors[0].id;
                        }
                    }
                    _this.colorsSubject.next("colors");
                }
                else {
                    console.log("%c getColors: No colors found", "color:red");
                    _this.filter.colorId = 0;
                    _this.tubeTypesSubject.next("tubeTypes");
                }
            }
        });
    };
    PartsTubesComponent.prototype.getTubeLength = function (filter) {
        var _this = this;
        this.corniceService.getTubeLength(filter).subscribe(function (response) {
            console.log('getTubeLength response', response);
            if (response.ok) {
                _this.timing('getTubeLength', response.json().times);
                console.log("getTubeLength: tubeLengths", response.json().arItems, response.json().times);
                if (response.json().arItems.length) {
                    _this.filter.tubeLength = response.json().arItems[0].value;
                }
            }
            _this.tubeLengthSubject.next("tubeLengths");
        });
    };
    PartsTubesComponent.prototype.getCorniceItem = function (loadItem, filter, params) {
        // alert('getCorniceItem ('+loadItem.func+'):' + filter.tubeTypeIds[1]);
        var _this = this;
        if (params === void 0) { params = null; }
        this.corniceService.getCorniceItem(loadItem.func, filter, params)
            .subscribe(function (response) {
            var corniceItem;
            if (response.ok && response.json().arItems) {
                // use new cornice item
                corniceItem = response.json().arItems;
                _this.timing(loadItem.func, response.json().times);
            }
            else {
                console.log("%c getCorniceItem (%s): %s not found! Use previous value.", "color:red", loadItem.func, loadItem.name, _this.cornice);
                // use setted cornice item in this.cornice
                corniceItem = _this.cornice[loadItem.corniceKey][loadItem.corniceIndx];
                // exlude mount for different number of rows
                if (loadItem.name == "mount" && _this.cornice.mounts[0].tubeTypeIds.length != _this.cornice.nRows) {
                    corniceItem = undefined;
                }
                // exlude rings for profiles
                if (filter.tubeFormIds[0] == global_1.Global.PROFILE && loadItem.corniceKey == "rings") {
                    corniceItem = undefined;
                }
            }
            if (corniceItem) {
                loadItem.subject.next(corniceItem);
            }
            else {
                console.log("%c %s: reset filter and reload", "color:red", loadItem.func);
                if (filter.tubeFormIds[0] == global_1.Global.PROFILE && loadItem.corniceKey == "rings") {
                    loadItem.subject.next(corniceItem);
                }
                else {
                    _this.reloadStep();
                }
            }
        });
    };
    PartsTubesComponent.prototype.getTube2 = function (filter) {
        if (this.cornice.mounts[0] && this.cornice.tubes[0] &&
            this.corniceService.contains(this.cornice.mounts[0].tubeTypeIds[1], this.cornice.tubes[0].tubeTypeId)) {
            this.cornice.tubes[1] = this.cornice.tubes[0];
            this.tube2Subject.next(this.cornice.tubes[1]);
        }
        else {
            this.getCorniceItem(this.loadItems.tubes[1], filter);
        }
    };
    PartsTubesComponent.prototype.getRing2 = function (filter) {
        if (this.cornice.mounts[0] && this.cornice.rings[0] &&
            this.corniceService.contains(this.cornice.mounts[0].tubeTypeIds[1], this.cornice.tubes[0].tubeTypeId)) {
            this.cornice.rings[1] = this.cornice.rings[0];
            this.ring2Subject.next(this.cornice.rings[1]);
        }
        else {
            this.getCorniceItem(this.loadItems.rings[1], filter);
        }
    };
    PartsTubesComponent.prototype.getTube3 = function (filter) {
        if (this.cornice.mounts[0] && this.cornice.tubes[0] &&
            this.corniceService.contains(this.cornice.mounts[0].tubeTypeIds[2], this.cornice.tubes[0].tubeTypeId)) {
            this.cornice.tubes[2] = this.cornice.tubes[0];
            this.tube3Subject.next(this.cornice.tubes[2]);
        }
        else {
            this.getCorniceItem(this.loadItems.tubes[2], filter);
        }
    };
    ;
    PartsTubesComponent.prototype.getCap3 = function (filter) {
        if (this.cornice.mounts[0] && this.cornice.caps[1] &&
            this.corniceService.isArraysEquals(this.cornice.mounts[0].tubeTypeIds[1], this.cornice.mounts[0].tubeTypeIds[2])) {
            this.cornice.caps[2] = this.cornice.caps[1];
            this.cap3Subject.next(this.cornice.caps[2]);
        }
        else {
            this.getCorniceItem(this.loadItems.caps[2], filter);
        }
    };
    ;
    PartsTubesComponent.prototype.getRing3 = function (filter) {
        if (this.cornice.mounts[0] && this.cornice.rings[0] && this.cornice.tubes[0] &&
            this.corniceService.contains(this.cornice.mounts[0].tubeTypeIds[2], this.cornice.tubes[0].tubeTypeId)) {
            this.cornice.rings[2] = this.cornice.rings[0];
            this.ring3Subject.next(this.cornice.rings[2]);
        }
        else {
            this.getCorniceItem(this.loadItems.rings[2], filter);
        }
    };
    ;
    // ******* //
    PartsTubesComponent.prototype.drawCornice = function (cornice) {
        var _this = this;
        this.corniceService.getCorniceImg(cornice)
            .subscribe(function (response) {
            // console.log("getCorniceImg: response", response);
            if (response.ok) {
                var imgSrc = response.json().arItems;
                var ar = imgSrc.split(".");
                _this.cornice.imgSrc = imgSrc + "?v=" + new Date().getTime();
                _this.cornice['imgExt'] = ar[1];
                _this.timing('drawCornice', response.json().times);
                console.log("getCorniceImg: this.cornice.imgSrc", _this.cornice.imgSrc);
                _this.corniceDrawSubject.next(true);
            }
        });
    };
    /*=== ОБРАБОТКА СОБЫТИЙ ===*/
    /**
     * Смена шага
     */
    PartsTubesComponent.prototype.gotoStep = function (step_id) {
        this.onStepChanged.emit(step_id);
    };
    PartsTubesComponent.prototype.printImg = function (imgSrc) {
        this.corniceService.printImg(imgSrc);
    };
    /**
     * Добавление скриптов после загрузки
     */
    PartsTubesComponent.prototype.ngAfterViewInit = function () {
    };
    // загрузка скрипта для запуска слайдеров
    // loadMainJS(){
    // 	var el = document.getElementById('main-js');
    // 	if(!el){
    // 		var s = document.createElement("script");
    // 		s.type = "text/javascript";
    // 		s.id = "main-js";
    // 		s.src = "./assets/js/main.js?v=1.3";
    // 		this.elementRef.nativeElement.appendChild(s);
    // 	}
    // }
    /**
     * click on tube type handler
     */
    PartsTubesComponent.prototype.tubeFormSelected = function (tubeForm_id) {
        this.startPreloading();
        this.cornice.resetLeftSide(); // очистка настроек карниза слева на 6-м шаге
        this.cornice.resetRightSide(); // очистка настроек карниза справа на 6-м шаге
        this.filter.tubeFormIds[0] = tubeForm_id;
        this.tubeFormsSubject.next("tubeForms");
    };
    /**
     * click on color box handler
     */
    PartsTubesComponent.prototype.colorSelected = function (color_id) {
        this.startPreloading();
        this.cornice.resetLeftSide(); // очистка настроек карниза слева на 6-м шаге
        this.cornice.resetRightSide(); // очистка настроек карниза справа на 6-м шаге
        this.filter.colorId = color_id;
        this.colorsSubject.next("colors");
    };
    /**
     * click on tube type handler
     */
    PartsTubesComponent.prototype.swiperItemSelected = function (tubeType_id) {
        console.log('swiperItemSelected tubeType_id', tubeType_id);
        this.startPreloading();
        this.cornice.resetLeftSide(); // очистка настроек карниза слева на 6-м шаге
        this.cornice.resetRightSide(); // очистка настроек карниза справа на 6-м шаге
        this.filter.tubeTypeIds[0] = tubeType_id;
        this.tubeTypesSubject.next("tubeTypes");
        // this.getCorniceItem(this.loadItems.tubes[0], this.filter);
    };
    /**
     * click on tube type for 2 row handler
     */
    PartsTubesComponent.prototype.swiperItemSelected2 = function (tubeType_id) {
        this.startPreloading();
        this.cornice.resetLeftSide(); // очистка настроек карниза слева на 6-м шаге
        this.cornice.resetRightSide(); // очистка настроек карниза справа на 6-м шаге
        this.filter.tubeTypeIds[1] = tubeType_id;
        this.getCorniceItem(this.loadItems.mounts[0], this.filter, { "nRow": 1 });
        this.capSubject.next(this.cornice.caps[0]);
    };
    PartsTubesComponent.prototype.resetFilter = function () {
        this.filter.tubeFormIds = [0, 0, 0];
        this.filter.tubeTypeIds = [0, 0, 0];
        this.filter.colorId = 0;
    };
    PartsTubesComponent.prototype.reloadStep = function () {
        this.ngOnDestroy();
        this.resetFilter();
        this.initStep();
    };
    PartsTubesComponent.prototype.buy = function (cornice) {
        this.gotoStep(6);
    };
    PartsTubesComponent.prototype.updateImg = function (imgSrc) {
        var _this = this;
        this.startPreloading();
        this.corniceService.updateImg(imgSrc).subscribe(function (response) {
            if (response.ok && response.json().arItems) {
                _this.drawCornice(_this.cornice);
            }
        });
    };
    PartsTubesComponent.prototype.startPreloading = function () {
        this.preloader = true;
    };
    PartsTubesComponent.prototype.stopPreloading = function () {
        this.preloader = false;
        console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
        this.phpTimes = [];
        this.phpAllTime = 0;
    };
    PartsTubesComponent.prototype.timing = function (title, times) {
        if (global_1.Global.IS_DEBUG) {
            this.phpTimes.push((_a = {}, _a[title] = times, _a));
            this.phpAllTime += times;
        }
        var _a;
    };
    PartsTubesComponent.prototype.mobBasketlinkClicked = function () {
        this.isMobileBasketActive = !this.isMobileBasketActive;
        console.log('mobBasketlinkClicked', this.isMobileBasketActive);
    };
    PartsTubesComponent.prototype.ngOnDestroy = function () {
        this.tubeFormsSubscription.unsubscribe();
        this.colorsSubscription.unsubscribe();
        this.tubeTypesSubscription.unsubscribe();
        this.tubeSubscription.unsubscribe();
        this.capSubscription.unsubscribe();
        this.mountSubscription.unsubscribe();
        this.tube2Subscription.unsubscribe();
        this.tube3Subscription.unsubscribe();
        this.cap2Subscription.unsubscribe();
        this.cap3Subscription.unsubscribe();
        this.ringSubscription.unsubscribe();
        this.ring2Subscription.unsubscribe();
        this.ring3Subscription.unsubscribe();
        this.corniceDrawSubscription.unsubscribe();
    };
    return PartsTubesComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", cornice_1.Cornice)
], PartsTubesComponent.prototype, "cornice", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", filter_1.Filter)
], PartsTubesComponent.prototype, "filter", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], PartsTubesComponent.prototype, "steps", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], PartsTubesComponent.prototype, "user", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], PartsTubesComponent.prototype, "isMobileBasketActive", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PartsTubesComponent.prototype, "onStepChanged", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PartsTubesComponent.prototype, "onCorniceChanged", void 0);
__decorate([
    core_1.ViewChild('itemsSlider'),
    __metadata("design:type", items_slider_component_1.ItemsSliderComponent)
], PartsTubesComponent.prototype, "itemsSlider", void 0);
__decorate([
    core_1.ViewChild('basketSlider'),
    __metadata("design:type", basket_slider_component_1.BasketSliderComponent)
], PartsTubesComponent.prototype, "basketSlider", void 0);
PartsTubesComponent = __decorate([
    core_1.Component({
        selector: 'cornice-parts-tubes',
        animations: [
            core_2.trigger('mobileBasketTrigger', [
                core_2.state('true', core_2.style({ right: '0' })),
                core_2.state('false', core_2.style({ right: '-100%' })),
                core_2.transition('1 => 0', core_2.animate('200ms ease-in-out')),
                core_2.transition('0 => 1', core_2.animate('200ms ease-in-out'))
            ]),
        ],
        templateUrl: 'app/templates/parts.template.html' + ((global_1.Global.IS_DEBUG) ? '?v=' + Math.random() : '')
    }),
    __metadata("design:paramtypes", [cornice_service_1.CorniceService])
], PartsTubesComponent);
exports.PartsTubesComponent = PartsTubesComponent;
//# sourceMappingURL=parts-tubes.component.js.map