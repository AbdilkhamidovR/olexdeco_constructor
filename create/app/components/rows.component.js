"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var global_1 = require("../global"); // мои глобальные константы
var cornice_1 = require("../data/cornice");
var cornice_service_1 = require("../services/cornice.service");
var filter_1 = require("../data/filter");
var RowsComponent = (function () {
    function RowsComponent(corniceService) {
        this.corniceService = corniceService;
        this.isVisible = false;
        this.preloader = false;
        this.phpTimes = [];
        this.phpAllTime = 0;
        // объявление события изменения фильтра выборки элементов карниза
        this.onFilterChanged = new core_1.EventEmitter();
        this.onCorniceChanged = new core_1.EventEmitter();
        this.rowQuantitiesSubject = new Subject_1.Subject();
    }
    RowsComponent.prototype.ngOnInit = function () {
        this.startPreloading();
        this.getRowQuantities();
    };
    RowsComponent.prototype.startPreloading = function () {
        this.preloader = true;
        this.isVisible = false;
    };
    RowsComponent.prototype.stopPreloading = function () {
        this.preloader = false;
        this.isVisible = true;
        console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
        this.phpTimes = [];
        this.phpAllTime = 0;
    };
    /* Навигация: выбор кол-ва рядов */
    RowsComponent.prototype.rowsSelected = function (nRowsId) {
        if (nRowsId === void 0) { nRowsId = 0; }
        this.steps[0].setDisactive();
        this.steps[1].setActive();
        this.steps[2].setEnable();
        this.steps[3].setEnable();
        this.steps[4].setEnable();
        this.steps[5].setEnable();
        this.steps[6].setEnable();
        this.cornice.activeStep = 1;
        this.filter.nRowsId = nRowsId;
        this.cornice.nRows = global_1.Global.NROWS[this.filter.nRowsId];
        this.cornice.caps.length = global_1.Global.NROWS[nRowsId];
        this.cornice.tubes.length = global_1.Global.NROWS[nRowsId];
        this.cornice.rings.length = global_1.Global.NROWS[nRowsId];
        this.filter.tubeFormIds.length = global_1.Global.NROWS[nRowsId];
        this.filter.tubeTypeIds.length = global_1.Global.NROWS[nRowsId];
        console.log("%c rowsSelected: this.filter", "color:red", this.filter);
        this.onFilterChanged.emit(this.filter);
        this.onCorniceChanged.emit(this.cornice);
    };
    RowsComponent.prototype.getRowQuantities = function () {
        var _this = this;
        this.corniceService.getRowQuantities()
            .subscribe(function (response) {
            if (response.ok) {
                // console.log("getRowQuantities: this.response", response);
                if (response.json().arItems) {
                    _this.rows = response.json().arItems;
                    _this.timing('getRowQuantities', response.json().times);
                    _this.stopPreloading();
                }
            }
        });
    };
    RowsComponent.prototype.timing = function (title, times) {
        if (global_1.Global.IS_DEBUG) {
            this.phpTimes.push((_a = {}, _a[title] = times, _a));
            this.phpAllTime += times;
        }
        var _a;
    };
    RowsComponent.prototype.ngOnDestroy = function () {
    };
    return RowsComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", cornice_1.Cornice)
], RowsComponent.prototype, "cornice", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", filter_1.Filter)
], RowsComponent.prototype, "filter", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], RowsComponent.prototype, "steps", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], RowsComponent.prototype, "onFilterChanged", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], RowsComponent.prototype, "onCorniceChanged", void 0);
RowsComponent = __decorate([
    core_1.Component({
        selector: 'cornice-rows',
        animations: [
            core_2.trigger('visibilityTrigger', [
                core_2.state('true', core_2.style({ opacity: 1, transform: 'scale(1.0)' })),
                core_2.state('false', core_2.style({ opacity: 0, transform: 'scale(0.0)' })),
                core_2.transition('1 => 0', core_2.animate('200ms ease-out')),
                core_2.transition('0 => 1', core_2.animate('200ms cubic-bezier(0.3, 0.3, .8, 1.5)'))
            ])
        ],
        templateUrl: 'app/templates/rows.template.html' + ((global_1.Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
    }),
    __metadata("design:paramtypes", [cornice_service_1.CorniceService])
], RowsComponent);
exports.RowsComponent = RowsComponent;
//# sourceMappingURL=rows.component.js.map