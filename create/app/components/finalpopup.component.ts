import { Component, OnInit, Input } from '@angular/core';
import { ElementRef } from '@angular/core';

import { Step }	from '../data/step';
import { Cornice }	from '../data/cornice';

@Component({
  selector: 'cornice-finalpopup',
  templateUrl: 'app/templates/finalpopup.template.html',
})
export class FinalpopupComponent  implements OnInit{


	// данные из app.component
	@Input() cornice: Cornice;
	@Input() steps: any;

	constructor(
		private elementRef: ElementRef,
	) {};

	
	ngOnInit(): void {
		// console.log(this.steps);
		console.log(this.cornice);
	}

}
