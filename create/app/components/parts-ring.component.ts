import { Component, EventEmitter, OnInit, Input, Output, ElementRef, ViewChild } from '@angular/core';
import { trigger, state, animate, transition, style} from '@angular/core';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/zip';

import { Global } from '../global'; 
import { Step }	from '../data/step';
import { Cornice }	from '../data/cornice';
import { CorniceItem }	from '../data/cornice-item';
import { CorniceService }  from '../services/cornice.service';
import { Filter }	from '../data/filter';
import { Color }	from '../data/color';
import { Basket }	from '../data/basket';
import { ItemsSliderComponent }  from '../components/items-slider.component';
import { BasketSliderComponent }  from '../components/basket-slider.component';

@Component({
	selector: 'cornice-parts-ring',
	animations: [
			trigger('mobileBasketTrigger', [
				state('true' , style({ right: '0' })),
				state('false', style({ right: '-100%' })),
				transition('1 => 0', animate('200ms ease-in-out')),
				transition('0 => 1', animate('200ms ease-in-out'))
			]),
		],
	templateUrl: 'app/templates/parts.template.html' + ((Global.IS_DEBUG) ? '?v=' + Math.random() : '')
})
export class PartsRingComponent  implements OnInit{

	// данные из app.component
	@Input() private cornice: Cornice;
	@Input() private filter: Filter;
	@Input() private steps: any;
	@Input() private user: object;
	@Input() private isMobileBasketActive: boolean;

	@Output() private onStepChanged = new EventEmitter<number>();
	@Output() private onCorniceChanged = new EventEmitter<Cornice>();

	@ViewChild('itemsSlider')
	private itemsSlider: ItemsSliderComponent;

	@ViewChild('basketSlider')
	private basketSlider: BasketSliderComponent;

	//
	private preloader: boolean;
	private isInit: boolean = true;

	private colors: Color[];
	private ringFilter: Filter;
	private swiperItems: CorniceItem[];
	private corniceItemId: number;
	private corniceItemColorId: string;
	private basket: Basket = new Basket(0);
	private basketItemKeys: string[];
	private phpTimes: object[] = [];
	private phpAllTime: number = 0;
	//
	private ringsSubject: Subject<string> = new Subject();
	private ringsSubscription: Subscription;

	private ring2Subject: Subject<CorniceItem> = new Subject();
	private ring2Subscription: Subscription;

	private ring3Subject: Subject<CorniceItem> = new Subject();
	private ring3Subscription: Subscription;

	private colorsSubject: Subject<string> = new Subject();
	private colorsSubscription: Subscription;

	private corniceSubject: Subject<Cornice> = new Subject();
	private corniceSubscription: Subscription;

	private corniceDrawSubject: Subject<boolean> = new Subject();
	private corniceDrawSubscription: Subscription;
	//


	constructor(
		private elementRef: ElementRef,
		private corniceService: CorniceService,
	) {
	};

		
	/*=========*/
	ngOnInit(): void {
		this.startPreloading();

		// загрузка шага
		this.initStep();
		this.initSubscriptions();
	}

	// инициализация данных шага 1. Выбор трубы (профиля)
	initStep(){
		console.clear();

		this.ringFilter = new Filter(this.filter.nRowsId, this.filter.tubeFormIds, this.filter.tubeTypeIds, this.filter.tubeLength, this.filter.colorId, 0);
		this.corniceItemId = this.cornice.rings[0].id;
		this.corniceItemColorId = this.cornice.rings[0].colorId;

		this.basket = this.corniceService.makeBasket(this.cornice);

		this.getColors(this.ringFilter);
	}


	initSubscriptions(){
		// Отслеживание загрузки цветов
		this.colorsSubscription = this.colorsSubject
			.subscribe( 
				(x) => { 
					console.log('%c Colors loaded! olorsSubject onNext: ' + x, 'color:green'); 
					this.getRings(this.ringFilter);

				},
				(e) => { console.log('colorsSubject onError: ' + e.message); },
				() => { console.log('%c colorsSubject complete', 'color:green'); }
			);


		// Отслеживание загрузки наконечников
		this.ringsSubscription = this.ringsSubject
			.subscribe( 
				(s) => { 
					console.log('%c ringsSubject onNext: ', 'color:green',  s);
					// this.corniceSubject.next(this.cornice);
					if(this.isInit){
						this.stopPreloading();
						this.isInit = false;
					}else{
						
						this.ringFilter["name"] = this.getRingNameToFilter(this.cornice.rings[0].name)

						switch (this.cornice.nRows) {
							case 3:
								// карниз 3-рядный
								this.getRing3byRing1(this.ringFilter);
							case 2:
								// карниз 2-рядный
								this.getRing2byRing1(this.ringFilter);
							case 1:
								// карниз 1-рядный
								break;
						}
					}
				},
				(e) => { console.log('ringsSubject onError: ' + e.message); },
				() => { console.log('%c ringsSubject complete', 'color:green'); }
			);

		// Отслеживание загрузки наконечника 2-го ряда
		this.ring2Subscription = this.ring2Subject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c ring2Subject onNext: ', 'color:green',  item); 
					this.cornice.rings[1] = item;
				},
				(e) => { console.log('ring2Subject onError: ' + e.message); },
				() => { console.log('%c ring2Subject complete', 'color:green'); }
			);

		// Отслеживание загрузки наконечника 3-го ряда
		this.ring3Subscription = this.ring3Subject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c ring3Subject onNext: ', 'color:green', item); 
					this.cornice.rings[2] = item;
				},
				(e) => { console.log('Ring3Subject onError: ' + e.message); },
				() => { console.log('%c ring3Subject complete', 'color:green'); }
			);



		//======//
		// Параллельная загрузка доп. рядов
		switch (this.cornice.nRows) {
			case 1:
				// карниз 1-рядный
				Observable.zip(this.colorsSubject, this.ringsSubject)
					.subscribe(items => {
						console.log('%c nRows: 1. Observable.zip onNext: ' , 'color:green', items);
				   	this.corniceSubject.next(this.cornice);
				});
				break;
			case 2:
				// карниз 2-рядный
				Observable.zip(this.colorsSubject, this.ringsSubject, this.ring2Subject)
					.subscribe(items => {
						console.log('%c nRows: 2. Observable.zip onNext: ' , 'color:green', items);
				   	this.corniceSubject.next(this.cornice);
				});
				break;
			case 3:
				// карниз 3-рядный
				Observable.zip(this.ringsSubject, this.ring2Subject, this.ring3Subject)
					.subscribe(items => {
						console.log('%c nRows: 3. Observable.zip onNext: ', 'color:green', items);
				   	this.corniceSubject.next(this.cornice);
				});
				break;
		}

		/* ===== */
		// Отслеживание изменения параметров карниза
		this.corniceSubscription = this.corniceSubject
			.subscribe( 
				(cornice:Cornice) => { 
					console.log('%c Cornice changed! corniceSubject onNext:', 'color:orange', this.cornice); 
					// console.log('this.basket', this.basket); 
					this.onCorniceChanged.emit(this.cornice);
					this.corniceItemId = this.cornice.rings[0].id;
					this.corniceItemColorId = this.cornice.rings[0].colorId;

					for (let i in this.cornice.rings) {
						this.cornice.rings[i].length = this.corniceService.getBasketRingsInRowLength(this.cornice);
					}

					this.basket = this.corniceService.makeBasket(this.cornice);
					this.basketSlider.slidesLoaded();
					
					this.drawCornice(this.cornice);
				},
				(e) => { console.log('corniceSubject onError: ' + e.message); },
				() => { console.log('%c corniceSubject complete', 'color:green'); }
			);

		
		// Отслеживание рендеринга карниза
		this.corniceDrawSubscription = this.corniceDrawSubject
			.subscribe( 
				(x) => { 
					console.log('%c Cornice drawed! corniceDrawSubject onNext: ' + x, 'color:green');
					this.stopPreloading();
				},
				(e) => { console.log('corniceDrawSubject onError: ' + e.message); },
				() => { console.log('%c corniceDrawSubject complete', 'color:green'); }
			);
	}

	getRings(filter:Filter){
		// this.filter.tubeTypeIds = item.tubeTypeIds;
		let params = null;
		if(this.filter.tubeFormIds[0] == Global.PROFILE){
			params = {profileId: this.cornice.tubes[0].productId}
		}
		this.corniceService.getCorniceItems("getRings", filter, params)
			.subscribe(
				(response:Response) => {
					// console.log("%c getCorniceItems (%s): ", "color:red", "response", response);
					if(response.ok && response.json().arItems){
						this.swiperItems = response.json().arItems;
						this.timing('getRings', response.json().times);
						console.log("getCorniceItems (%s): ", "swiperItems", this.swiperItems);

						// смена цвета наконечников
						if(!this.isInit){

							let productId = this.cornice.rings[0].productId;
							let arFounded = this.swiperItems.filter(function(item) {
								return item.productId == productId;
							});
							if(arFounded.length){
								this.cornice.rings[0] = arFounded[0];
							}else{
								this.cornice.rings[0] = this.swiperItems[0];
							}
						}
						this.ringsSubject.next("SliderItems");
						this.itemsSlider.slidesLoaded();
						
					}else{
						console.log("%c getCorniceItems (%s): %s not found!", "color:red", "getRings", "rings");
						if(this.cornice.rings[0]){
							this.ringFilter.colorId = this.cornice.rings[0].colorId;
						}else{
							this.ringFilter.colorId = "";
						}
						console.log("this.ringFilter", this.ringFilter);
						this.getRings(this.ringFilter);
					}

				}
			);
			
	}

	getRing2byRing1(filter:Filter){
		let params = null;
		if(this.filter.tubeFormIds[0] == Global.PROFILE){
			params = {profileId: this.cornice.tubes[1].productId}
		}
		this.corniceService.getCorniceItem("getRing2", filter, params)
			.subscribe(
				(response:Response) => {
					let corniceItem:CorniceItem;
					if(response.ok && response.json().arItems){
						// use new cornice item
						this.cornice.rings[1] = response.json().arItems;
						this.timing('getRing2byRing1', response.json().times);
					}else{
						console.log("%c getCorniceItem (%s): %s not found! Use previous value.", "color:red", "getRing2", "Ring2", this.cornice);
					}
					if(this.cornice.nRows == 3){
						this.getRing3byRing1(filter);
					}else{
						this.corniceSubject.next(this.cornice);
					}
				}
			);
	}

	getRing3byRing1(filter:Filter){
		let params = null;
		if(this.filter.tubeFormIds[0] == Global.PROFILE){
			params = {profileId: this.cornice.tubes[2].productId}
		}
		this.corniceService.getCorniceItem("getRing2", filter)
			.subscribe(
				(response:Response) => {
					let corniceItem:CorniceItem;
					if(response.ok && response.json().arItems){
						// use new cornice item
						this.cornice.rings[2] = response.json().arItems;
						this.timing('getRing3byRing1', response.json().times);
					}else{
						console.log("%c getCorniceItem (%s): %s not found! Use previous value.", "color:red", "getRing2", "Ring2", this.cornice);
					}
					this.corniceSubject.next(this.cornice);
				}
			);
	}

	getColors(filter:Filter){
		this.corniceService.getColors(filter, "getRingsColors")
			.subscribe(
				(response:Response) => {

					// console.log("%c getColors (%s): ", "color:red", "response", response);

					if(response.ok && response.json().arItems){
						this.colors = response.json().arItems;
						this.timing('getColors', response.json().times);
						console.log("getColors (%s): ", "this.colors", this.colors);
					}else{
						console.log("%c getColors: No colors found", "color:red");
					}
					this.colorsSubject.next("colors");
				}
			);
	}


	startPreloading(){
		this.preloader = true;
	}

	stopPreloading(){
		this.preloader = false;
		console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
		this.phpTimes = [];
		this.phpAllTime = 0;
	}

	

	/*=== ОБРАБОТКА СОБЫТИЙ ===*/
	/**
	 * Смена шага
	 */
	gotoStep(step_id:number){
		this.onStepChanged.emit(step_id);
	}

	/**
	 * Отрисовка карниза
	 */
	drawCornice(cornice:Cornice){
		this.corniceService.getCorniceImg(cornice)
			.subscribe(
				(response:Response) => {
					if(response.ok){
						let imgSrc = response.json().arItems;
						let ar = imgSrc.split(".");
						this.cornice.imgSrc = imgSrc + "?v=" + new Date().getTime();
						this.cornice['imgExt'] = ar[1];
						this.timing('drawCornice', response.json().times);
						console.log("getCorniceImg: this.cornice.imgSrc", cornice.imgSrc);
						this.corniceDrawSubject.next(true);
					}
				}
			);
	}

	/**
	 * Добавление скриптов после загрузки
	 */
	ngAfterViewInit() {
	}

	/*=== ОБРАБОТКА СОБЫТИЙ ===*/

	/**
	 * click on color box handler
	 */
	colorSelected(color_id: number){
		this.startPreloading();
		this.ringFilter.colorId = color_id;
		// this.filter.colorId = this.ringFilter.colorId;
		this.colorsSubject.next("colors");
	}

	/**
	 * click on rings in slider
	 */
	swiperItemSelected(swiperItemId: number){
		this.startPreloading();

		let arFounded = this.swiperItems.filter(function(item) {
			return item.id == swiperItemId;
		});

		this.cornice.rings[0] = arFounded[0];

		this.ringFilter["name"] = this.getRingNameToFilter(this.cornice.rings[0].name);
		
		switch (this.cornice.nRows) {
			case 3:
				// карниз 3-рядный
			case 2:
				// карниз 2-рядный
				this.getRing2byRing1(this.ringFilter);
				break;
			case 1:
				// карниз 1-рядный
				this.corniceSubject.next(this.cornice);
				break;
		}
		
	}

	getRingNameToFilter(str: string){
		str = str.replace(/( упаковка.*)/gi, '');
		str = str.replace(/\d/gi, '%');
		str = str.replace(/%+/gi, '%');
		str += '%';
		// console.log('getRingNameToFilter: str', str);
		return str;
	}

	buy(cornice:Cornice){
		this.gotoStep(6);
	}

	updateImg(imgSrc:string){
		this.startPreloading();
		this.corniceService.updateImg(imgSrc).subscribe(
				(response:Response) => {
					if(response.ok && response.json().arItems){
						this.drawCornice(this.cornice);
					}
				}
			);
	}

	timing(title:string, times:number){
		if(Global.IS_DEBUG){
			this.phpTimes.push({[title]: times});
			this.phpAllTime += times;
		}
	}


	ngOnDestroy() {
		this.ringsSubscription.unsubscribe();
		this.ring2Subscription.unsubscribe();
		this.ring3Subscription.unsubscribe();
		this.corniceSubscription.unsubscribe();
		this.corniceDrawSubscription.unsubscribe();
	}

}