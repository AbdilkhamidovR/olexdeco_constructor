import { Component, EventEmitter, OnInit, Input, Output, ViewChild } from '@angular/core';
import { trigger, state, animate, transition, style} from '@angular/core';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/zip';

import { Global } from '../global'; 
import { Step }	from '../data/step';
import { Cornice }	from '../data/cornice';
import { CorniceItem }	from '../data/cornice-item';
import { LoadItem }	from '../data/load-item';
import { CorniceService }  from '../services/cornice.service';
import { Filter }	from '../data/filter';
import { TubeForm }	from '../data/tube-form';
import { TubeType }	from '../data/tube-type';
import { Color }	from '../data/color';
import { Basket }	from '../data/basket';

import { ItemsSliderComponent }  from '../components/items-slider.component';
import { BasketSliderComponent }  from '../components/basket-slider.component';



@Component({
	selector: 'cornice-parts-tubes',
	animations: [
			trigger('mobileBasketTrigger', [
				state('true' , style({ right: '0' })),
				state('false', style({ right: '-100%' })),
				transition('1 => 0', animate('200ms ease-in-out')),
				transition('0 => 1', animate('200ms ease-in-out'))
			]),
		],
	templateUrl: 'app/templates/parts.template.html' + ((Global.IS_DEBUG) ? '?v=' + Math.random() : '')
})
export class PartsTubesComponent  implements OnInit{

	// данные из app.component
	@Input() private cornice: Cornice;
	@Input() private filter: Filter;
	@Input() private steps: any;
	@Input() private user: object;
	@Input() private isMobileBasketActive: boolean;


	@Output() private onStepChanged = new EventEmitter<number>();
	@Output() private onCorniceChanged = new EventEmitter<Cornice>();

	@ViewChild('itemsSlider')
	private itemsSlider: ItemsSliderComponent;

	@ViewChild('basketSlider')
	private basketSlider: BasketSliderComponent;
	//

	private tubeForms: TubeForm[];
	private tubeTypes: TubeType[]; // типы труб (диаметры) для первого ряда
	private tubeTypes2: TubeType[]; // типы труб (диаметры) для второго ряда
	private colors: Color[];
	private basket: Basket = new Basket(0);
	private basketItemKeys: string[];
	private preloader: boolean;
	private swiperItems: CorniceItem[];
	private swiperItems2: CorniceItem[];
	private corniceItemId: any;
	private corniceItemId2: any;
	private corniceItemColorId: string;
	
	private tubeFormsSubject: Subject<string> = new Subject();
	private tubeFormsSubscription: Subscription;

	private colorsSubject: Subject<string> = new Subject();
	private colorsSubscription: Subscription;

	private tubeTypesSubject: Subject<string> = new Subject();
	private tubeTypesSubscription: Subscription;

	private tubeLengthSubject: Subject<string> = new Subject();
	private tubeLengthSubscription: Subscription;

	private tubeTypes2Subject: Subject<string> = new Subject();
	private tubeTypes2Subscription: Subscription;

	private tubeSubject: Subject<CorniceItem> = new Subject();
	private tubeSubscription: Subscription;

	private capSubject: Subject<CorniceItem> = new Subject();
	private capSubscription: Subscription;

	private mountSubject: Subject<CorniceItem> = new Subject();
	private mountSubscription: Subscription;

	private ringSubject: Subject<CorniceItem> = new Subject();
	private ringSubscription: Subscription;

	private ring2Subject: Subject<CorniceItem> = new Subject();
	private ring2Subscription: Subscription;

	private ring3Subject: Subject<CorniceItem> = new Subject();
	private ring3Subscription: Subscription;

	private tube2Subject: Subject<CorniceItem> = new Subject();
	private tube2Subscription: Subscription;

	private tube3Subject: Subject<CorniceItem> = new Subject();
	private tube3Subscription: Subscription;

	private cap2Subject: Subject<CorniceItem> = new Subject();
	private cap2Subscription: Subscription;

	private cap3Subject: Subject<CorniceItem> = new Subject();
	private cap3Subscription: Subscription;

	private corniceSubject: Subject<Cornice> = new Subject();
	private corniceSubscription: Subscription;

	private corniceDrawSubject: Subject<boolean> = new Subject();
	private corniceDrawSubscription: Subscription;

	private phpTimes: object[] = [];
	private phpAllTime: number = 0;

	private loadItems:any = {
		tubes:[
			new LoadItem("tubes", 0, "tube", "getTube", this.tubeSubject, this.tubeSubscription),
			new LoadItem("tubes", 1, "tube2", "getTube2", this.tube2Subject, this.tube2Subscription),
			new LoadItem("tubes", 2, "tube3", "getTube3", this.tube3Subject, this.tube3Subscription),
		],
		caps:[
			new LoadItem("caps", 0, "cap", "getCap", this.capSubject, this.capSubscription),
			new LoadItem("caps", 1, "cap2", "getCap2", this.cap2Subject, this.cap2Subscription),
			new LoadItem("caps", 2, "cap3", "getCap3", this.cap3Subject, this.cap3Subscription),
		],
		mounts:[
			new LoadItem("mounts", 0, "mount", "getMount", this.mountSubject, this.mountSubscription),
		],
		rings:[
			new LoadItem("rings", 0, "ring", "getRing", this.ringSubject, this.ringSubscription),
			new LoadItem("rings", 1, "ring2", "getRing2", this.ring2Subject, this.ring2Subscription),
			new LoadItem("rings", 2, "ring3", "getRing3", this.ring3Subject, this.ring3Subscription),
		]
	};


	constructor(
		private corniceService: CorniceService,
	) {
	};

		
	/*=========*/
	ngOnInit(): void {
		this.startPreloading();
		this.initStep();
	}

	// инициализация шага 1
	initStep(){
		this.getTubeForms(this.filter);
		this.initSubsciptions();
		console.log('user', this.user);
	}

	initSubsciptions(){
		// Отслеживание загрузки форм труб
		this.tubeFormsSubscription = this.tubeFormsSubject
			.subscribe( 
				(x) => { 
					console.log('%c Tube forms loaded! tubeFormsSubject onNext: ' + x, 'color:green'); 
					this.getTubeTypes(this.filter);
				},
				(e) => { console.log('tubeFormsSubject onError: ' + e.message); },
				() => { console.log('%c tubeFormsSubject complete', 'color:green'); }
			);

		
		// Отслеживание загрузки типов труб (диаметров и др.)
		this.tubeTypesSubscription = this.tubeTypesSubject
			.subscribe( 
				(x) => { 
					console.log('%c Tube types loaded! tubeTypesSubject onNext: ' + x, 'color:green'); 
					this.getColors(this.filter);
				},
				(e) => { console.log('tubeTypesSubject onError: ' + e.message); },
				() => { console.log('%c tubeTypesSubject complete', 'color:green'); }
			);
		
		// Отслеживание загрузки типов труб (диаметров и др.) 2-го ряда
		this.tubeTypes2Subscription = this.tubeTypes2Subject
			.subscribe( 
				(x) => { 
					console.log('%c Tube types for 2 row loaded! tubeTypes2Subject onNext: ' + x, 'color:green'); 
					// this.getColors(this.filter);
				},
				(e) => { console.log('tubeTypesSubject onError: ' + e.message); },
				() => { console.log('%c tubeTypesSubject complete', 'color:green'); }
			);

		
		// Отслеживание загрузки цветов
		this.colorsSubscription = this.colorsSubject
			.subscribe( 
				(x) => { 
					console.log('%c Colors loaded! olorsSubject onNext: ' + x, 'color:green'); 
					this.getTubeLength(this.filter);
					// this.getCorniceItem(this.loadItems.tubes[0], this.filter);
				},
				(e) => { console.log('colorsSubject onError: ' + e.message); },
				() => { console.log('%c colorsSubject complete', 'color:green'); }
			);
		
		// Отслеживание загрузки длины трубы
		this.tubeLengthSubscription = this.tubeLengthSubject
			.subscribe( 
				(x) => { 
					console.log('%c tubeLength loaded! tubeLength onNext: ' + x, 'color:green'); 
					this.getCorniceItem(this.loadItems.tubes[0], this.filter);
				},
				(e) => { console.log('colorsSubject onError: ' + e.message); },
				() => { console.log('%c colorsSubject complete', 'color:green'); }
			);


		// Отслеживание загрузки трубы
		this.tubeSubscription = this.tubeSubject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c tubeSubject onNext: ', 'color:green', item ); 
					this.cornice.tubes[0] = item;
					this.filter.tubeLength = item.tubeLength;
					this.getCorniceItem(this.loadItems.caps[0], this.filter);
					this.getCorniceItem(this.loadItems.mounts[0], this.filter);
				},
				(e) => { console.log('tubeSubject onError: ' + e.message); },
				() => { console.log('%c tubeSubject complete', 'color:green'); }
			);


		// Отслеживание загрузки наконечника
		this.capSubscription = this.capSubject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c capSubject onNext: ', 'color:green', item); 
					this.cornice.caps[0] = item;
				},
				(e) => { console.log('capSubject onError: ' + e.message); },
				() => { console.log('%c capSubject complete', 'color:green'); }
			);

		// Отслеживание загрузки кронштейна
		this.mountSubscription = this.mountSubject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c mountSubject onNext: ', 'color:green', item); 
					this.cornice.mounts[0] = item;

					for (let i in item.tubeTypeIds) {
						if(this.corniceService.contains(item.tubeTypeIds[i], this.cornice.tubes[0].tubeTypeId)){
							this.filter.tubeTypeIds[i] = this.cornice.tubes[0].tubeTypeId;
						}else{
							this.filter.tubeTypeIds[i] = item.tubeTypeIds[i][0];
						}
					}

					// this.filter.tubeTypeIds = item.tubeTypeIds;
					let params = null;
					if(this.filter.tubeFormIds[0] == Global.PROFILE){
						params = {profileId: this.cornice.tubes[0].productId}
					}
					switch (this.cornice.nRows) {
						case 1:
							// карниз 1-рядный
							this.getCorniceItem(this.loadItems.rings[0], this.filter, params);
							break;
						case 2:
							// карниз 2-рядный
							this.getTubeTypes(this.filter, {"nRow": 1});
						case 3:
							// карниз 3-рядный
							this.getCorniceItem(this.loadItems.rings[0], this.filter, params);
							this.getCorniceItem(this.loadItems.tubes[1], this.filter);
							this.getCorniceItem(this.loadItems.caps[1], this.filter);

							break;
					}
				},
				(e) => { console.log('mountSubject onError: ' + e.message); },
				() => { console.log('%c mountSubject complete', 'color:green'); }
			);

		// Отслеживание загрузки наконечника
		this.ringSubscription = this.ringSubject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c ringSubject onNext: ', 'color:green', item); 
					if(item){
						this.cornice.rings[0] = item;
					}else{
						delete this.cornice.rings[0];
					}
					if(this.cornice.nRows == 2 || this.cornice.nRows == 3){
						this.getRing2(this.filter);
					}
				},
				(e) => { console.log('ringSubject onError: ' + e.message); },
				() => { console.log('%c ringSubject complete', 'color:green'); }
			);

		// Отслеживание загрузки трубы второго ряда
		this.tube2Subscription = this.tube2Subject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c tube2Subject onNext: ', 'color:green', item); 
					this.cornice.tubes[1] = item;
					if(this.cornice.nRows == 3){
						this.getTube3(this.filter);
					}
				},
				(e) => { console.log('tube2Subject onError: ' + e.message); },
				() => { console.log('%c tube2Subject complete', 'color:green'); }
			);
		// Отслеживание загрузки трубы третьего ряда
		this.tube3Subscription = this.tube3Subject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c tube3Subject onNext: ' , 'color:green', item); 
					this.cornice.tubes[2] = item;
				},
				(e) => { console.log('tube3Subject onError: ' + e.message); },
				() => { console.log('%c tube3Subject complete', 'color:green'); }
			);


		// Отслеживание загрузки наконечника 2-го ряда
		this.cap2Subscription = this.cap2Subject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c cap2Subject onNext: ', 'color:green',  item); 
					this.cornice.caps[1] = item;
					if(this.cornice.nRows == 3){
						this.getCap3(this.filter);
					}
				},
				(e) => { console.log('cap2Subject onError: ' + e.message); },
				() => { console.log('%c cap2Subject complete', 'color:green'); }
			);
		// Отслеживание загрузки наконечника 3-го ряда
		this.cap3Subscription = this.cap3Subject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c cap3Subject onNext: ', 'color:green', item); 
					this.cornice.caps[2] = item;
				},
				(e) => { console.log('Cap3Subject onError: ' + e.message); },
				() => { console.log('%c cap3Subject complete', 'color:green'); }
			);


		// Отслеживание загрузки кольца 2-го ряда
		this.ring2Subscription = this.ring2Subject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c ring2Subject onNext: ', 'color:green', item); 
					if(item){
						this.cornice.rings[1] = item;
					}else{
						delete this.cornice.rings[1];
					}

					if(this.cornice.nRows == 3){
						this.getRing3(this.filter);
					}
				},
				(e) => { console.log('ring2Subject onError: ' + e.message); },
				() => { console.log('%c ring2Subject complete', 'color:green'); }
			);
		// Отслеживание загрузки кольца 3-го ряда
		this.ring3Subscription = this.ring3Subject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c ring3Subject onNext: ', 'color:green', item);
					if(item){
						this.cornice.rings[2] = item;
					}else{
						delete this.cornice.rings[2];
					}
				},
				(e) => { console.log('ring3Subject onError: ' + e.message); },
				() => { console.log('%c ring3Subject complete', 'color:green'); }
			);


		//======//
		// Параллельная загрузка доп. рядов
		switch (this.cornice.nRows) {
			case 1:
				// карниз 1-рядный
				Observable.zip(this.capSubject, this.ringSubject, this.mountSubject)
					.subscribe(items => {
						console.log('%c nRows: 1. Observable.zip onNext: ', 'color:green', items);
				   	this.corniceSubject.next(this.cornice);
				});
				break;
			case 2:
				// карниз 2-рядный
				Observable.zip(this.capSubject, this.ringSubject, this.tube2Subject, this.cap2Subject, this.ring2Subject)
					.subscribe(items => {
						console.log('%c nRows: 2. Observable.zip onNext: ' , 'color:green', items);
				   	this.corniceSubject.next(this.cornice);
				});
				break;
			case 3:
				// карниз 3-рядный
				// грузим параллельно
				Observable.zip(this.capSubject, this.ringSubject, this.tube2Subject, this.cap2Subject,  this.tube3Subject, this.cap3Subject, this.ring2Subject, this.ring3Subject)
					.subscribe(items => {
						console.log('%c nRows: 3. Observable.zip onNext: ', 'color:green', items);
				   	this.corniceSubject.next(this.cornice);
				});
				break;
		}


		/* ===== */
		// Отслеживание изменения параметров карниза
		this.corniceSubscription = this.corniceSubject
			.subscribe( 
				(cornice:Cornice) => { 
					console.log('%c Cornice changed! corniceSubject onNext:', 'color:orange', this.cornice); 

					// установка кол-ва для корзины
					this.cornice.mounts[0].length = this.corniceService.getMountLength(this.cornice.tubes[0].tubeLength);

					for (let i in this.cornice.rings) {
						this.cornice.rings[i].length = this.corniceService.getBasketRingsInRowLength(this.cornice);
					}

					this.onCorniceChanged.emit(this.cornice);
					this.basket = this.corniceService.makeBasket(this.cornice);
					this.basketSlider.slidesLoaded();
					this.drawCornice(this.cornice);
					this.corniceItemId = this.filter.tubeTypeIds[0];
					this.corniceItemId2 = this.filter.tubeTypeIds[1];
					this.corniceItemColorId = this.cornice.tubes[0].colorId;

					if(!this.cornice.rings[0]){
						this.steps[4].setDisable();
					}else{
						this.steps[4].setEnable();
					}
				},
				(e) => { console.log('corniceSubject onError: ' + e.message); },
				() => { console.log('%c corniceSubject complete', 'color:green'); }
			);

		
		// Отслеживание рендеринга карниза
		this.corniceDrawSubscription = this.corniceDrawSubject
			.subscribe( 
				(x) => { 
					console.log('%c Cornice drawed! corniceDrawSubject onNext: ' + x, 'color:green');
					this.stopPreloading();
				},
				(e) => { console.log('corniceDrawSubject onError: ' + e.message); },
				() => { console.log('%c corniceDrawSubject complete', 'color:green'); }
			);
	}

	getTubeForms(filter:Filter){
		this.corniceService.getTubeForms(filter).subscribe(
				(response:Response) => {
					if(response.ok){
						this.tubeForms = response.json().arItems;
						this.timing('getTubeForms', response.json().times);

						console.log("initTubes: this.tubeForms", this.tubeForms, response.json().times);

						if(!this.filter.tubeFormIds[0]){
							this.filter.tubeFormIds[0] = this.tubeForms[0].id;
						}

						this.tubeFormsSubject.next("tubeForms");
					}
				}
			);
	}

	getTubeTypes(filter:Filter, params: object = null){
		let nRow = 0;
		if(params && params.hasOwnProperty('nRow')){
			nRow = params["nRow"];
		}
		this.corniceService.getTubeTypes(filter, params).subscribe(
				(response:Response) => {
					if(response.ok){
						switch (nRow) {
							case 0:
								// получаем типы труб (диаметры и др.) для 1-го ряда
								this.tubeTypes = response.json().arItems;
								this.swiperItems = this.tubeTypes;
								this.timing('getTubeTypes', response.json().times);
								console.log("initTubeTypes: this.TubeTypes", this.tubeTypes);

								if(this.tubeTypes.length){
									if(!this.filter.tubeTypeIds[0]){
										// инициализация
										this.filter.tubeTypeIds[0] = this.tubeTypes[0].id;
									}else{
										let isTubeTypeFound = false;
										for (let tubeType of this.tubeTypes) {
											if(tubeType.id == this.filter.tubeTypeIds[0]){
												isTubeTypeFound = true;
												break;
											}
										}
										if(!isTubeTypeFound){
											console.log("%c getTubeTypes: No tubeType selected", "color:red");
											this.filter.tubeTypeIds[0] = this.tubeTypes[0].id;
										}
									}
									this.tubeTypesSubject.next("tubeTypes");
									this.itemsSlider.slidesLoaded();
								}else{
									console.log("%c getTubeTypes: No tubeType found", "color:red");
									this.reloadStep();
								}
								break;

							case 1:
								// получаем типы труб (диаметры и др.) для 2-го ряда
								this.tubeTypes2 = response.json().arItems;
								this.swiperItems2 = this.tubeTypes2;

								let isTubeType2Found = false;
								for (let tubeType2 of this.tubeTypes2) {
									if(tubeType2.id == this.filter.tubeTypeIds[1]){
										isTubeType2Found = true;
										break;
									}
								}

								// console.log("getTubeTypes: TubeTypes2 this.filter", this.filter);
								console.log("getTubeTypes: this.TubeTypes2", this.tubeTypes2);
								if(!isTubeType2Found){
									console.log("%c getTubeTypes: No tubeType2 founded", "color:red");
									this.filter.tubeTypeIds[1] = this.tubeTypes2[0].id;
								}

								this.tubeTypes2Subject.next("tubeTypes2");
								if(this.swiperItems2.length > 1){
									this.itemsSlider.slidesLoaded();
								}
								break;
							
							default:
								// code...
								break;
						}
					}	
				}
			);
	}

	getColors(filter:Filter){
		this.corniceService.getColors(filter, "getTubeColors")
			.subscribe(
				(response:Response) => {
					if(response.ok){
						this.colors = response.json().arItems;
						this.timing('getColors', response.json().times);

						if(this.colors.length){
							if(!this.filter.colorId){
								this.filter.colorId = this.colors[0].id;
							}else{
								let isColorFound = false;
								for (let color of this.colors) {
									if(color.id == this.filter.colorId){
										isColorFound = true;
										break;
									}
								}
								if(!isColorFound){
									console.log("%c getColors: No colors selected", "color:red");
									this.filter.colorId = this.colors[0].id;
								}
							}

							this.colorsSubject.next("colors");
						}else{
							console.log("%c getColors: No colors found", "color:red");
							this.filter.colorId = 0;
							this.tubeTypesSubject.next("tubeTypes");
						}
					}
				}
			);
	}

	getTubeLength(filter:Filter){
		this.corniceService.getTubeLength(filter).subscribe(
				(response:Response) => {

					console.log('getTubeLength response', response);
					if(response.ok){
						this.timing('getTubeLength', response.json().times);

						console.log("getTubeLength: tubeLengths", response.json().arItems, response.json().times);

						if(response.json().arItems.length){
							this.filter.tubeLength = response.json().arItems[0].value;
						}

					}
					this.tubeLengthSubject.next("tubeLengths");
				}
			);

	}


	getCorniceItem(loadItem:LoadItem, filter:Filter, params: object = null){

		// alert('getCorniceItem ('+loadItem.func+'):' + filter.tubeTypeIds[1]);

		this.corniceService.getCorniceItem(loadItem.func, filter, params)
			.subscribe(
				(response:Response) => {

					let corniceItem:CorniceItem;
					if(response.ok && response.json().arItems){
						// use new cornice item
						corniceItem = response.json().arItems;
						this.timing(loadItem.func, response.json().times);
					}else{
						console.log("%c getCorniceItem (%s): %s not found! Use previous value.", "color:red", loadItem.func, loadItem.name, this.cornice);
						// use setted cornice item in this.cornice
						corniceItem = this.cornice[loadItem.corniceKey][loadItem.corniceIndx];

						// exlude mount for different number of rows
						if(loadItem.name == "mount" && this.cornice.mounts[0].tubeTypeIds.length != this.cornice.nRows){
							corniceItem = undefined;
						}
						// exlude rings for profiles
						if(filter.tubeFormIds[0] == Global.PROFILE && loadItem.corniceKey == "rings"){
							corniceItem = undefined;
						}
					}

					if(corniceItem){
						loadItem.subject.next(corniceItem);
					}else{
						console.log("%c %s: reset filter and reload", "color:red", loadItem.func);
						if(filter.tubeFormIds[0] == Global.PROFILE && loadItem.corniceKey == "rings"){
							loadItem.subject.next(corniceItem);
						}else{
							this.reloadStep();
						}
					}
				}
			);
	}


	getTube2(filter:Filter){
		if(this.cornice.mounts[0] && this.cornice.tubes[0] && 
			this.corniceService.contains(this.cornice.mounts[0].tubeTypeIds[1], this.cornice.tubes[0].tubeTypeId))
		{
			this.cornice.tubes[1] = this.cornice.tubes[0];
			this.tube2Subject.next(this.cornice.tubes[1]);
		}else{
			this.getCorniceItem(this.loadItems.tubes[1], filter);
		}
	}
	getRing2(filter:Filter){
		if(this.cornice.mounts[0] && this.cornice.rings[0] && 
			this.corniceService.contains(this.cornice.mounts[0].tubeTypeIds[1], this.cornice.tubes[0].tubeTypeId))
		{
			this.cornice.rings[1] = this.cornice.rings[0];
			this.ring2Subject.next(this.cornice.rings[1]);
		}else{
			this.getCorniceItem(this.loadItems.rings[1], filter);
		}
	}

	getTube3(filter:Filter){
		if(this.cornice.mounts[0] && this.cornice.tubes[0] && 
			this.corniceService.contains(this.cornice.mounts[0].tubeTypeIds[2], this.cornice.tubes[0].tubeTypeId))
		{
			this.cornice.tubes[2] = this.cornice.tubes[0];
			this.tube3Subject.next(this.cornice.tubes[2]);
		}else{
			this.getCorniceItem(this.loadItems.tubes[2], filter);
		}
	};

	getCap3(filter:Filter){

		if( this.cornice.mounts[0] && this.cornice.caps[1] && 
			this.corniceService.isArraysEquals(this.cornice.mounts[0].tubeTypeIds[1], this.cornice.mounts[0].tubeTypeIds[2]) )
		{
			this.cornice.caps[2] = this.cornice.caps[1];
			this.cap3Subject.next(this.cornice.caps[2]);
		}else{
			this.getCorniceItem(this.loadItems.caps[2], filter);
		}
	};

	getRing3(filter:Filter){
		if(this.cornice.mounts[0] && this.cornice.rings[0] && this.cornice.tubes[0] && 
			this.corniceService.contains(this.cornice.mounts[0].tubeTypeIds[2], this.cornice.tubes[0].tubeTypeId))
		{
			this.cornice.rings[2] = this.cornice.rings[0];
			this.ring3Subject.next(this.cornice.rings[2]);
		}else{
			this.getCorniceItem(this.loadItems.rings[2], filter);
		}
	};


	// ******* //
	drawCornice(cornice:Cornice){
		this.corniceService.getCorniceImg(cornice)
			.subscribe(
				(response:Response) => {
					// console.log("getCorniceImg: response", response);
					
					if(response.ok){
						let imgSrc = response.json().arItems;
						let ar = imgSrc.split(".");
						this.cornice.imgSrc = imgSrc + "?v=" + new Date().getTime();
						this.cornice['imgExt'] = ar[1];
						this.timing('drawCornice', response.json().times);
						console.log("getCorniceImg: this.cornice.imgSrc", this.cornice.imgSrc);
						this.corniceDrawSubject.next(true);
					}
				}
			);
	}



	/*=== ОБРАБОТКА СОБЫТИЙ ===*/
	/**
	 * Смена шага
	 */
	gotoStep(step_id:number){
		this.onStepChanged.emit(step_id);
	}

	printImg(imgSrc:string){
		this.corniceService.printImg(imgSrc);
	}

	/**
	 * Добавление скриптов после загрузки
	 */
	ngAfterViewInit() {
	}

	// загрузка скрипта для запуска слайдеров
	// loadMainJS(){
	// 	var el = document.getElementById('main-js');
	// 	if(!el){
	// 		var s = document.createElement("script");
	// 		s.type = "text/javascript";
	// 		s.id = "main-js";
	// 		s.src = "./assets/js/main.js?v=1.3";
	// 		this.elementRef.nativeElement.appendChild(s);
	// 	}
	// }


	/**
	 * click on tube type handler
	 */
	tubeFormSelected(tubeForm_id: number){
		this.startPreloading();
		this.cornice.resetLeftSide(); // очистка настроек карниза слева на 6-м шаге
		this.cornice.resetRightSide(); // очистка настроек карниза справа на 6-м шаге
		this.filter.tubeFormIds[0] = tubeForm_id;
		this.tubeFormsSubject.next("tubeForms");
	}


	/**
	 * click on color box handler
	 */
	colorSelected(color_id: number){
		this.startPreloading();
		this.cornice.resetLeftSide(); // очистка настроек карниза слева на 6-м шаге
		this.cornice.resetRightSide(); // очистка настроек карниза справа на 6-м шаге
		this.filter.colorId = color_id;
		this.colorsSubject.next("colors");
	}

	/**
	 * click on tube type handler
	 */
	swiperItemSelected(tubeType_id: any){
		console.log('swiperItemSelected tubeType_id', tubeType_id);
		this.startPreloading();
		this.cornice.resetLeftSide(); // очистка настроек карниза слева на 6-м шаге
		this.cornice.resetRightSide(); // очистка настроек карниза справа на 6-м шаге
		this.filter.tubeTypeIds[0] = tubeType_id;
		this.tubeTypesSubject.next("tubeTypes");
		// this.getCorniceItem(this.loadItems.tubes[0], this.filter);
	}
	/**
	 * click on tube type for 2 row handler
	 */
	swiperItemSelected2(tubeType_id: any){
		this.startPreloading();
		this.cornice.resetLeftSide(); // очистка настроек карниза слева на 6-м шаге
		this.cornice.resetRightSide(); // очистка настроек карниза справа на 6-м шаге
		this.filter.tubeTypeIds[1] = tubeType_id;

		this.getCorniceItem(this.loadItems.mounts[0], this.filter, {"nRow":1});
		this.capSubject.next(this.cornice.caps[0]);
	}

	resetFilter(){
		this.filter.tubeFormIds = [0,0,0];
		this.filter.tubeTypeIds = [0,0,0];
		this.filter.colorId = 0;
	}

	reloadStep(){
		this.ngOnDestroy();
		this.resetFilter();
		this.initStep();
	}

	buy(cornice:Cornice){
		this.gotoStep(6);
	}

	updateImg(imgSrc:string){
		this.startPreloading();
		this.corniceService.updateImg(imgSrc).subscribe(
				(response:Response) => {
					if(response.ok && response.json().arItems){
						this.drawCornice(this.cornice);
					}
				}
			);
	}

	startPreloading(){
		this.preloader = true;
	}

	stopPreloading(){
		this.preloader = false;
		console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
		this.phpTimes = [];
		this.phpAllTime = 0;
	}

	timing(title:string, times:number){
		if(Global.IS_DEBUG){
			this.phpTimes.push({[title]: times});
			this.phpAllTime += times;
		}
	}

	mobBasketlinkClicked(){
		this.isMobileBasketActive = !this.isMobileBasketActive;
		console.log('mobBasketlinkClicked', this.isMobileBasketActive);
	}

	ngOnDestroy() {
		this.tubeFormsSubscription.unsubscribe();
		this.colorsSubscription.unsubscribe();
		this.tubeTypesSubscription.unsubscribe();
		this.tubeSubscription.unsubscribe();
		this.capSubscription.unsubscribe();
		this.mountSubscription.unsubscribe();
		this.tube2Subscription.unsubscribe();
		this.tube3Subscription.unsubscribe();
		this.cap2Subscription.unsubscribe();
		this.cap3Subscription.unsubscribe();
		this.ringSubscription.unsubscribe();
		this.ring2Subscription.unsubscribe();
		this.ring3Subscription.unsubscribe();
		this.corniceDrawSubscription.unsubscribe();
	}

}