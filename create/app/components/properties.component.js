"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/zip");
var global_1 = require("../global");
var cornice_service_1 = require("../services/cornice.service");
var cornice_1 = require("../data/cornice");
var filter_1 = require("../data/filter");
var basket_1 = require("../data/basket");
var items_slider_component_1 = require("../components/items-slider.component");
var basket_slider_component_1 = require("../components/basket-slider.component");
var PropertiesComponent = (function () {
    function PropertiesComponent(corniceService) {
        this.corniceService = corniceService;
        this.onStepChanged = new core_1.EventEmitter();
        this.phpTimes = [];
        this.phpAllTime = 0;
        this.tubeLengthsSubject = new Subject_1.Subject();
        this.tubeSubject = new Subject_1.Subject();
        this.tsNextEventsLength = 0;
        this.corniceSubject = new Subject_1.Subject();
        this.connectorsSubject = new Subject_1.Subject();
        //
        this.picturePath = "./assets/images/segments3/";
        this.pictureExt = ".png";
        this.isLeftSideVisible = false;
        this.isRightSideVisible = false;
        this.mountsLength = 0;
        this.ringsLength = 0;
        this.ringsTitle = "Кольца";
        this.ringByMeter = global_1.Global.RINGS_BY_METER;
    }
    ;
    PropertiesComponent.prototype.ngOnInit = function () {
        console.clear();
        this.startPreloading();
        this.initStep();
        this.initSubscriptions();
    };
    PropertiesComponent.prototype.initStep = function () {
        this.propsFilter = new filter_1.Filter(this.filter.nRowsId, this.filter.tubeFormIds, this.filter.tubeTypeIds, this.filter.tubeLength, this.filter.colorId, 0);
        this.basket = this.corniceService.makeBasket(this.cornice);
        // console.log('basket', this.basket);
        this.getTubeLengthsByTubes(this.cornice.tubes);
        this.getConnectors(this.cornice.tubes);
    };
    PropertiesComponent.prototype.initSubscriptions = function () {
        var _this = this;
        // Отслеживание загрузки длин труб
        this.tubeLengthsSubscription = this.tubeLengthsSubject
            .subscribe(function (tubeLengths) {
            console.log('%c tubeLengths loaded! tubeLengths onNext: ', 'color:green', tubeLengths);
            // this.corniceSubject.next(this.cornice);
        }, function (e) { console.log('tubeLengthsSubject onError: ' + e.message); }, function () { console.log('%c tubeLengthsSubject complete', 'color:green'); });
        // Отслеживание загрузки соединителей
        this.connectorsSubscription = this.connectorsSubject
            .subscribe(function (connectors) {
            console.log('%c Сonnectors loaded! сonnectors:', 'color:green', connectors);
            console.log('%c connectorExists:', 'color:green', _this.connectorExists);
        }, function (e) { console.log('tubeSubject onError: ' + e.message); }, function () { console.log('%c tubeSubject complete', 'color:green'); });
        // Отслеживание загрузки трубы
        this.tubeSubscription = this.tubeSubject
            .subscribe(function (item) {
            console.log('%c tubeSubject onNext: ', 'color:green', item);
            _this.tsNextEventsLength++;
            if (_this.tsNextEventsLength == _this.cornice.tubes.length) {
                _this.tsNextEventsLength = 0;
                _this.corniceSubject.next(_this.cornice);
            }
        }, function (e) { console.log('tubeSubject onError: ' + e.message); }, function () { console.log('%c tubeSubject complete', 'color:green'); });
        // Отслеживание окончания загрузки соединителей и длин труб
        Observable_1.Observable.zip.apply(null, [this.tubeLengthsSubject, this.connectorsSubject])
            .subscribe(function (x) {
            console.log('%c Observable.zip onNext: ', 'color:green', x);
            _this.corniceSubject.next(_this.cornice);
        });
        /* ===== */
        // Отслеживание изменения параметров карниза
        this.corniceSubscription = this.corniceSubject
            .subscribe(function (cornice) {
            console.log('%c Cornice changed! corniceSubject onNext:', 'color:orange', _this.cornice);
            // выбор картинок схем   
            var centerType = "";
            if (_this.cornice.sides.left.tubeLength > 0) {
                _this.isLeftSideVisible = true;
                var leftMountLength = _this.corniceService.getMountLength(_this.cornice.sides.left.tubeLength);
                if (_this.cornice.sides.left.type == "direct") {
                    leftMountLength--;
                }
                _this.cornice.sides.left.imgSrc = _this.getPictureSrc("left", _this.cornice.sides.left.type, _this.cornice.nRows, leftMountLength);
                centerType = "left" + _this.cornice.sides.left.type;
            }
            if (_this.cornice.sides.right.tubeLength > 0) {
                _this.isRightSideVisible = true;
                centerType += "right" + _this.cornice.sides.right.type;
                var rightMountLength = _this.corniceService.getMountLength(_this.cornice.sides.right.tubeLength);
                if (_this.cornice.sides.right.type == "direct") {
                    rightMountLength--;
                }
                _this.cornice.sides.right.imgSrc = _this.getPictureSrc("right", _this.cornice.sides.right.type, _this.cornice.nRows, rightMountLength);
            }
            // обновление кол-ва наконечников и колец
            _this.cornice.mounts[0].length = _this.corniceService.getMountLength(_this.cornice.tubes[0].tubeLength);
            switch (_this.cornice.rings[0].ringType) {
                case "RING":
                    // code...
                    _this.ringsTitle = "Кольца";
                    _this.ringsPackQuantity = global_1.Global.RINGS_IN_PACK;
                    break;
                case "RUNNER":
                    // code...
                    _this.ringsTitle = "Бегунки";
                    _this.ringsPackQuantity = global_1.Global.RUNNERS_IN_PACK;
                    break;
                default:
                    // code...
                    break;
            }
            for (var i in _this.cornice.rings) {
                _this.cornice.rings[i].length = _this.corniceService.getBasketRingsInRowLength(_this.cornice);
            }
            _this.cornice.schemeImgSrc = _this.getPictureSrc("center", centerType, _this.cornice.nRows, _this.cornice.mounts[0].length);
            // апдэйт корзины
            _this.basket = _this.corniceService.makeBasket(_this.cornice);
            console.log('Cornice changed! corniceSubject basket:', _this.basket);
            _this.stopPreloading();
        }, function (e) { console.log('corniceSubject onError: ' + e.message); }, function () { console.log('%c corniceSubject complete', 'color:green'); });
    };
    PropertiesComponent.prototype.getTubeLengthsByTubes = function (tubes) {
        var _this = this;
        this.corniceService.getTubeLengthsByTubes(tubes)
            .subscribe(function (response) {
            if (response.ok && response.json().arItems) {
                _this.tubeLengths = response.json().arItems;
                _this.timing('tubeLengths', response.json().times);
                // console.log("%c getTubeLengths (%s): ", "color:green", "tubeLengths", this.tubeLengths);
                _this.tubeLengthsSubject.next(_this.tubeLengths);
            }
            else {
                console.log("%c getTubeLengths: not found!", "color:red");
            }
        });
    };
    PropertiesComponent.prototype.getTubeExt = function (filter, nRow, side) {
        var _this = this;
        if (nRow === void 0) { nRow = 0; }
        var params = { nRow: nRow };
        this.corniceService.getCorniceItem("getTubeExt", filter, params)
            .subscribe(function (response) {
            var corniceItem;
            if (response.ok && response.json().arItems) {
                // use new cornice item
                corniceItem = response.json().arItems;
                _this.timing('getTubeExt', response.json().times);
                switch (side) {
                    case "center":
                        _this.cornice.tubes[nRow] = corniceItem;
                        _this.filter.tubeLength = corniceItem.tubeLength;
                        break;
                    case "left":
                        _this.cornice.sides.left.tubes[nRow] = corniceItem;
                        break;
                    case "right":
                        _this.cornice.sides.right.tubes[nRow] = corniceItem;
                        break;
                    default:
                        break;
                }
                _this.tubeSubject.next(corniceItem);
            }
            else {
                corniceItem = undefined;
                console.log("%c getTube: Tube not found!", "color:red");
            }
        });
    };
    PropertiesComponent.prototype.getConnectors = function (tubes) {
        var _this = this;
        this.corniceService.getConnectors(tubes)
            .subscribe(function (response) {
            // console.log("%c getConnectors: response.json", "color:blue", response.json());
            if (response.ok && response.json().arItems) {
                // use new cornice item
                _this.connectors = response.json().arItems.connectors;
                _this.connectorExists = response.json().arItems.exists;
                _this.timing('getConnectors', response.json().times);
                _this.connectorsSubject.next(_this.connectors);
            }
            else {
                console.log("%c getConnectors: Connectors not found for all rows!", "color:red");
            }
        });
    };
    PropertiesComponent.prototype.startPreloading = function () {
        this.preloader = true;
    };
    PropertiesComponent.prototype.stopPreloading = function () {
        this.preloader = false;
        console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
        this.phpTimes = [];
        this.phpAllTime = 0;
    };
    /*=== ОБРАБОТКА ПОЛЬЗОВАТЕЛЬСКИХ СОБЫТИЙ ===*/
    /**
     * click on tube length
     */
    PropertiesComponent.prototype.tubeLengthSelected = function (value) {
        this.startPreloading();
        this.propsFilter.tubeLength = Number(value);
        for (var nRow in this.cornice.tubes) {
            this.propsFilter.productId = this.cornice.tubes[nRow].productId;
            this.getTubeExt(this.propsFilter, nRow, 'center');
        }
    };
    PropertiesComponent.prototype.leftSegmentAdded = function (type) {
        this.startPreloading();
        this.cornice.sides.left.type = type;
        this.cornice.sides.left.tubeLength = this.cornice.tubes[0].tubeLength;
        this.cornice.sides.left.tubes = this.corniceService.copyObj(this.cornice.tubes);
        this.cornice.sides.left.connectors = this.connectors[type];
        this.corniceSubject.next(this.cornice);
    };
    PropertiesComponent.prototype.rightSegmentAdded = function (type) {
        this.startPreloading();
        this.cornice.sides.right.type = type;
        this.cornice.sides.right.tubeLength = this.cornice.tubes[0].tubeLength;
        this.cornice.sides.right.tubes = this.corniceService.copyObj(this.cornice.tubes);
        this.cornice.sides.right.connectors = this.connectors[type];
        this.corniceSubject.next(this.cornice);
    };
    PropertiesComponent.prototype.leftSegmentTubeLengthSelected = function (value) {
        this.startPreloading();
        this.propsFilter.tubeLength = Number(value);
        this.cornice.sides.left.tubeLength = Number(value);
        for (var nRow in this.cornice.tubes) {
            this.propsFilter.productId = this.cornice.tubes[nRow].productId;
            this.getTubeExt(this.propsFilter, nRow, 'left');
        }
    };
    PropertiesComponent.prototype.rightSegmentTubeLengthSelected = function (value) {
        this.startPreloading();
        this.propsFilter.tubeLength = Number(value);
        this.cornice.sides.right.tubeLength = Number(value);
        for (var nRow in this.cornice.tubes) {
            this.propsFilter.productId = this.cornice.tubes[nRow].productId;
            this.getTubeExt(this.propsFilter, nRow, 'right');
        }
    };
    PropertiesComponent.prototype.setLeftSegmentType = function (type) {
        this.cornice.sides.left.type = type;
        this.cornice.sides.left.connectors = this.connectors[type];
        if (!this.cornice.sides.left.tubeLength) {
            this.cornice.sides.left.tubeLength = this.cornice.tubes[0].tubeLength;
            this.cornice.sides.left.tubes = this.corniceService.copyObj(this.cornice.tubes); // передача по ссылке!
        }
        this.corniceSubject.next(this.cornice);
    };
    PropertiesComponent.prototype.setRightSegmentType = function (type) {
        this.cornice.sides.right.type = type;
        this.cornice.sides.right.connectors = this.connectors[type];
        if (!this.cornice.sides.right.tubeLength) {
            this.cornice.sides.right.tubeLength = this.cornice.tubes[0].tubeLength;
            this.cornice.sides.right.tubes = this.corniceService.copyObj(this.cornice.tubes); // передача по ссылке!
        }
        this.corniceSubject.next(this.cornice);
    };
    PropertiesComponent.prototype.leftSegmentDelete = function () {
        this.cornice.resetLeftSide();
        this.isLeftSideVisible = false;
        this.corniceSubject.next(this.cornice);
    };
    PropertiesComponent.prototype.rightSegmentDelete = function () {
        this.cornice.resetRightSide();
        this.isRightSideVisible = false;
        this.corniceSubject.next(this.cornice);
    };
    PropertiesComponent.prototype.getPictureSrc = function (side, type, nRows, nMounts) {
        var pictSrc = side + "-" + type + "-r" + nRows + "-m" + nMounts + this.pictureExt;
        return this.picturePath + pictSrc;
    };
    /**
     * Смена шага
     */
    PropertiesComponent.prototype.gotoStep = function (step_id) {
        this.onStepChanged.emit(step_id);
    };
    PropertiesComponent.prototype.buy = function (cornice) {
        this.gotoStep(6);
    };
    PropertiesComponent.prototype.timing = function (title, times) {
        if (global_1.Global.IS_DEBUG) {
            this.phpTimes.push((_a = {}, _a[title] = times, _a));
            this.phpAllTime += times;
        }
        var _a;
    };
    /*==*/
    PropertiesComponent.prototype.ngOnDestroy = function () {
        this.tubeLengthsSubscription.unsubscribe();
        this.tubeSubscription.unsubscribe();
        this.connectorsSubscription.unsubscribe();
        this.corniceSubscription.unsubscribe();
    };
    return PropertiesComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", cornice_1.Cornice)
], PropertiesComponent.prototype, "cornice", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], PropertiesComponent.prototype, "steps", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", filter_1.Filter)
], PropertiesComponent.prototype, "filter", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", basket_1.Basket)
], PropertiesComponent.prototype, "basket", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PropertiesComponent.prototype, "onStepChanged", void 0);
__decorate([
    core_1.ViewChild('itemsSlider'),
    __metadata("design:type", items_slider_component_1.ItemsSliderComponent)
], PropertiesComponent.prototype, "itemsSlider", void 0);
__decorate([
    core_1.ViewChild('basketSlider'),
    __metadata("design:type", basket_slider_component_1.BasketSliderComponent)
], PropertiesComponent.prototype, "basketSlider", void 0);
PropertiesComponent = __decorate([
    core_1.Component({
        selector: 'cornice-properties',
        animations: [
            core_2.trigger('leftSideTrigger', [
                core_2.state('true', core_2.style({ marginLeft: '-100%' })),
                core_2.state('false', core_2.style({ marginLeft: '0' })),
                core_2.transition('1 => 0', core_2.animate('200ms ease-out')),
                core_2.transition('0 => 1', core_2.animate('200ms cubic-bezier(0.3, 0.3, .8, 1.5)'))
            ]),
            core_2.trigger('rightSideTrigger', [
                core_2.state('true', core_2.style({ marginLeft: '0' })),
                core_2.state('false', core_2.style({ marginLeft: '-100%' })),
                core_2.transition('1 => 0', core_2.animate('200ms ease-out')),
                core_2.transition('0 => 1', core_2.animate('200ms cubic-bezier(0.3, 0.3, .8, 1.5)'))
            ])
        ],
        templateUrl: 'app/templates/properties.template.html' + ((global_1.Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
    }),
    __metadata("design:paramtypes", [cornice_service_1.CorniceService])
], PropertiesComponent);
exports.PropertiesComponent = PropertiesComponent;
//# sourceMappingURL=properties.component.js.map