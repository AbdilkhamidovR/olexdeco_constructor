import {Component, ViewChild, AfterViewInit, Input, Output, EventEmitter} from '@angular/core';
import {KSSwiperContainer, KSSwiperSlide} from 'angular2-swiper';

//my
import { Global } from '../global'; 
import { Basket }	from '../data/basket';

@Component({
  selector: 'basket-slider',
  templateUrl: 'app/templates/basket-slider.template.html' + ((Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
})
export class BasketSliderComponent  implements AfterViewInit{

	// данные из app.component
	@Input() basket: Basket;

	// this is how you get access to the child component
	@ViewChild(KSSwiperContainer) basketSwiperContainer: KSSwiperContainer;
	private basketSwiperOptions: any;


	constructor(
	) { 
		this.basketSwiperOptions = {
			init: true,
			direction: "vertical",
			slidesPerView: 'auto',
			scrollbarHide: true,
			nextButton: ".swiper-button-down",
			prevButton: ".swiper-button-up",
			scrollbar: ".swiper-scrollbar"
		};
	}

	ngOnInit() {
		
	}

	slidesLoaded() {
		// console.log('basket-slider: basket loaded!');
		this.basketSwiperContainer.swiper.slideTo(0);
	}

	// moveNext() {
	// 	this.basketSwiperOptions.swiper.slideNext();
	// }

	// movePrev() {
	// 	this.basketSwiperOptions.swiper.slidePrev();
	// }

	// slideClicked(id: any){
	// 	console.log("items-slider: slideClicked id", id);
	// 	this.swiperItemSelected.emit(id);
	// }

	ngAfterViewInit() {
		// console.log("basket-slider: basketSwiperContainer", this.basketSwiperContainer);
	}

	ngOnDestroy() {
		this.basketSwiperContainer.swiper.destroy();
	}


}
