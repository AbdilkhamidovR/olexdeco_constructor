import { Component, EventEmitter, OnInit, Input, Output, ElementRef, ViewChild } from '@angular/core';
import { trigger, state, animate, transition, style} from '@angular/core';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/zip';

import { Global } from '../global'; 
import { Step }	from '../data/step';
import { Cornice }	from '../data/cornice';
import { CorniceItem }	from '../data/cornice-item';
import { CorniceService }  from '../services/cornice.service';
import { Filter }	from '../data/filter';
import { Color }	from '../data/color';
import { Basket }	from '../data/basket';
import { ItemsSliderComponent }  from '../components/items-slider.component';
import { BasketSliderComponent }  from '../components/basket-slider.component';

@Component({
	selector: 'cornice-parts-cap',
	animations: [
			trigger('mobileBasketTrigger', [
				state('true' , style({ right: '0' })),
				state('false', style({ right: '-100%' })),
				transition('1 => 0', animate('200ms ease-in-out')),
				transition('0 => 1', animate('200ms ease-in-out'))
			]),
		],
	templateUrl: 'app/templates/parts.template.html' + ((Global.IS_DEBUG) ? '?v=' + Math.random() : '')
})
export class PartsCapComponent  implements OnInit{

	// данные из app.component
	@Input() private cornice: Cornice;
	@Input() private filter: Filter;
	@Input() private steps: any;
	@Input() private user: object;
	@Input() private isMobileBasketActive: boolean;

	@Output() private onStepChanged = new EventEmitter<number>();
	@Output() private onCorniceChanged = new EventEmitter<Cornice>();

	@ViewChild('itemsSlider')
	private itemsSlider: ItemsSliderComponent;

	@ViewChild('basketSlider')
	private basketSlider: BasketSliderComponent;

	//
	private preloader: boolean;
	private isInit: boolean = true;

	private colors: Color[];
	private capFilter: Filter;
	private swiperItems: CorniceItem[];
	private corniceItemId: number;
	private corniceItemColorId: string;
	private basket: Basket = new Basket(0);
	private basketItemKeys: string[];
	private phpTimes: object[] = [];
	private phpAllTime: number = 0;
	//
	private capsSubject: Subject<string> = new Subject();
	private capsSubscription: Subscription;

	private cap2Subject: Subject<CorniceItem> = new Subject();
	private cap2Subscription: Subscription;

	private cap3Subject: Subject<CorniceItem> = new Subject();
	private cap3Subscription: Subscription;

	private colorsSubject: Subject<string> = new Subject();
	private colorsSubscription: Subscription;

	private corniceSubject: Subject<Cornice> = new Subject();
	private corniceSubscription: Subscription;

	private corniceDrawSubject: Subject<boolean> = new Subject();
	private corniceDrawSubscription: Subscription;
	//


	constructor(
		private elementRef: ElementRef,
		private corniceService: CorniceService,
	) {
	};

		
	/*=========*/
	ngOnInit(): void {
		this.startPreloading();

		// загрузка шага
		this.initStep();
		this.initSubscriptions();
	}

	// инициализация данных шага 1. Выбор трубы (профиля)
	initStep(){
		console.clear();

		this.capFilter = new Filter(this.filter.nRowsId, this.filter.tubeFormIds, this.filter.tubeTypeIds, this.filter.tubeLength, this.filter.colorId, 0);
		this.corniceItemId = this.cornice.caps[0].id;
		this.corniceItemColorId = this.cornice.caps[0].colorId;

		this.basket = this.corniceService.makeBasket(this.cornice);

		this.getColors(this.capFilter);
	}

	initSubscriptions(){
		// Отслеживание загрузки цветов
		this.colorsSubscription = this.colorsSubject
			.subscribe( 
				(x) => { 
					console.log('%c Colors loaded! olorsSubject onNext: ' + x, 'color:green'); 
					this.getCaps(this.capFilter);
					switch (this.cornice.nRows) {
						case 3:
							// карниз 3-рядный
							this.getCap3(this.capFilter);
						case 2:
							// карниз 2-рядный
							this.getCap2(this.capFilter);
						case 1:
							// карниз 1-рядный
							break;
					}
				},
				(e) => { console.log('colorsSubject onError: ' + e.message); },
				() => { console.log('%c colorsSubject complete', 'color:green'); }
			);

		// Отслеживание загрузки наконечников
		this.capsSubscription = this.capsSubject
			.subscribe( 
				(s) => { 
					console.log('%c capsSubject onNext: ', 'color:green',  s);
					// this.corniceSubject.next(this.cornice);
					if(this.isInit){
						this.stopPreloading();
						this.isInit = false;
					}
				},
				(e) => { console.log('capsSubject onError: ' + e.message); },
				() => { console.log('%c capsSubject complete', 'color:green'); }
			);

		// Отслеживание загрузки наконечника 2-го ряда
		this.cap2Subscription = this.cap2Subject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c cap2Subject onNext: ', 'color:green',  item); 
					this.cornice.caps[1] = item;
				},
				(e) => { console.log('cap2Subject onError: ' + e.message); },
				() => { console.log('%c cap2Subject complete', 'color:green'); }
			);

		// Отслеживание загрузки наконечника 3-го ряда
		this.cap3Subscription = this.cap3Subject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c cap3Subject onNext: ', 'color:green', item); 
					this.cornice.caps[2] = item;
				},
				(e) => { console.log('Cap3Subject onError: ' + e.message); },
				() => { console.log('%c cap3Subject complete', 'color:green'); }
			);


		//======//
		// Параллельная загрузка доп. рядов
		switch (this.cornice.nRows) {
			case 1:
				// карниз 1-рядный
				Observable.zip(this.colorsSubject, this.capsSubject)
					.subscribe(items => {
						console.log('%c nRows: 1. Observable.zip onNext: ' , 'color:green', items);
				   	this.corniceSubject.next(this.cornice);
				});
				break;
			case 2:
				// карниз 2-рядный
				Observable.zip(this.colorsSubject, this.capsSubject, this.cap2Subject)
					.subscribe(items => {
						console.log('%c nRows: 2. Observable.zip onNext: ' , 'color:green', items);
				   	this.corniceSubject.next(this.cornice);
				});
				break;
			case 3:
				// карниз 3-рядный
				Observable.zip(this.capsSubject, this.cap2Subject, this.cap3Subject)
					.subscribe(items => {
						console.log('%c nRows: 3. Observable.zip onNext: ', 'color:green', items);
				   	this.corniceSubject.next(this.cornice);
				});
				break;
		}

		/* ===== */
		// Отслеживание изменения параметров карниза
		this.corniceSubscription = this.corniceSubject
			.subscribe( 
				(cornice:Cornice) => { 
					console.log('%c Cornice changed! corniceSubject onNext:', 'color:orange', this.cornice); 
					// console.log('this.basket', this.basket); 
					this.onCorniceChanged.emit(this.cornice);
					this.corniceItemId = this.cornice.caps[0].id;
					this.corniceItemColorId = this.cornice.caps[0].colorId;

					this.basket = this.corniceService.makeBasket(this.cornice);
					this.basketSlider.slidesLoaded();
					
					this.drawCornice(this.cornice);
				},
				(e) => { console.log('corniceSubject onError: ' + e.message); },
				() => { console.log('%c corniceSubject complete', 'color:green'); }
			);

		
		// Отслеживание рендеринга карниза
		this.corniceDrawSubscription = this.corniceDrawSubject
			.subscribe( 
				(x) => { 
					console.log('%c Cornice drawed! corniceDrawSubject onNext: ' + x, 'color:green');
					this.stopPreloading();
				},
				(e) => { console.log('corniceDrawSubject onError: ' + e.message); },
				() => { console.log('%c corniceDrawSubject complete', 'color:green'); }
			);
	}


	getCaps(filter:Filter){
		this.corniceService.getCorniceItems("getCaps", filter)
			.subscribe(
				(response:Response) => {
					// console.log("%c getCorniceItems (%s): ", "color:red", "response", response);
					if(response.ok && response.json().arItems){
						this.swiperItems = response.json().arItems;
						this.timing('getCaps', response.json().times);
						console.log("%c getCorniceItems (%s): ", "color:red", "swiperItems", this.swiperItems);

						// смена цвета наконечников
						if(!this.isInit){

							let productId = this.cornice.caps[0].productId;
							let arFounded = this.swiperItems.filter(function(item) {
								return item.productId == productId;
							});
							if(arFounded.length){
								this.cornice.caps[0] = arFounded[0];
							}else{
								this.cornice.caps[0] = this.swiperItems[0];
							}
						}
						this.capsSubject.next("SliderItems");
						this.itemsSlider.slidesLoaded();
						
					}else{
						console.log("%c getCorniceItems (%s): %s not found!", "color:red", "getCaps", "caps");
						this.capFilter.colorId = this.cornice.caps[0].colorId;
						console.log("this.capFilter", this.capFilter);
						this.getCaps(this.capFilter);
					}

				}
			);
	}

	getCap2(filter:Filter){
		this.corniceService.getCorniceItem("getCap2", filter)
			.subscribe(
				(response:Response) => {
					let corniceItem:CorniceItem;
					if(response.ok && response.json().arItems){
						// use new cornice item
						corniceItem = response.json().arItems;
						this.timing('getCap2', response.json().times);
					}else{
						console.log("%c getCorniceItem (%s): %s not found! Use previous value.", "color:red", "getCap2", "Cap2", this.cornice);
						corniceItem = this.cornice.caps[1];
					}
					this.cap2Subject.next(corniceItem);
				}
			);
	}

	getCap3(filter:Filter){
		this.corniceService.getCorniceItem("getCap3", filter)
			.subscribe(
				(response:Response) => {
					let corniceItem:CorniceItem;
					if(response.ok && response.json().arItems){
						// use new cornice item
						corniceItem = response.json().arItems;
						this.timing('getCap3', response.json().times);
					}else{
						console.log("%c getCorniceItem (%s): %s not found! Use previous value.", "color:red", "getCap3", "Cap3", this.cornice);
						corniceItem = this.cornice.caps[2];
					}
					this.cap3Subject.next(corniceItem);
				}
			);
	}

	getColors(filter:Filter){
		this.corniceService.getColors(filter, "getCapsColors")
			.subscribe(
				(response:Response) => {

					// console.log("%c getColors (%s): ", "color:red", "response", response);

					if(response.ok && response.json().arItems){
						this.colors = response.json().arItems;
						this.timing('getColors', response.json().times);
						console.log("%c getColors (%s): ", "color:red", "this.colors", this.colors);
					}else{
						console.log("%c getColors: No colors found", "color:red");
					}
					this.colorsSubject.next("colors");
				}
			);
	}


	startPreloading(){
		this.preloader = true;
	}

	stopPreloading(){
		this.preloader = false;
		console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
		this.phpTimes = [];
		this.phpAllTime = 0;
	}

	

	/*=== ОБРАБОТКА СОБЫТИЙ ===*/
	/**
	 * Смена шага
	 */
	gotoStep(step_id:number){
		this.onStepChanged.emit(step_id);
	}

	/**
	 * Отрисовка карниза
	 */
	drawCornice(cornice:Cornice){
		this.corniceService.getCorniceImg(cornice)
			.subscribe(
				(response:Response) => {
					if(response.ok){
						let imgSrc = response.json().arItems;
						let ar = imgSrc.split(".");
						this.cornice.imgSrc = imgSrc + "?v=" + new Date().getTime();
						this.cornice['imgExt'] = ar[1];
						this.timing('drawCornice', response.json().times);
						console.log("getCorniceImg: this.cornice.imgSrc", cornice.imgSrc);
						this.corniceDrawSubject.next(true);
					}
				}
			);
	}

	/**
	 * Добавление скриптов после загрузки
	 */
	ngAfterViewInit() {
	}

	/*=== ОБРАБОТКА СОБЫТИЙ ===*/

	/**
	 * click on color box handler
	 */
	colorSelected(color_id: number){
		this.startPreloading();
		this.capFilter.colorId = color_id;
		// this.filter.colorId = this.capFilter.colorId;
		this.colorsSubject.next("colors");
	}

	/**
	 * click on mount
	 */
	swiperItemSelected(swiperItemId: number){
		this.startPreloading();

		let arFounded = this.swiperItems.filter(function(item) {
			return item.id == swiperItemId;
		});

		this.cornice.caps[0] = arFounded[0];
		this.corniceSubject.next(this.cornice);
	}


	buy(cornice:Cornice){
		this.gotoStep(6);
	}

	updateImg(imgSrc:string){
		this.startPreloading();
		this.corniceService.updateImg(imgSrc).subscribe(
				(response:Response) => {
					if(response.ok && response.json().arItems){
						this.drawCornice(this.cornice);
					}
				}
			);
	}

	timing(title:string, times:number){
		if(Global.IS_DEBUG){
			this.phpTimes.push({[title]: times});
			this.phpAllTime += times;
		}
	}


	ngOnDestroy() {
		this.capsSubscription.unsubscribe();
		this.cap2Subscription.unsubscribe();
		this.cap3Subscription.unsubscribe();
		this.corniceSubscription.unsubscribe();
		this.corniceDrawSubscription.unsubscribe();
	}

}