"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/zip");
var global_1 = require("../global");
var cornice_1 = require("../data/cornice");
var cornice_service_1 = require("../services/cornice.service");
var filter_1 = require("../data/filter");
var basket_1 = require("../data/basket");
var items_slider_component_1 = require("../components/items-slider.component");
var basket_slider_component_1 = require("../components/basket-slider.component");
var PartsCapComponent = (function () {
    //
    function PartsCapComponent(elementRef, corniceService) {
        this.elementRef = elementRef;
        this.corniceService = corniceService;
        this.onStepChanged = new core_1.EventEmitter();
        this.onCorniceChanged = new core_1.EventEmitter();
        this.isInit = true;
        this.basket = new basket_1.Basket(0);
        this.phpTimes = [];
        this.phpAllTime = 0;
        //
        this.capsSubject = new Subject_1.Subject();
        this.cap2Subject = new Subject_1.Subject();
        this.cap3Subject = new Subject_1.Subject();
        this.colorsSubject = new Subject_1.Subject();
        this.corniceSubject = new Subject_1.Subject();
        this.corniceDrawSubject = new Subject_1.Subject();
    }
    ;
    /*=========*/
    PartsCapComponent.prototype.ngOnInit = function () {
        this.startPreloading();
        // загрузка шага
        this.initStep();
        this.initSubscriptions();
    };
    // инициализация данных шага 1. Выбор трубы (профиля)
    PartsCapComponent.prototype.initStep = function () {
        console.clear();
        this.capFilter = new filter_1.Filter(this.filter.nRowsId, this.filter.tubeFormIds, this.filter.tubeTypeIds, this.filter.tubeLength, this.filter.colorId, 0);
        this.corniceItemId = this.cornice.caps[0].id;
        this.corniceItemColorId = this.cornice.caps[0].colorId;
        this.basket = this.corniceService.makeBasket(this.cornice);
        this.getColors(this.capFilter);
    };
    PartsCapComponent.prototype.initSubscriptions = function () {
        var _this = this;
        // Отслеживание загрузки цветов
        this.colorsSubscription = this.colorsSubject
            .subscribe(function (x) {
            console.log('%c Colors loaded! olorsSubject onNext: ' + x, 'color:green');
            _this.getCaps(_this.capFilter);
            switch (_this.cornice.nRows) {
                case 3:
                    // карниз 3-рядный
                    _this.getCap3(_this.capFilter);
                case 2:
                    // карниз 2-рядный
                    _this.getCap2(_this.capFilter);
                case 1:
                    // карниз 1-рядный
                    break;
            }
        }, function (e) { console.log('colorsSubject onError: ' + e.message); }, function () { console.log('%c colorsSubject complete', 'color:green'); });
        // Отслеживание загрузки наконечников
        this.capsSubscription = this.capsSubject
            .subscribe(function (s) {
            console.log('%c capsSubject onNext: ', 'color:green', s);
            // this.corniceSubject.next(this.cornice);
            if (_this.isInit) {
                _this.stopPreloading();
                _this.isInit = false;
            }
        }, function (e) { console.log('capsSubject onError: ' + e.message); }, function () { console.log('%c capsSubject complete', 'color:green'); });
        // Отслеживание загрузки наконечника 2-го ряда
        this.cap2Subscription = this.cap2Subject
            .subscribe(function (item) {
            console.log('%c cap2Subject onNext: ', 'color:green', item);
            _this.cornice.caps[1] = item;
        }, function (e) { console.log('cap2Subject onError: ' + e.message); }, function () { console.log('%c cap2Subject complete', 'color:green'); });
        // Отслеживание загрузки наконечника 3-го ряда
        this.cap3Subscription = this.cap3Subject
            .subscribe(function (item) {
            console.log('%c cap3Subject onNext: ', 'color:green', item);
            _this.cornice.caps[2] = item;
        }, function (e) { console.log('Cap3Subject onError: ' + e.message); }, function () { console.log('%c cap3Subject complete', 'color:green'); });
        //======//
        // Параллельная загрузка доп. рядов
        switch (this.cornice.nRows) {
            case 1:
                // карниз 1-рядный
                Observable_1.Observable.zip(this.colorsSubject, this.capsSubject)
                    .subscribe(function (items) {
                    console.log('%c nRows: 1. Observable.zip onNext: ', 'color:green', items);
                    _this.corniceSubject.next(_this.cornice);
                });
                break;
            case 2:
                // карниз 2-рядный
                Observable_1.Observable.zip(this.colorsSubject, this.capsSubject, this.cap2Subject)
                    .subscribe(function (items) {
                    console.log('%c nRows: 2. Observable.zip onNext: ', 'color:green', items);
                    _this.corniceSubject.next(_this.cornice);
                });
                break;
            case 3:
                // карниз 3-рядный
                Observable_1.Observable.zip(this.capsSubject, this.cap2Subject, this.cap3Subject)
                    .subscribe(function (items) {
                    console.log('%c nRows: 3. Observable.zip onNext: ', 'color:green', items);
                    _this.corniceSubject.next(_this.cornice);
                });
                break;
        }
        /* ===== */
        // Отслеживание изменения параметров карниза
        this.corniceSubscription = this.corniceSubject
            .subscribe(function (cornice) {
            console.log('%c Cornice changed! corniceSubject onNext:', 'color:orange', _this.cornice);
            // console.log('this.basket', this.basket); 
            _this.onCorniceChanged.emit(_this.cornice);
            _this.corniceItemId = _this.cornice.caps[0].id;
            _this.corniceItemColorId = _this.cornice.caps[0].colorId;
            _this.basket = _this.corniceService.makeBasket(_this.cornice);
            _this.basketSlider.slidesLoaded();
            _this.drawCornice(_this.cornice);
        }, function (e) { console.log('corniceSubject onError: ' + e.message); }, function () { console.log('%c corniceSubject complete', 'color:green'); });
        // Отслеживание рендеринга карниза
        this.corniceDrawSubscription = this.corniceDrawSubject
            .subscribe(function (x) {
            console.log('%c Cornice drawed! corniceDrawSubject onNext: ' + x, 'color:green');
            _this.stopPreloading();
        }, function (e) { console.log('corniceDrawSubject onError: ' + e.message); }, function () { console.log('%c corniceDrawSubject complete', 'color:green'); });
    };
    PartsCapComponent.prototype.getCaps = function (filter) {
        var _this = this;
        this.corniceService.getCorniceItems("getCaps", filter)
            .subscribe(function (response) {
            // console.log("%c getCorniceItems (%s): ", "color:red", "response", response);
            if (response.ok && response.json().arItems) {
                _this.swiperItems = response.json().arItems;
                _this.timing('getCaps', response.json().times);
                console.log("%c getCorniceItems (%s): ", "color:red", "swiperItems", _this.swiperItems);
                // смена цвета наконечников
                if (!_this.isInit) {
                    var productId_1 = _this.cornice.caps[0].productId;
                    var arFounded = _this.swiperItems.filter(function (item) {
                        return item.productId == productId_1;
                    });
                    if (arFounded.length) {
                        _this.cornice.caps[0] = arFounded[0];
                    }
                    else {
                        _this.cornice.caps[0] = _this.swiperItems[0];
                    }
                }
                _this.capsSubject.next("SliderItems");
                _this.itemsSlider.slidesLoaded();
            }
            else {
                console.log("%c getCorniceItems (%s): %s not found!", "color:red", "getCaps", "caps");
                _this.capFilter.colorId = _this.cornice.caps[0].colorId;
                console.log("this.capFilter", _this.capFilter);
                _this.getCaps(_this.capFilter);
            }
        });
    };
    PartsCapComponent.prototype.getCap2 = function (filter) {
        var _this = this;
        this.corniceService.getCorniceItem("getCap2", filter)
            .subscribe(function (response) {
            var corniceItem;
            if (response.ok && response.json().arItems) {
                // use new cornice item
                corniceItem = response.json().arItems;
                _this.timing('getCap2', response.json().times);
            }
            else {
                console.log("%c getCorniceItem (%s): %s not found! Use previous value.", "color:red", "getCap2", "Cap2", _this.cornice);
                corniceItem = _this.cornice.caps[1];
            }
            _this.cap2Subject.next(corniceItem);
        });
    };
    PartsCapComponent.prototype.getCap3 = function (filter) {
        var _this = this;
        this.corniceService.getCorniceItem("getCap3", filter)
            .subscribe(function (response) {
            var corniceItem;
            if (response.ok && response.json().arItems) {
                // use new cornice item
                corniceItem = response.json().arItems;
                _this.timing('getCap3', response.json().times);
            }
            else {
                console.log("%c getCorniceItem (%s): %s not found! Use previous value.", "color:red", "getCap3", "Cap3", _this.cornice);
                corniceItem = _this.cornice.caps[2];
            }
            _this.cap3Subject.next(corniceItem);
        });
    };
    PartsCapComponent.prototype.getColors = function (filter) {
        var _this = this;
        this.corniceService.getColors(filter, "getCapsColors")
            .subscribe(function (response) {
            // console.log("%c getColors (%s): ", "color:red", "response", response);
            if (response.ok && response.json().arItems) {
                _this.colors = response.json().arItems;
                _this.timing('getColors', response.json().times);
                console.log("%c getColors (%s): ", "color:red", "this.colors", _this.colors);
            }
            else {
                console.log("%c getColors: No colors found", "color:red");
            }
            _this.colorsSubject.next("colors");
        });
    };
    PartsCapComponent.prototype.startPreloading = function () {
        this.preloader = true;
    };
    PartsCapComponent.prototype.stopPreloading = function () {
        this.preloader = false;
        console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
        this.phpTimes = [];
        this.phpAllTime = 0;
    };
    /*=== ОБРАБОТКА СОБЫТИЙ ===*/
    /**
     * Смена шага
     */
    PartsCapComponent.prototype.gotoStep = function (step_id) {
        this.onStepChanged.emit(step_id);
    };
    /**
     * Отрисовка карниза
     */
    PartsCapComponent.prototype.drawCornice = function (cornice) {
        var _this = this;
        this.corniceService.getCorniceImg(cornice)
            .subscribe(function (response) {
            if (response.ok) {
                var imgSrc = response.json().arItems;
                var ar = imgSrc.split(".");
                _this.cornice.imgSrc = imgSrc + "?v=" + new Date().getTime();
                _this.cornice['imgExt'] = ar[1];
                _this.timing('drawCornice', response.json().times);
                console.log("getCorniceImg: this.cornice.imgSrc", cornice.imgSrc);
                _this.corniceDrawSubject.next(true);
            }
        });
    };
    /**
     * Добавление скриптов после загрузки
     */
    PartsCapComponent.prototype.ngAfterViewInit = function () {
    };
    /*=== ОБРАБОТКА СОБЫТИЙ ===*/
    /**
     * click on color box handler
     */
    PartsCapComponent.prototype.colorSelected = function (color_id) {
        this.startPreloading();
        this.capFilter.colorId = color_id;
        // this.filter.colorId = this.capFilter.colorId;
        this.colorsSubject.next("colors");
    };
    /**
     * click on mount
     */
    PartsCapComponent.prototype.swiperItemSelected = function (swiperItemId) {
        this.startPreloading();
        var arFounded = this.swiperItems.filter(function (item) {
            return item.id == swiperItemId;
        });
        this.cornice.caps[0] = arFounded[0];
        this.corniceSubject.next(this.cornice);
    };
    PartsCapComponent.prototype.buy = function (cornice) {
        this.gotoStep(6);
    };
    PartsCapComponent.prototype.updateImg = function (imgSrc) {
        var _this = this;
        this.startPreloading();
        this.corniceService.updateImg(imgSrc).subscribe(function (response) {
            if (response.ok && response.json().arItems) {
                _this.drawCornice(_this.cornice);
            }
        });
    };
    PartsCapComponent.prototype.timing = function (title, times) {
        if (global_1.Global.IS_DEBUG) {
            this.phpTimes.push((_a = {}, _a[title] = times, _a));
            this.phpAllTime += times;
        }
        var _a;
    };
    PartsCapComponent.prototype.ngOnDestroy = function () {
        this.capsSubscription.unsubscribe();
        this.cap2Subscription.unsubscribe();
        this.cap3Subscription.unsubscribe();
        this.corniceSubscription.unsubscribe();
        this.corniceDrawSubscription.unsubscribe();
    };
    return PartsCapComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", cornice_1.Cornice)
], PartsCapComponent.prototype, "cornice", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", filter_1.Filter)
], PartsCapComponent.prototype, "filter", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], PartsCapComponent.prototype, "steps", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], PartsCapComponent.prototype, "user", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], PartsCapComponent.prototype, "isMobileBasketActive", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PartsCapComponent.prototype, "onStepChanged", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PartsCapComponent.prototype, "onCorniceChanged", void 0);
__decorate([
    core_1.ViewChild('itemsSlider'),
    __metadata("design:type", items_slider_component_1.ItemsSliderComponent)
], PartsCapComponent.prototype, "itemsSlider", void 0);
__decorate([
    core_1.ViewChild('basketSlider'),
    __metadata("design:type", basket_slider_component_1.BasketSliderComponent)
], PartsCapComponent.prototype, "basketSlider", void 0);
PartsCapComponent = __decorate([
    core_1.Component({
        selector: 'cornice-parts-cap',
        animations: [
            core_2.trigger('mobileBasketTrigger', [
                core_2.state('true', core_2.style({ right: '0' })),
                core_2.state('false', core_2.style({ right: '-100%' })),
                core_2.transition('1 => 0', core_2.animate('200ms ease-in-out')),
                core_2.transition('0 => 1', core_2.animate('200ms ease-in-out'))
            ]),
        ],
        templateUrl: 'app/templates/parts.template.html' + ((global_1.Global.IS_DEBUG) ? '?v=' + Math.random() : '')
    }),
    __metadata("design:paramtypes", [core_1.ElementRef,
        cornice_service_1.CorniceService])
], PartsCapComponent);
exports.PartsCapComponent = PartsCapComponent;
//# sourceMappingURL=parts-cap.component.js.map