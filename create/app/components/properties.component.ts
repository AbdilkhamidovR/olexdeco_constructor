import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { trigger, state, animate, transition, style} from '@angular/core';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/zip';

import { Global } from '../global'; 
import { CorniceService }  from '../services/cornice.service';
import { Step }	from '../data/step';
import { Cornice }	from '../data/cornice';
import { CorniceItem }	from '../data/cornice-item';
import { Filter }	from '../data/filter';
import { Basket }	from '../data/basket';

import { ItemsSliderComponent }  from '../components/items-slider.component';
import { BasketSliderComponent }  from '../components/basket-slider.component';

@Component({
	selector: 'cornice-properties',
	animations: [
		trigger('leftSideTrigger', [
			state('true' , style({ marginLeft: '-100%' })),
			state('false', style({ marginLeft: '0' })),
			transition('1 => 0', animate('200ms ease-out')),
			transition('0 => 1', animate('200ms cubic-bezier(0.3, 0.3, .8, 1.5)'))
		]),
		trigger('rightSideTrigger', [
			state('true' , style({ marginLeft: '0' })),
			state('false', style({ marginLeft: '-100%' })),
			transition('1 => 0', animate('200ms ease-out')),
			transition('0 => 1', animate('200ms cubic-bezier(0.3, 0.3, .8, 1.5)'))
		])
	],
	templateUrl: 'app/templates/properties.template.html' + ((Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
})
export class PropertiesComponent  implements OnInit{

	// данные из app.component
	@Input() cornice: Cornice;
	@Input() steps: any;
	@Input() filter: Filter;
	@Input() basket: Basket;
	@Output() private onStepChanged = new EventEmitter<number>();

	@ViewChild('itemsSlider')
	private itemsSlider: ItemsSliderComponent;

	@ViewChild('basketSlider')
	private basketSlider: BasketSliderComponent;
	
	//
	
	private preloader: boolean;
	private tubeLengths: number[];
	private propsFilter: Filter;
	private phpTimes: object[] = [];
	private phpAllTime: number = 0;

	private connectors: CorniceItem[];
	private connectorExists: {direct:false, oriel:false};

	private tubeLengthsSubject: Subject<number[]> = new Subject();
	private tubeLengthsSubscription: Subscription;

	private tubeSubject: Subject<CorniceItem> = new Subject();
	private tubeSubscription: Subscription;
	private tsNextEventsLength: number = 0;

	private corniceSubject: Subject<Cornice> = new Subject();
	private corniceSubscription: Subscription;

	private connectorsSubject: Subject<CorniceItem[]> = new Subject();
	private connectorsSubscription: Subscription;
	//
	
	private picturePath:string = "./assets/images/segments3/";
	private pictureExt:string = ".png";
	private leftPicture:string;
	private isLeftSideVisible : boolean = false;
	private isRightSideVisible : boolean = false;
	private centerPicture:string;
	private rightPicture:string;
	
	private mountsLength:number = 0;
	private ringsLength:number = 0;
	private ringsTitle:string = "Кольца";
	private ringByMeter:number = Global.RINGS_BY_METER;
	private ringsPackQuantity:number;


	constructor(
		private corniceService: CorniceService,
	) {};

	
	ngOnInit(): void {
		console.clear();

		this.startPreloading();
		this.initStep();
		this.initSubscriptions();
	}

	initStep(){

		this.propsFilter = new Filter(this.filter.nRowsId, this.filter.tubeFormIds, this.filter.tubeTypeIds, this.filter.tubeLength, this.filter.colorId, 0);

		this.basket = this.corniceService.makeBasket(this.cornice);
		// console.log('basket', this.basket);

		this.getTubeLengthsByTubes(this.cornice.tubes);

		this.getConnectors(this.cornice.tubes);
	}

	initSubscriptions(){
		// Отслеживание загрузки длин труб
		this.tubeLengthsSubscription = this.tubeLengthsSubject
			.subscribe( 
				(tubeLengths) => { 
					console.log('%c tubeLengths loaded! tubeLengths onNext: ', 'color:green', tubeLengths);
					// this.corniceSubject.next(this.cornice);
				},
				(e) => { console.log('tubeLengthsSubject onError: ' + e.message); },
				() => { console.log('%c tubeLengthsSubject complete', 'color:green'); }
			);

		// Отслеживание загрузки соединителей
		this.connectorsSubscription = this.connectorsSubject
			.subscribe( 
				(connectors:CorniceItem[]) => { 
					console.log('%c Сonnectors loaded! сonnectors:', 'color:green', connectors ); 
					console.log('%c connectorExists:', 'color:green', this.connectorExists ); 
				},
				(e) => { console.log('tubeSubject onError: ' + e.message); },
				() => { console.log('%c tubeSubject complete', 'color:green'); }
			);

		// Отслеживание загрузки трубы
		this.tubeSubscription = this.tubeSubject
			.subscribe( 
				(item:CorniceItem) => { 
					console.log('%c tubeSubject onNext: ', 'color:green', item ); 
					this.tsNextEventsLength++;
					if(this.tsNextEventsLength == this.cornice.tubes.length){
						this.tsNextEventsLength = 0;
						this.corniceSubject.next(this.cornice);
					}
				},
				(e) => { console.log('tubeSubject onError: ' + e.message); },
				() => { console.log('%c tubeSubject complete', 'color:green'); }
			);

		// Отслеживание окончания загрузки соединителей и длин труб
		Observable.zip.apply(null, [this.tubeLengthsSubject, this.connectorsSubject])
			.subscribe((x:any) => {
				console.log('%c Observable.zip onNext: ', 'color:green', x);
		   	this.corniceSubject.next(this.cornice);
		});

		/* ===== */
		// Отслеживание изменения параметров карниза
		this.corniceSubscription = this.corniceSubject
			.subscribe( 
				(cornice:Cornice) => { 

					console.log('%c Cornice changed! corniceSubject onNext:', 'color:orange', this.cornice); 

					// выбор картинок схем   
					let centerType = "";
					if(this.cornice.sides.left.tubeLength > 0){
						this.isLeftSideVisible = true;
						let leftMountLength = this.corniceService.getMountLength(this.cornice.sides.left.tubeLength);
						if(this.cornice.sides.left.type == "direct"){
							leftMountLength--;
						}
						this.cornice.sides.left.imgSrc = this.getPictureSrc("left", this.cornice.sides.left.type, this.cornice.nRows, leftMountLength);
						centerType = "left"+this.cornice.sides.left.type;
					}
					if(this.cornice.sides.right.tubeLength > 0){
						this.isRightSideVisible = true;
						centerType += "right"+this.cornice.sides.right.type;
						let rightMountLength = this.corniceService.getMountLength(this.cornice.sides.right.tubeLength);
						if(this.cornice.sides.right.type == "direct"){
							rightMountLength--;
						}
						this.cornice.sides.right.imgSrc = this.getPictureSrc("right", this.cornice.sides.right.type, this.cornice.nRows, rightMountLength);
					}
					
					// обновление кол-ва наконечников и колец
					this.cornice.mounts[0].length = this.corniceService.getMountLength(this.cornice.tubes[0].tubeLength);

					switch (this.cornice.rings[0].ringType) {
						case "RING":
							// code...
							this.ringsTitle = "Кольца";
							this.ringsPackQuantity = Global.RINGS_IN_PACK;
							break;
						
						case "RUNNER":
							// code...
							this.ringsTitle = "Бегунки";
							this.ringsPackQuantity = Global.RUNNERS_IN_PACK;
							break;
						
						default:
							// code...
							break;
					}
					for (let i in this.cornice.rings) {
						this.cornice.rings[i].length = this.corniceService.getBasketRingsInRowLength(this.cornice);
					}
					this.cornice.schemeImgSrc = this.getPictureSrc("center", centerType, this.cornice.nRows, this.cornice.mounts[0].length);

					// апдэйт корзины
					this.basket = this.corniceService.makeBasket(this.cornice);
					console.log('Cornice changed! corniceSubject basket:', this.basket); 

					this.stopPreloading();
				},
				(e) => { console.log('corniceSubject onError: ' + e.message); },
				() => { console.log('%c corniceSubject complete', 'color:green'); }
			);
	}

	getTubeLengthsByTubes(tubes: CorniceItem[]){
		this.corniceService.getTubeLengthsByTubes(tubes)
			.subscribe(
				(response:Response) => {
					if(response.ok && response.json().arItems){
						this.tubeLengths = response.json().arItems;
						this.timing('tubeLengths', response.json().times);
						// console.log("%c getTubeLengths (%s): ", "color:green", "tubeLengths", this.tubeLengths);
						this.tubeLengthsSubject.next(this.tubeLengths);
					}else{
						console.log("%c getTubeLengths: not found!", "color:red");
					}
				}
			);
	}

	getTubeExt(filter:Filter, nRow:any = 0, side:string){
		let params = {nRow: nRow};
		this.corniceService.getCorniceItem("getTubeExt", filter, params)
			.subscribe(
				(response:Response) => {
					let corniceItem:CorniceItem;
					if(response.ok && response.json().arItems){
						// use new cornice item
						corniceItem = response.json().arItems;
						this.timing('getTubeExt', response.json().times);
						switch (side) {
							case "center":
								this.cornice.tubes[nRow] = corniceItem;
								this.filter.tubeLength = corniceItem.tubeLength;
								break;

							case "left":
								this.cornice.sides.left.tubes[nRow] = corniceItem;
								break;

							case "right":
								this.cornice.sides.right.tubes[nRow] = corniceItem;
								break;
							
							default:
								break;
						}
						this.tubeSubject.next(corniceItem);
					}else{
						corniceItem = undefined;
						console.log("%c getTube: Tube not found!", "color:red");
					}
				}
			);
	}

	getConnectors(tubes: CorniceItem[]){
		this.corniceService.getConnectors(tubes)
			.subscribe(
				(response:Response) => {
					// console.log("%c getConnectors: response.json", "color:blue", response.json());
					if(response.ok && response.json().arItems){
						// use new cornice item
						this.connectors = response.json().arItems.connectors;
						this.connectorExists = response.json().arItems.exists;
						this.timing('getConnectors', response.json().times);
						this.connectorsSubject.next(this.connectors);
					}else{
						console.log("%c getConnectors: Connectors not found for all rows!", "color:red");
					}
				}
			);
	}

	startPreloading(){
		this.preloader = true;
	}

	stopPreloading(){
		this.preloader = false;
		console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
		this.phpTimes = [];
		this.phpAllTime = 0;
	}




	/*=== ОБРАБОТКА ПОЛЬЗОВАТЕЛЬСКИХ СОБЫТИЙ ===*/


	/**
	 * click on tube length
	 */
	tubeLengthSelected(value: number){
		this.startPreloading();
		this.propsFilter.tubeLength = Number(value);
		for (let nRow in this.cornice.tubes) {
			this.propsFilter.productId = this.cornice.tubes[nRow].productId;
			this.getTubeExt(this.propsFilter, nRow, 'center');
		}
	}

	leftSegmentAdded(type: string){
		this.startPreloading();
		this.cornice.sides.left.type = type;
		this.cornice.sides.left.tubeLength = this.cornice.tubes[0].tubeLength;
		this.cornice.sides.left.tubes = this.corniceService.copyObj(this.cornice.tubes);
		this.cornice.sides.left.connectors = this.connectors[type];
		this.corniceSubject.next(this.cornice);
	}
	rightSegmentAdded(type: string){
		this.startPreloading();
		this.cornice.sides.right.type = type;
		this.cornice.sides.right.tubeLength = this.cornice.tubes[0].tubeLength;
		this.cornice.sides.right.tubes = this.corniceService.copyObj(this.cornice.tubes);
		this.cornice.sides.right.connectors = this.connectors[type];
		this.corniceSubject.next(this.cornice);
	}

	leftSegmentTubeLengthSelected(value: number){
		this.startPreloading();
		this.propsFilter.tubeLength = Number(value);
		this.cornice.sides.left.tubeLength = Number(value);
		for (let nRow in this.cornice.tubes) {
			this.propsFilter.productId = this.cornice.tubes[nRow].productId;
			this.getTubeExt(this.propsFilter, nRow, 'left');
		}
	}
	rightSegmentTubeLengthSelected(value: number){
		this.startPreloading();
		this.propsFilter.tubeLength = Number(value);
		this.cornice.sides.right.tubeLength = Number(value);
		for (let nRow in this.cornice.tubes) {
			this.propsFilter.productId = this.cornice.tubes[nRow].productId;
			this.getTubeExt(this.propsFilter, nRow, 'right');
		}
	}

	setLeftSegmentType(type: string){
		this.cornice.sides.left.type = type;
		this.cornice.sides.left.connectors = this.connectors[type];
		if(!this.cornice.sides.left.tubeLength){
			this.cornice.sides.left.tubeLength = this.cornice.tubes[0].tubeLength;
			this.cornice.sides.left.tubes = this.corniceService.copyObj(this.cornice.tubes); // передача по ссылке!
		}
		this.corniceSubject.next(this.cornice);
	}
	setRightSegmentType(type: string){
		this.cornice.sides.right.type = type;
		this.cornice.sides.right.connectors = this.connectors[type];
		if(!this.cornice.sides.right.tubeLength){
			this.cornice.sides.right.tubeLength = this.cornice.tubes[0].tubeLength;
			this.cornice.sides.right.tubes = this.corniceService.copyObj(this.cornice.tubes); // передача по ссылке!
		}
		this.corniceSubject.next(this.cornice);
	}


	leftSegmentDelete(){
		this.cornice.resetLeftSide();
		this.isLeftSideVisible = false;
		this.corniceSubject.next(this.cornice);
	}
	rightSegmentDelete(){
		this.cornice.resetRightSide();
		this.isRightSideVisible = false;
		this.corniceSubject.next(this.cornice);
	}

	getPictureSrc(side:string, type:string, nRows:number, nMounts:number){
		let pictSrc = side+"-"+type+"-r"+nRows+"-m"+nMounts+this.pictureExt;
		return this.picturePath+pictSrc;
	}

	/**
	 * Смена шага
	 */
	gotoStep(step_id:number){
		this.onStepChanged.emit(step_id);
	}

	buy(cornice:Cornice){
		this.gotoStep(6);
	}

	timing(title:string, times:number){
		if(Global.IS_DEBUG){
			this.phpTimes.push({[title]: times});
			this.phpAllTime += times;
		}
	}

	/*==*/

	ngOnDestroy() {
		this.tubeLengthsSubscription.unsubscribe();
		this.tubeSubscription.unsubscribe();
		this.connectorsSubscription.unsubscribe();
		this.corniceSubscription.unsubscribe();
	}

}
