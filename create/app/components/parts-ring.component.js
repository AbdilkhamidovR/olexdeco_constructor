"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/zip");
var global_1 = require("../global");
var cornice_1 = require("../data/cornice");
var cornice_service_1 = require("../services/cornice.service");
var filter_1 = require("../data/filter");
var basket_1 = require("../data/basket");
var items_slider_component_1 = require("../components/items-slider.component");
var basket_slider_component_1 = require("../components/basket-slider.component");
var PartsRingComponent = (function () {
    //
    function PartsRingComponent(elementRef, corniceService) {
        this.elementRef = elementRef;
        this.corniceService = corniceService;
        this.onStepChanged = new core_1.EventEmitter();
        this.onCorniceChanged = new core_1.EventEmitter();
        this.isInit = true;
        this.basket = new basket_1.Basket(0);
        this.phpTimes = [];
        this.phpAllTime = 0;
        //
        this.ringsSubject = new Subject_1.Subject();
        this.ring2Subject = new Subject_1.Subject();
        this.ring3Subject = new Subject_1.Subject();
        this.colorsSubject = new Subject_1.Subject();
        this.corniceSubject = new Subject_1.Subject();
        this.corniceDrawSubject = new Subject_1.Subject();
    }
    ;
    /*=========*/
    PartsRingComponent.prototype.ngOnInit = function () {
        this.startPreloading();
        // загрузка шага
        this.initStep();
        this.initSubscriptions();
    };
    // инициализация данных шага 1. Выбор трубы (профиля)
    PartsRingComponent.prototype.initStep = function () {
        console.clear();
        this.ringFilter = new filter_1.Filter(this.filter.nRowsId, this.filter.tubeFormIds, this.filter.tubeTypeIds, this.filter.tubeLength, this.filter.colorId, 0);
        this.corniceItemId = this.cornice.rings[0].id;
        this.corniceItemColorId = this.cornice.rings[0].colorId;
        this.basket = this.corniceService.makeBasket(this.cornice);
        this.getColors(this.ringFilter);
    };
    PartsRingComponent.prototype.initSubscriptions = function () {
        var _this = this;
        // Отслеживание загрузки цветов
        this.colorsSubscription = this.colorsSubject
            .subscribe(function (x) {
            console.log('%c Colors loaded! olorsSubject onNext: ' + x, 'color:green');
            _this.getRings(_this.ringFilter);
        }, function (e) { console.log('colorsSubject onError: ' + e.message); }, function () { console.log('%c colorsSubject complete', 'color:green'); });
        // Отслеживание загрузки наконечников
        this.ringsSubscription = this.ringsSubject
            .subscribe(function (s) {
            console.log('%c ringsSubject onNext: ', 'color:green', s);
            // this.corniceSubject.next(this.cornice);
            if (_this.isInit) {
                _this.stopPreloading();
                _this.isInit = false;
            }
            else {
                _this.ringFilter["name"] = _this.getRingNameToFilter(_this.cornice.rings[0].name);
                switch (_this.cornice.nRows) {
                    case 3:
                        // карниз 3-рядный
                        _this.getRing3byRing1(_this.ringFilter);
                    case 2:
                        // карниз 2-рядный
                        _this.getRing2byRing1(_this.ringFilter);
                    case 1:
                        // карниз 1-рядный
                        break;
                }
            }
        }, function (e) { console.log('ringsSubject onError: ' + e.message); }, function () { console.log('%c ringsSubject complete', 'color:green'); });
        // Отслеживание загрузки наконечника 2-го ряда
        this.ring2Subscription = this.ring2Subject
            .subscribe(function (item) {
            console.log('%c ring2Subject onNext: ', 'color:green', item);
            _this.cornice.rings[1] = item;
        }, function (e) { console.log('ring2Subject onError: ' + e.message); }, function () { console.log('%c ring2Subject complete', 'color:green'); });
        // Отслеживание загрузки наконечника 3-го ряда
        this.ring3Subscription = this.ring3Subject
            .subscribe(function (item) {
            console.log('%c ring3Subject onNext: ', 'color:green', item);
            _this.cornice.rings[2] = item;
        }, function (e) { console.log('Ring3Subject onError: ' + e.message); }, function () { console.log('%c ring3Subject complete', 'color:green'); });
        //======//
        // Параллельная загрузка доп. рядов
        switch (this.cornice.nRows) {
            case 1:
                // карниз 1-рядный
                Observable_1.Observable.zip(this.colorsSubject, this.ringsSubject)
                    .subscribe(function (items) {
                    console.log('%c nRows: 1. Observable.zip onNext: ', 'color:green', items);
                    _this.corniceSubject.next(_this.cornice);
                });
                break;
            case 2:
                // карниз 2-рядный
                Observable_1.Observable.zip(this.colorsSubject, this.ringsSubject, this.ring2Subject)
                    .subscribe(function (items) {
                    console.log('%c nRows: 2. Observable.zip onNext: ', 'color:green', items);
                    _this.corniceSubject.next(_this.cornice);
                });
                break;
            case 3:
                // карниз 3-рядный
                Observable_1.Observable.zip(this.ringsSubject, this.ring2Subject, this.ring3Subject)
                    .subscribe(function (items) {
                    console.log('%c nRows: 3. Observable.zip onNext: ', 'color:green', items);
                    _this.corniceSubject.next(_this.cornice);
                });
                break;
        }
        /* ===== */
        // Отслеживание изменения параметров карниза
        this.corniceSubscription = this.corniceSubject
            .subscribe(function (cornice) {
            console.log('%c Cornice changed! corniceSubject onNext:', 'color:orange', _this.cornice);
            // console.log('this.basket', this.basket); 
            _this.onCorniceChanged.emit(_this.cornice);
            _this.corniceItemId = _this.cornice.rings[0].id;
            _this.corniceItemColorId = _this.cornice.rings[0].colorId;
            for (var i in _this.cornice.rings) {
                _this.cornice.rings[i].length = _this.corniceService.getBasketRingsInRowLength(_this.cornice);
            }
            _this.basket = _this.corniceService.makeBasket(_this.cornice);
            _this.basketSlider.slidesLoaded();
            _this.drawCornice(_this.cornice);
        }, function (e) { console.log('corniceSubject onError: ' + e.message); }, function () { console.log('%c corniceSubject complete', 'color:green'); });
        // Отслеживание рендеринга карниза
        this.corniceDrawSubscription = this.corniceDrawSubject
            .subscribe(function (x) {
            console.log('%c Cornice drawed! corniceDrawSubject onNext: ' + x, 'color:green');
            _this.stopPreloading();
        }, function (e) { console.log('corniceDrawSubject onError: ' + e.message); }, function () { console.log('%c corniceDrawSubject complete', 'color:green'); });
    };
    PartsRingComponent.prototype.getRings = function (filter) {
        var _this = this;
        // this.filter.tubeTypeIds = item.tubeTypeIds;
        var params = null;
        if (this.filter.tubeFormIds[0] == global_1.Global.PROFILE) {
            params = { profileId: this.cornice.tubes[0].productId };
        }
        this.corniceService.getCorniceItems("getRings", filter, params)
            .subscribe(function (response) {
            // console.log("%c getCorniceItems (%s): ", "color:red", "response", response);
            if (response.ok && response.json().arItems) {
                _this.swiperItems = response.json().arItems;
                _this.timing('getRings', response.json().times);
                console.log("getCorniceItems (%s): ", "swiperItems", _this.swiperItems);
                // смена цвета наконечников
                if (!_this.isInit) {
                    var productId_1 = _this.cornice.rings[0].productId;
                    var arFounded = _this.swiperItems.filter(function (item) {
                        return item.productId == productId_1;
                    });
                    if (arFounded.length) {
                        _this.cornice.rings[0] = arFounded[0];
                    }
                    else {
                        _this.cornice.rings[0] = _this.swiperItems[0];
                    }
                }
                _this.ringsSubject.next("SliderItems");
                _this.itemsSlider.slidesLoaded();
            }
            else {
                console.log("%c getCorniceItems (%s): %s not found!", "color:red", "getRings", "rings");
                if (_this.cornice.rings[0]) {
                    _this.ringFilter.colorId = _this.cornice.rings[0].colorId;
                }
                else {
                    _this.ringFilter.colorId = "";
                }
                console.log("this.ringFilter", _this.ringFilter);
                _this.getRings(_this.ringFilter);
            }
        });
    };
    PartsRingComponent.prototype.getRing2byRing1 = function (filter) {
        var _this = this;
        var params = null;
        if (this.filter.tubeFormIds[0] == global_1.Global.PROFILE) {
            params = { profileId: this.cornice.tubes[1].productId };
        }
        this.corniceService.getCorniceItem("getRing2", filter, params)
            .subscribe(function (response) {
            var corniceItem;
            if (response.ok && response.json().arItems) {
                // use new cornice item
                _this.cornice.rings[1] = response.json().arItems;
                _this.timing('getRing2byRing1', response.json().times);
            }
            else {
                console.log("%c getCorniceItem (%s): %s not found! Use previous value.", "color:red", "getRing2", "Ring2", _this.cornice);
            }
            if (_this.cornice.nRows == 3) {
                _this.getRing3byRing1(filter);
            }
            else {
                _this.corniceSubject.next(_this.cornice);
            }
        });
    };
    PartsRingComponent.prototype.getRing3byRing1 = function (filter) {
        var _this = this;
        var params = null;
        if (this.filter.tubeFormIds[0] == global_1.Global.PROFILE) {
            params = { profileId: this.cornice.tubes[2].productId };
        }
        this.corniceService.getCorniceItem("getRing2", filter)
            .subscribe(function (response) {
            var corniceItem;
            if (response.ok && response.json().arItems) {
                // use new cornice item
                _this.cornice.rings[2] = response.json().arItems;
                _this.timing('getRing3byRing1', response.json().times);
            }
            else {
                console.log("%c getCorniceItem (%s): %s not found! Use previous value.", "color:red", "getRing2", "Ring2", _this.cornice);
            }
            _this.corniceSubject.next(_this.cornice);
        });
    };
    PartsRingComponent.prototype.getColors = function (filter) {
        var _this = this;
        this.corniceService.getColors(filter, "getRingsColors")
            .subscribe(function (response) {
            // console.log("%c getColors (%s): ", "color:red", "response", response);
            if (response.ok && response.json().arItems) {
                _this.colors = response.json().arItems;
                _this.timing('getColors', response.json().times);
                console.log("getColors (%s): ", "this.colors", _this.colors);
            }
            else {
                console.log("%c getColors: No colors found", "color:red");
            }
            _this.colorsSubject.next("colors");
        });
    };
    PartsRingComponent.prototype.startPreloading = function () {
        this.preloader = true;
    };
    PartsRingComponent.prototype.stopPreloading = function () {
        this.preloader = false;
        console.log('%c Times report: ', 'color:green', this.phpTimes, this.phpAllTime);
        this.phpTimes = [];
        this.phpAllTime = 0;
    };
    /*=== ОБРАБОТКА СОБЫТИЙ ===*/
    /**
     * Смена шага
     */
    PartsRingComponent.prototype.gotoStep = function (step_id) {
        this.onStepChanged.emit(step_id);
    };
    /**
     * Отрисовка карниза
     */
    PartsRingComponent.prototype.drawCornice = function (cornice) {
        var _this = this;
        this.corniceService.getCorniceImg(cornice)
            .subscribe(function (response) {
            if (response.ok) {
                var imgSrc = response.json().arItems;
                var ar = imgSrc.split(".");
                _this.cornice.imgSrc = imgSrc + "?v=" + new Date().getTime();
                _this.cornice['imgExt'] = ar[1];
                _this.timing('drawCornice', response.json().times);
                console.log("getCorniceImg: this.cornice.imgSrc", cornice.imgSrc);
                _this.corniceDrawSubject.next(true);
            }
        });
    };
    /**
     * Добавление скриптов после загрузки
     */
    PartsRingComponent.prototype.ngAfterViewInit = function () {
    };
    /*=== ОБРАБОТКА СОБЫТИЙ ===*/
    /**
     * click on color box handler
     */
    PartsRingComponent.prototype.colorSelected = function (color_id) {
        this.startPreloading();
        this.ringFilter.colorId = color_id;
        // this.filter.colorId = this.ringFilter.colorId;
        this.colorsSubject.next("colors");
    };
    /**
     * click on rings in slider
     */
    PartsRingComponent.prototype.swiperItemSelected = function (swiperItemId) {
        this.startPreloading();
        var arFounded = this.swiperItems.filter(function (item) {
            return item.id == swiperItemId;
        });
        this.cornice.rings[0] = arFounded[0];
        this.ringFilter["name"] = this.getRingNameToFilter(this.cornice.rings[0].name);
        switch (this.cornice.nRows) {
            case 3:
            // карниз 3-рядный
            case 2:
                // карниз 2-рядный
                this.getRing2byRing1(this.ringFilter);
                break;
            case 1:
                // карниз 1-рядный
                this.corniceSubject.next(this.cornice);
                break;
        }
    };
    PartsRingComponent.prototype.getRingNameToFilter = function (str) {
        str = str.replace(/( упаковка.*)/gi, '');
        str = str.replace(/\d/gi, '%');
        str = str.replace(/%+/gi, '%');
        str += '%';
        // console.log('getRingNameToFilter: str', str);
        return str;
    };
    PartsRingComponent.prototype.buy = function (cornice) {
        this.gotoStep(6);
    };
    PartsRingComponent.prototype.updateImg = function (imgSrc) {
        var _this = this;
        this.startPreloading();
        this.corniceService.updateImg(imgSrc).subscribe(function (response) {
            if (response.ok && response.json().arItems) {
                _this.drawCornice(_this.cornice);
            }
        });
    };
    PartsRingComponent.prototype.timing = function (title, times) {
        if (global_1.Global.IS_DEBUG) {
            this.phpTimes.push((_a = {}, _a[title] = times, _a));
            this.phpAllTime += times;
        }
        var _a;
    };
    PartsRingComponent.prototype.ngOnDestroy = function () {
        this.ringsSubscription.unsubscribe();
        this.ring2Subscription.unsubscribe();
        this.ring3Subscription.unsubscribe();
        this.corniceSubscription.unsubscribe();
        this.corniceDrawSubscription.unsubscribe();
    };
    return PartsRingComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", cornice_1.Cornice)
], PartsRingComponent.prototype, "cornice", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", filter_1.Filter)
], PartsRingComponent.prototype, "filter", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], PartsRingComponent.prototype, "steps", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], PartsRingComponent.prototype, "user", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], PartsRingComponent.prototype, "isMobileBasketActive", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PartsRingComponent.prototype, "onStepChanged", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PartsRingComponent.prototype, "onCorniceChanged", void 0);
__decorate([
    core_1.ViewChild('itemsSlider'),
    __metadata("design:type", items_slider_component_1.ItemsSliderComponent)
], PartsRingComponent.prototype, "itemsSlider", void 0);
__decorate([
    core_1.ViewChild('basketSlider'),
    __metadata("design:type", basket_slider_component_1.BasketSliderComponent)
], PartsRingComponent.prototype, "basketSlider", void 0);
PartsRingComponent = __decorate([
    core_1.Component({
        selector: 'cornice-parts-ring',
        animations: [
            core_2.trigger('mobileBasketTrigger', [
                core_2.state('true', core_2.style({ right: '0' })),
                core_2.state('false', core_2.style({ right: '-100%' })),
                core_2.transition('1 => 0', core_2.animate('200ms ease-in-out')),
                core_2.transition('0 => 1', core_2.animate('200ms ease-in-out'))
            ]),
        ],
        templateUrl: 'app/templates/parts.template.html' + ((global_1.Global.IS_DEBUG) ? '?v=' + Math.random() : '')
    }),
    __metadata("design:paramtypes", [core_1.ElementRef,
        cornice_service_1.CorniceService])
], PartsRingComponent);
exports.PartsRingComponent = PartsRingComponent;
//# sourceMappingURL=parts-ring.component.js.map