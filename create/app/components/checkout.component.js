"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var global_1 = require("../global");
var cornice_1 = require("../data/cornice");
var cornice_service_1 = require("../services/cornice.service");
var CheckoutComponent = (function () {
    function CheckoutComponent(corniceService) {
        this.corniceService = corniceService;
        this.onStepChanged = new core_1.EventEmitter();
        this.preloader = false;
        this.showTimer = false;
        this.ticks = 0;
        this.addToBxBasketSubject = new Subject_1.Subject();
    }
    ;
    CheckoutComponent.prototype.ngOnInit = function () {
        // console.log(this.steps);
        console.clear();
        // console.log(this.cornice);
        this.initStep();
    };
    CheckoutComponent.prototype.initStep = function () {
        // апдэйт корзины
        this.basket = this.corniceService.makeBasket(this.cornice);
        // Отслеживание загрузки цветов
        this.addToBxBasketSubscription = this.addToBxBasketSubject
            .subscribe(function (x) {
            console.log('%c BxBasket updated! addToBxBasketSubject onNext: ' + x, 'color:green');
        }, function (e) { console.log('addToBxBasketSubject onError: ' + e.message); }, function () { console.log('%c addToBxBasketSubject complete', 'color:green'); });
    };
    CheckoutComponent.prototype.startPreloading = function () {
        this.preloader = true;
    };
    CheckoutComponent.prototype.stopPreloading = function () {
        this.preloader = false;
    };
    CheckoutComponent.prototype.addToBxBasket = function (basket) {
        this.startPreloading();
        this.corniceService.addToBxBasket(basket)
            .subscribe(function (response) {
            // console.log("%c addToBxBasket (%s): ", "color:black", "response", response);
            if (response.ok && response.json().arItems) {
                console.log("%c addToBxBasket (%s): ", "color:green", "added");
                window.location.href = global_1.Global.BX_BASKET_URL;
            }
            else {
                console.log("%c addToBxBasket: error adding!", "color:red");
            }
        });
    };
    /*=== ОБРАБОТКА ПОЛЬЗОВАТЕЛЬСКИХ СОБЫТИЙ ===*/
    CheckoutComponent.prototype.gotoStep = function (step_id) {
        this.onStepChanged.emit(step_id);
    };
    CheckoutComponent.prototype.checkout = function (basket) {
        this.addToBxBasket(basket);
    };
    CheckoutComponent.prototype.ngOnDestroy = function () {
        this.addToBxBasketSubscription.unsubscribe();
    };
    return CheckoutComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", cornice_1.Cornice)
], CheckoutComponent.prototype, "cornice", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], CheckoutComponent.prototype, "steps", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], CheckoutComponent.prototype, "onStepChanged", void 0);
CheckoutComponent = __decorate([
    core_1.Component({
        selector: 'cornice-checkout',
        templateUrl: 'app/templates/checkout.template.html' + ((global_1.Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
    }),
    __metadata("design:paramtypes", [cornice_service_1.CorniceService])
], CheckoutComponent);
exports.CheckoutComponent = CheckoutComponent;
//# sourceMappingURL=checkout.component.js.map