import {Component, ViewChild, AfterViewInit, Input, Output, EventEmitter} from '@angular/core';
import {KSSwiperContainer, KSSwiperSlide} from 'angular2-swiper';

//my
import { Global } from '../global'; 
import { CorniceItem }	from '../data/cornice-item';


@Component({
  selector: 'items-slider',
  templateUrl: 'app/templates/items-slider.template.html' + ((Global.IS_DEBUG) ? '?v=' + Math.random() : ''),
})
export class ItemsSliderComponent  implements AfterViewInit{

	// данные из app.component
	@Input() swiperItems: CorniceItem[];
	@Input() corniceItemId: number;

	// объявление события изменения фильтра выборки элементов карниза
	@Output() swiperItemSelected = new EventEmitter<number>();

	// private preloader: boolean = false;
	
	// this is how you get access to the child component
	@ViewChild(KSSwiperContainer) itemsSwiperContainer: KSSwiperContainer;
	private slides: Array<any>;
	private itemsSwipeOptions: any;


	constructor(
	) { 
		this.itemsSwipeOptions = {
			init: false,
			slidesPerView: 'auto',
			scrollbarHide: false,
			nextButton: ".ks-swiper-container .swiper-button-next",
			prevButton: ".ks-swiper-container .swiper-button-prev",
			scrollbar: ".ks-swiper-container .swiper-scrollbar"
		};
	}

	ngOnInit() {
		
	}

	slidesLoaded() {
		// console.log('items-slider: sliderItemsLoaded from parts-mount loaded!');
		this.itemsSwiperContainer.swiper.slideTo(0);
	}

	// moveNext() {
	// 	this.itemsSwiperContainer.swiper.slideNext();
	// }

	// movePrev() {
	// 	this.itemsSwiperContainer.swiper.slidePrev();
	// }

	slideClicked(id: any){
		// console.log("items-slider: slideClicked id", id);
		this.swiperItemSelected.emit(id);
	}

	ngAfterViewInit() {
		// console.log("items-slider: itemsSwiperContainer", this.itemsSwiperContainer);
		// this.itemsSwiperContainer.swiper.update();
	}

	ngOnDestroy() {
		this.itemsSwiperContainer.swiper.destroy();
	}


}
