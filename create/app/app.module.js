"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var http_1 = require("@angular/http");
var angular2_swiper_1 = require("angular2-swiper");
var app_component_1 = require("./app.component");
//my
var priceformat_pipe_1 = require("./pipes/priceformat.pipe");
var rows_component_1 = require("./components/rows.component");
var parts_tubes_component_1 = require("./components/parts-tubes.component");
var parts_mount_component_1 = require("./components/parts-mount.component");
var parts_cap_component_1 = require("./components/parts-cap.component");
var parts_ring_component_1 = require("./components/parts-ring.component");
var properties_component_1 = require("./components/properties.component");
var checkout_component_1 = require("./components/checkout.component");
var finalpopup_component_1 = require("./components/finalpopup.component");
var helper_service_1 = require("./services/helper.service");
var cornice_service_1 = require("./services/cornice.service");
var items_slider_component_1 = require("./components/items-slider.component");
var basket_slider_component_1 = require("./components/basket-slider.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            animations_1.BrowserAnimationsModule,
            http_1.HttpModule,
            angular2_swiper_1.KSSwiperModule
        ],
        declarations: [
            app_component_1.AppComponent,
            priceformat_pipe_1.PriceformatPipe,
            rows_component_1.RowsComponent,
            parts_tubes_component_1.PartsTubesComponent,
            parts_mount_component_1.PartsMountComponent,
            parts_cap_component_1.PartsCapComponent,
            parts_ring_component_1.PartsRingComponent,
            properties_component_1.PropertiesComponent,
            checkout_component_1.CheckoutComponent,
            finalpopup_component_1.FinalpopupComponent,
            items_slider_component_1.ItemsSliderComponent,
            basket_slider_component_1.BasketSliderComponent
        ],
        providers: [helper_service_1.HelperService, cornice_service_1.CorniceService],
        bootstrap: [app_component_1.AppComponent]
    }),
    __metadata("design:paramtypes", [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map