import { Pipe, PipeTransform } from '@angular/core';
 
@Pipe({
	name: 'priceformat'
})
export class PriceformatPipe implements PipeTransform {
	transform(value: any, format?: string): string {
		format = format ? format : "# руб.";
		return format.replace('#', value);
	}
}