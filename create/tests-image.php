<?php 
namespace Ra\Constructor;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
include "./api/cornice.class.php";

$CCornice = new Cornice();

// $res = $CCornice -> getCorniceImg(array(
// 	"cornice" => array(
// 		"id" => "",
// 		"activeStep" => "1",
// 		"caps" => array(array("id" => "18087"), array("id" => "18473")),
// 		"nRows" => 2,
// 		"mounts" => array(array("id"=>"18727")),
// 		"rings" => array(array("id" => "19438"), array("id" => "19418")),
// 		"tubes" => array(array("id" => "19166"), array("id" => "19036")),
// 	)
// ));


$res = $CCornice -> getCorniceImg(array(
	"cornice" => array(
		"id" => "",
		"activeStep" => "1",
		"caps" => array(array("id" => "18705")),
		"nRows" => 2,
		"mounts" => array(array("id"=>"18761")),
		"rings" => array(array("id" => "19418")),
		"tubes" => array(array("id" => "19036")),
	)
));

// ppr($res, __FILE__.' $res');
// echo '<img src="'.$res.'" alt="">';
?>


<style type="text/css">
	.img-block{
		width: 855px;
		background-color: #ccc;
	}
	.cornice-test-img{
		margin: 0;
		height: 564px;
		display: table-cell;
		vertical-align: middle;
	}
	.cornice-test-img img{
		width: 100%;
		height: auto;
	}
</style>
<div class="img-block">
	<a class="save"></a>
	<a class="print"></a>
	<figure class="cornice-test-img">
		<img src="<?=$res;?>">
	</figure>
</div>

<div class="descr">
	Cornice Img
	<? //ppr($CCornice -> getParams(), __FILE__.' $CCornice -> getParams()');?>
	<? fppr($CCornice -> getResult(), __FILE__.' $CCornice -> getResult()', false, false);?>
</div>
<?
// 


?>