<?php 
namespace Ra\Constructor;

$start = microtime(true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
include "./api/cornice.class.php";

// phpinfo();

// 
$CCornice = new Cornice();

// $arRes = $CCornice -> getParams();


// $arRes = $CCornice -> getUser();
// $arRes = $CCornice -> isAdmin();


// $CCornice -> getItem(array("ID" => 17500));
// $CCornice -> getItems( array("FILTER" => array("ID"=>18025)) );
// 
// $CCornice -> getCapsColors();
// $CCornice -> getTubeColors(array(
// 	"colorId" => "90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30057",
// 	"tubeFormIds"=> array("30069", 0, 0),
// 	"tubeTypeIds"=> array("9d4e3524-7c3e-11e7-a9bb-000c290d7f42", 0, 0),

// ));
// $CCornice -> getColorProperties();
// $CCornice -> getTubeTypeProperties();
// 

// $res = $CCornice -> getRowQuantities();

// $res = $CCornice -> getMount(array(
// 	"nRowsId" => "30057",
// 	"tubeFormIds" => array("30072", 0, 0),
// 	"tubeTypeIds" => array("5feaf38b-7c3f-11e7-a9bb-000c290d7f42", "9d4e3524-7c3e-11e7-a9bb-000c290d7f42"), 
// 	"colorId" => "a345aad2-7c3f-11e7-a9bb-000c290d7f42"
// ));

// $CCornice -> getTubeForms(array(
// 	"nRowsId" =>"30057", "tubeFormId" => 0, "tubeTypeId" => 0, "colorId" => 0
// 	));
// 

// Трубы
// $CCornice -> getTubeLengthsByTube(array(
// 	"colorId" => "90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30056", 
// 	"tubeFormIds" => array("30069"), 
// 	"tubeTypeIds" => array("cb9501de-7c3e-11e7-a9bb-000c290d7f42"), 
// 	"tubeLength" => 3000,
// 	"productId" => 0
// 	));

// Профили
// $CCornice -> getTubeLengthsByTube(array(
// 	"colorId" => "90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30056", 
// 	"tubeFormIds" => array("30072"), 
// 	"tubeTypeIds" => array("32dc1d77-7c3f-11e7-a9bb-000c290d7f42"), 
// 	"tubeLength" => 3000,
// 	"productId" => 0
// 	));

// $CCornice -> getMountTubeTypes(
// 	array(
// 		"colorId" => "90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 		"nRowsId" => "30057",
// 		"productId" => 0,
// 		"tubeFormsIds" => array("30069", null),
// 		"tubeLength" => 3000,
// 		"tubeTypeIds" => array("b9fac578-7c3e-11e7-a9bb-000c290d7f42", "9d4e3524-7c3e-11e7-a9bb-000c290d7f42"),
// 	),
// 	array('nRow' => 1)
// ); 

$arRes = $CCornice -> getTubeTypes(
	array(
		"colorId" => "90d02b96-7c3f-11e7-a9bb-000c290d7f42",
		"nRowsId" => "30056",
		"productId" => 0,
		"tubeFormIds" => array("30069"),
		"tubeLength" => 1400,
		"tubeTypeIds" => array("9d4e3524-7c3e-11e7-a9bb-000c290d7f42"),
	),
	array()
);

// $CCornice -> getTubeByFormColorType(array(
// 	"tubeFormId" => 30069,
// 	"tubeTypeId" => "9d4e3524-7c3e-11e7-a9bb-000c290d7f42",
// 	"colorId" => "90d02b96-7c3f-11e7-a9bb-000c290d7f42"
// 	));
// 	

// $arRes = $CCornice -> getTube(array(
// 	"colorId" =>"90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30056",
// 	"productId" => 0,
// 	"tubeFormIds" => array("30069"), 
// 	"tubeLength" => 3000,
// 	"tubeTypeIds" => array("9d4e3524-7c3e-11e7-a9bb-000c290d7f42"), 
// ));

// $arRes = $CCornice -> getTube2(array(
// 	"colorId" =>"99b9db41-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30057",
// 	"productId" => 0,
// 	"tubeFormIds" => array("30069", 0, 0), 
// 	"tubeLength" => 3000,
// 	"tubeTypeIds" => array("9d4e3524-7c3e-11e7-a9bb-000c290d7f42", "32dc1d77-7c3f-11e7-a9bb-000c290d7f42"), 
// ));

// $arRes = $CCornice -> getCap2(array(
// 	"colorId" =>"a345aad2-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30057",
// 	"tubeFormIds" => array("30072", 0, 0), 
// 	"tubeTypeIds" => array("5feaf38b-7c3f-11e7-a9bb-000c290d7f42", "5feaf38b-7c3f-11e7-a9bb-000c290d7f42"), 
// ));

// $arRes = $CCornice -> getRings(
// 	array(
// 		"colorId" =>"90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 		"nRowsId" => "30057",
// 		"productId" => 0,
// 		"tubeFormIds" => array("30072",), 
// 		"tubeTypeIds" => array("59ca1d2d-7c3f-11e7-a9bb-000c290d7f42", "59ca1d2d-7c3f-11e7-a9bb-000c290d7f42"), 
// 		"tubeLength" => 3000
// 	),
// 	array(
// 		"profileId" => 19357,
// 	),
// 	false, 0
// );

// $arRes = $CCornice -> getRing(array(
// 	"colorId" =>"90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30056",
// 	"tubeFormIds" => array("30072", 0, 0), 
// 	"tubeTypeIds" => array("59ca1d2d-7c3f-11e7-a9bb-000c290d7f42"), 
// ));



// $arRes = $CCornice -> getMounts(array(
// 	"colorId" =>"90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30057",
// 	"productId" => 0,
// 	"tubeFormIds" => array("30069", null), 
// 	"tubeLength" => 3000,
// 	"tubeTypeIds" => array( "b9fac578-7c3e-11e7-a9bb-000c290d7f42", "b9fac577-7c3e-11e7-a9bb-000c290d7f42" ), 
// ));

// $arRes = $CCornice -> getMountByFirstRow(array(
// 	"colorId" =>"90d02b97-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30057",
// 	"tubeFormIds" => array("30069", null), 
// 	"tubeTypeIds" => array( "9d4e3524-7c3e-11e7-a9bb-000c290d7f42", "9d4e3524-7c3e-11e7-a9bb-000c290d7f42"), 
// 	"productId" => 0,
// 	"tubeLength" => 3000
// ));

// $arRes = $CCornice -> getCap2(array(
// 	"colorId" =>"90d02b97-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30057",
// 	"tubeFormIds" => array("30069", null), 
// 	"tubeTypeIds" => array("9d4e3524-7c3e-11e7-a9bb-000c290d7f42", "9d4e3524-7c3e-11e7-a9bb-000c290d7f42"), 
// 	"productId" => 0,
// 	"tubeLength" => 3000
// ));
// 
// $arRes = $CCornice -> getMountColors(array(
// 	"colorId" =>"45d4f0ed-83ef-11e7-a9c0-000c290d7f42",
// 	"nRowsId" => "30058",
// 	"tubeFormIds" => array("30069", 0, 0), 
// 	"tubeTypeIds" => array( "9d4e3524-7c3e-11e7-a9bb-000c290d7f42", "9d4e3524-7c3e-11e7-a9bb-000c290d7f42", "9d4e3524-7c3e-11e7-a9bb-000c290d7f42" ), 
// ));

// $arRes = $CCornice -> getCapsColors(array(
// 	"colorId" =>"90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30056",
// 	"tubeFormIds" => array("30072", 0, 0), 
// 	"tubeTypeIds" => array( "5feaf38b-7c3f-11e7-a9bb-000c290d7f42" ), 
// ));
// 
// $arRes = $CCornice -> getRing2(array(
// 	"colorId" =>"90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 	"name" => "Кольцо круглое % с пластиком%",
// 	"nRowsId" => "30057",
// 	"productId" => 0,
// 	"tubeFormIds" => array("30069", 0, 0), 
// 	"tubeTypeIds" => array( "b9fac578-7c3e-11e7-a9bb-000c290d7f42", "9d4e3524-7c3e-11e7-a9bb-000c290d7f42" ), 
// ));

// $arRes = $CCornice -> getTubeLengths(array(
// 	"colorId" =>"90d02b96-7c3f-11e7-a9bb-000c290d7f42",
// 	"nRowsId" => "30057",
// 	"productId" => "19030",
// 	"tubeFormIds" => array("30069", 0), 
// 	"tubeTypeIds" => array("9d4e3524-7c3e-11e7-a9bb-000c290d7f42", "9d4e3524-7c3e-11e7-a9bb-000c290d7f42"), 
// ));

// $arRes = $CCornice -> getTubeLengthsByTubes(array(
// 	"tubes" => array(array("id" => "19166"), array("id" => "19030")), 
// ));

// $arRes = $CCornice -> getTubeLengthsByTube(array("id"=>19048));

// $res = $CCornice -> getConnectors(array(
// 	"cornice" => array(
// 		"id" => "1510047611274",
// 		"activeStep" => "5",
// 		"caps" => array(array("id" => "18251")),
// 		"nRows" => 2,
// 		"tubes" => array(array("id" => "19166"), array("id" => "19036")),
// 		"mounts" => array(array("id"=>"18860")),
// 		"rings" => array(array("id" => "19520")),
// 		"sides" => array("left"=>array("type"=>"direct"))
// 	)
// ));

// $res = $CCornice -> addToBxBasket(array(
// 	"basket" => array(
// 			"id" => "1513185408281",
// 			"sum" => "1789",
// 			"currency" => "# руб.",
// 			"sumFormatted" => "1789 руб.",
// 			"items" => array(
// 					"t_19036" => array(
// 							"id" => "19036",
// 							"productId" => "19030",
// 							"name" => "Труба гладкая 16 мм",
// 							"previewImgSrc" => "/upload/resize_cache/iblock/fda/168_168_2/fda1e7d187ada9d2162cba36f47cdae0.png",
// 							"basketImgSrc" => "/upload/resize_cache/iblock/fda/70_70_2/fda1e7d187ada9d2162cba36f47cdae0.png",
// 							"price" => "502",
// 							"priceFormatted" => "502 руб.",
// 							"baseUnit" => "Штуку",
// 							"currency" => "# руб.",
// 							"tubeLength" => "3000",
// 							"length" => "1",
// 							"tubeTypeIds" => array
// 								(
// 								),

// 							"usage" => "tube",
// 							"volume" => "0,0000326000",
// 							"weight" => "0,018",
// 						),

// 					"m_18761" => array(
// 							"id" => "18761",
// 							"productId" => "18719",
// 							"name" => "Кронштейн «Винтаж I»",
// 							"previewImgSrc" => "/upload/resize_cache/iblock/642/168_168_2/6428f3193561ac8c781c1ac0cd38594f.jpg",
// 							"basketImgSrc" => "/upload/resize_cache/iblock/642/70_70_2/6428f3193561ac8c781c1ac0cd38594f.jpg",
// 							"price" => "241",
// 							"priceFormatted" => "241 руб.",
// 							"baseUnit" => "Штуку",
// 							"currency" => "# руб.",
// 							"tubeLength" => "0",
// 							"length" => "3",
// 							"tubeTypeIds" => array(
// 									"0" => "9d4e3524-7c3e-11e7-a9bb-000c290d7f42",
// 								),

// 							"usage" => "mount",
// 							"volume" => "0,0000326000",
// 							"weight" => "0,018",
// 						),

// 					"c_18702" => array(
// 							"id" => "18702",
// 							"productId" => "18638",
// 							"name" => "Наконечник «Ветка большая»",
// 							"previewImgSrc" => "/upload/resize_cache/iblock/720/168_168_2/720bc6439b74fdd24dec030477ad0b29.png",
// 							"basketImgSrc" => "/upload/resize_cache/iblock/720/70_70_2/720bc6439b74fdd24dec030477ad0b29.png",
// 							"price" => "360",
// 							"priceFormatted" => "360 руб.",
// 							"baseUnit" => "Пару (2 штуки)",
// 							"currency" =>" # руб.",
// 							"tubeLength" => "0",
// 							"length" => "1",
// 							"tubeTypeIds" => array(
// 								),

// 							"usage" => "cap",
// 							"volume" => "0,0000326000",
// 							"weight" => "0,018",
// 						),

// 					"r_19418" => array(
// 							"id" => "19418",
// 							"productId" => "19360",
// 							"name" => "Кольцо круглое 16",
// 							"previewImgSrc" => "/upload/resize_cache/iblock/ce7/168_168_2/ce7565731a39edf11dbf506fefc5e0c0.png",
// 							"basketImgSrc" => "/upload/resize_cache/iblock/ce7/70_70_2/ce7565731a39edf11dbf506fefc5e0c0.png",
// 							"price" => "68",
// 							"priceFormatted" => "68 руб.",
// 							"baseUnit" => "Упаковку (10 штук)",
// 							"currency" => "# руб.",
// 							"tubeLength" => "0",
// 							"length" => "3",
// 							"tubeTypeIds" => array(
// 								),

// 							"usage" => "ring",
// 							"volume" => "0,0000326000",
// 							"weight" => "0,018",
// 						),

// 				),

// 			"itemKeys" => array(
// 					"0" => "t_19036",
// 					"1" => "m_18761",
// 					"2" => "c_18702",
// 					"3" => "r_19418",
// 				),

// 			"itemLengths" => array(
// 					"mounts" => "3",
// 					"rings" => "3",
// 					"tubes" => "0",
// 					"caps" => "1",
// 					"connectors" => "0",
// 				),

// 		)
// ));
	
 // $CCornice -> updateImg();

ppr($arRes, __FILE__.' $arRes()');
// ppr($CCornice -> getResult(), __FILE__.' $CCornice -> getResult()');	

$time = (int)((microtime(true) - $start)*1000);
echo "<h3>Время работы: ".$time." cек.</h3>";
?>