<?php $path = "/constructor/create/"; ?>
<?php $protocol = isset($_SERVER["HTTPS"]) ? 'https:' : 'http:'; ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Конструктор «Olexdeco»</title>
		<base href="<?=$path;?>">
		<meta name="title" content="Конструктор «Olexdeco»">
		<meta name="description" content="Собрать карниз своей мечты">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="canonical" href="<?php echo $protocol;?>//<?php echo $_SERVER["SERVER_NAME"];?>/constructor/">
		
		<link href='./assets/vendor/css/fontawesome-all.min.css' rel='stylesheet' type='text/css' media="all">
		<link href="./assets/css/style.css?v=3.3" rel='stylesheet' type='text/css' media="all">
		<link href="./assets/css/swiper.css?v=3.0" rel='stylesheet' type='text/css' media="all">

		<!--script src="./assets/vendor/js/jquery-3.2.1.min.js" type="text/javascript"></script-->
		<!--script src="./assets/vendor/js/swiper.jquery.min.js" type="text/javascript"></script-->
		<!--script src="./assets/vendor/js/console.js" type="text/javascript"></script-->
		<!--script src="./assets/js/main.js" type="text/javascript"></script-->

		<!-- Begin Angular scripts -->
		<!-- Polyfills -->
		<script src="./assets/vendor/ng/shim.min.js?v=1.0"></script>

		<script src="./assets/vendor/ng/zone.min.js?v=1.0"></script>
		<!--script src="./assets/vendor/ng/zone.js?v=1.0"></script-->
		<script src="./assets/vendor/ng/system.src.js?v=1.0"></script>

		<script src="systemjs.deploy.config.js?v=5.1"></script>
		<script>
			System.import('main.js?v=1.0').catch(function(err){ console.error(err); });
		</script>
		<!-- End Angular scripts -->

	</head>

	<body id="main" class="cornice-builder-page">

		<div class="wrapper">

			<div class="content">
				<div id="builder">
					
					<cornice-app><div class="preloader-block show"><div class="preloader"></div></div></cornice-app>

				</div>    
			</div><!-- .content -->

		</div><!-- .wrapper -->

	</body>
</html>
