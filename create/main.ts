import {enableProdMode} from '@angular/core';
import { Global } from './app/global'; 
if(!Global.IS_DEBUG){
  enableProdMode();
  window.console.log = function(){};
  window.console.clear = function(){};
}

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
